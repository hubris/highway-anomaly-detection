# Modified from https://github.com/againerju/maad_highway/blob/master/src/dataset.py

import os
import math
import re

import configargparse
import numpy as np
from tqdm import tqdm
import torch

from utils.utils import LaneNode, find_nearest

DATA_MODE = ['train', 'test', 'val']


def read_file(_path, delim='\t'):
    data = []
    if delim == 'tab':
        delim = '\t'
    elif delim == 'space':
        delim = ' '
    with open(_path, 'r') as f:
        for line in f:
            line = line.strip().split(delim)
            line = [float(i) for i in line]
            data.append(line)
    return np.asarray(data)


if __name__ == '__main__':

    p = configargparse.ArgumentParser(
        prog='MAAD Dataset Converter',
        description='Converts the format of the MAAD dataset into numpy files \
                        with lane node information readable by the TrajectoryDataset class.\
                        Expects that the data to be converted is stored at:\
                        \'data/<run_name>/original/<split>/\'',
        formatter_class=configargparse.ArgumentDefaultsHelpFormatter
    )

    # Train or Test Data Conversion
    p.add('--split', type=str, metavar='SPLIT', required=True, choices=DATA_MODE,
          help='Train or test data conversion. This only impacts which subfolder the data is loaded from and saved to.')

    # Run Name
    p.add('--run_name', type=str, metavar='RUN_NAME', required=True,
          help='Name of subfolder under data folder to load trajectories from and save to.')

    # Normalize Data
    p.add('--norm', action='store_true', default=False,
                help='Normalize data points between 0 and 1.')

    # Sequence Length
    p.add('--obs_len', type=int, metavar='TIME', default=15,
                help='Sequence length of intermediate trajectories.')

    # Skip Frames
    p.add('--skip', type=int, metavar='SKIP', default=1,
                help='Number of frames to skip in the sliding window segmentation of the whole episode trajectory.')

    args = p.parse_args()

    # Directory with dataset
    data_dir = os.path.join("data", args.run_name, "original", args.split)
    try:
        assert os.path.exists(data_dir)
    except:
        print('[FAILED] Expecting to find the data at the following path: ', data_dir)
        quit()

    # Initialize variables for data converter
    max_agents_in_frame = 0
    obs_len = seq_len = args.obs_len
    skip = args.skip
    delim = '\t'
    min_agents = 0

    # data files
    all_files = os.listdir(data_dir)
    all_files = [os.path.join(data_dir, _path) for _path in all_files]

    # init data lists
    num_agents_in_seq = []
    seg_list = []
    seg_list_rel = []
    frame_ids_list = []
    seq_id_list = []
    labels_list = []

    # load sequences
    print("\nLoading dataset...")

    for seq_id, path in enumerate(tqdm(all_files)):

        # creating sequence id = <txt file number>.<0/1 anomaly label>
        curr_filename = os.path.basename(path)
        res = re.split('_|.txt', curr_filename)
        sequence_label = str(1 if res[0] == 'abnormal' else 0)
        sequence_num = str(int(res[1]))
        complete_sequence_id = float(sequence_num + '.' + sequence_label)

        data = read_file(path, delim)

        # organize sequence data per frame
        frames = np.unique(data[:, 0]).tolist()
        frame_data = []
        for frame in frames:
            frame_data.append(data[frame == data[:, 0], :])
            num_sequences = int(math.ceil((len(frames) - seq_len + 1) / skip))

        # sliding window segmentation
        for idx in range(0, num_sequences * skip + 1, skip):

            # get segment data
            curr_seq_data = np.concatenate(frame_data[idx:idx + seq_len], axis=0)
            agents_in_curr_seq = np.unique(curr_seq_data[:, 2])
            max_agents_in_frame = max(max_agents_in_frame, len(agents_in_curr_seq))

            # initialize segment
            curr_seq_rel = np.zeros((len(agents_in_curr_seq), 2, seq_len))
            curr_seq = np.zeros((len(agents_in_curr_seq), 2, seq_len))
            curr_frame_ids = np.zeros((len(agents_in_curr_seq), 1, seq_len))
            curr_labels = np.zeros((len(agents_in_curr_seq), 2, seq_len))
            curr_seq_id = np.zeros((len(agents_in_curr_seq), 1))

            # iterate through agents in segment
            num_agents_considered = 0
            _non_linear_agent = []
            for _, agent_id in enumerate(agents_in_curr_seq):

                # get agent data
                curr_agent_seq = curr_seq_data[curr_seq_data[:, 2] == agent_id, :]
                curr_agent_seq = np.around(curr_agent_seq, decimals=4)

                # define padding
                pad_front = frames.index(curr_agent_seq[0, 0]) - idx
                pad_end = frames.index(curr_agent_seq[-1, 0]) - idx + 1
                if pad_end - pad_front != seq_len or curr_agent_seq.shape[0] != seq_len:
                    continue

                # get agent frames
                curr_agent_frames = curr_agent_seq[:, 0]

                # get agent labels
                curr_agent_labels = np.transpose(curr_agent_seq[:, -2:])

                # get agent trajectory
                curr_agent_seq = np.transpose(curr_agent_seq[:, 3:5])

                # log segment data
                rel_curr_agent_seq = np.zeros(curr_agent_seq.shape)
                rel_curr_agent_seq[:, 1:] = curr_agent_seq[:, 1:] - curr_agent_seq[:, :-1]
                _idx = num_agents_considered
                curr_seq[_idx, :, pad_front:pad_end] = curr_agent_seq
                curr_seq_rel[_idx, :, pad_front:pad_end] = rel_curr_agent_seq
                curr_frame_ids[_idx, :, pad_front:pad_end] = curr_agent_frames
                curr_seq_id[_idx] = complete_sequence_id
                curr_labels[_idx, :, pad_front:pad_end] = curr_agent_labels

                num_agents_considered += 1

            if num_agents_considered > min_agents:
                num_agents_in_seq.append(num_agents_considered)
                frame_ids_list.append(curr_frame_ids)
                seq_id_list.append(curr_seq_id)
                labels_list.append(curr_labels)
                seg_list.append(curr_seq[:num_agents_considered])
                seg_list_rel.append(curr_seq_rel[:num_agents_considered])

    num_seq = len(seg_list)
    seg_list = np.concatenate(seg_list, axis=0)
    seg_list_rel = np.concatenate(seg_list_rel, axis=0)
    frame_ids_list = np.concatenate(frame_ids_list, axis=0)
    seq_id_list = np.concatenate(seq_id_list, axis=0)
    labels_list = np.concatenate(labels_list, axis=0)

    # convert numpy -> torch tensor
    obs_traj = torch.from_numpy(seg_list[:, :, :obs_len]).type(torch.float)                 # (num sequences * num vehicles, pos_dim, sequence length)
    obs_traj_rel = torch.from_numpy(seg_list_rel[:, :, :obs_len]).type(torch.float)         # (num sequences * num vehicles, pos_dim, sequence length)
    frame_ids = torch.from_numpy(frame_ids_list[:, :, :obs_len]).type(torch.int)            # (num sequences * num vehicles, 1, sequence length)
    seq_ids = torch.from_numpy(seq_id_list).type(torch.float)                               # (num sequences * num vehicles, 1)
    labels = torch.from_numpy(labels_list[:, :, :obs_len]).type(torch.int)                  # (num sequences * num vehicles, 2, sequence length)
    cum_start_idx = [0] + np.cumsum(num_agents_in_seq).tolist()
    seq_start_end = [(start, end) for start, end in zip(cum_start_idx, cum_start_idx[1:])]

    # Reshaping and permuting to expected shape for data loader
    batch_seq_ids = seq_ids.reshape((-1, max_agents_in_frame)).detach().to('cpu').numpy()                                               # (num sequences, num vehicles)
    batch_frame_ids = frame_ids.reshape((-1, max_agents_in_frame, obs_len)).detach().to('cpu').numpy()                                  # (num sequences, num vehicles, sequence length)
    batch_obs_traj = obs_traj.reshape((-1, max_agents_in_frame, 2, obs_len)).permute((0, 1, 3, 2)).detach().to('cpu').numpy()           # (num sequences, num vehicles, sequence length, pos_dim)
    batch_obs_traj_rel = obs_traj_rel.reshape((-1, max_agents_in_frame, 2, obs_len)).permute((0, 1, 3, 2)).detach().to('cpu').numpy()   # (num sequences, num vehicles, sequence length, pos_dim)
    batch_labels = labels.reshape((-1, max_agents_in_frame, 2, obs_len)).permute((0, 1, 3, 2)).detach().to('cpu').numpy()               # (num sequences, num vehicles, sequence length, 2)

    # Create Lane Graph Representation of MAAD Environment
    full_map = {}
    full_map_key_list = []

    # MAAD Map Setup
    H =  66.66666666666667              # height of total map
    W =  416.6666666666667              # width of total map
    LANE_WIDTH =  8.333333333333334     # width of each lane
    STREET_CENTER =  33.0               # horizontal center of map
    UPPER =  49.66666666666667          # top most edge of the map
    LOWER =  16.333333333333332         # bottom most edge of the map
    MIN_X_REL, MAX_X_REL = -5.6, 6.3    # min/max x change
    MIN_Y_REL, MAX_Y_REL = -2.1, 4.5    # min/max y change

    # Lane id's follow same convention as the MAAD environment
    #   0 - outer bottom lane
    #   1 - inner bottom lane
    #   2 - inner top lane
    #   3 - outer top lane

    map_resolution = 5.                     # x-distance of each lane node
    start_node = 0.                         # tracks x dim start of current lane node
    end_node = start_node + map_resolution  # tracks x dim end of current lane node

    # y-positions of each lane
    lane_0_y = LOWER + LANE_WIDTH / 2.
    lane_1_y = LOWER + LANE_WIDTH * 1.5
    lane_2_y = STREET_CENTER + LANE_WIDTH / 2.
    lane_3_y = STREET_CENTER + LANE_WIDTH * 1.5

    # Bottom lanes
    # Lane 0
    start_node_lane_0 = np.array([start_node, lane_0_y])
    end_node_lane_0 = np.array([end_node, lane_0_y])
    curr_node_lane_0_pos = np.average(np.array([start_node_lane_0, end_node_lane_0]), axis=0)
    full_map_key_list.append(curr_node_lane_0_pos)
    curr_node_lane_0_index = len(full_map_key_list) - 1
    curr_node_lane_0 = LaneNode(None)
    curr_node_lane_0.pos_x, curr_node_lane_0.pos_y = curr_node_lane_0_pos[0], curr_node_lane_0_pos[1]
    full_map[curr_node_lane_0_index] = curr_node_lane_0
    # Lane 1
    start_node_lane_1 = np.array([start_node, lane_1_y])
    end_node_lane_1 = np.array([end_node, lane_1_y])
    curr_node_lane_1_pos = np.average(np.array([start_node_lane_1, end_node_lane_1]), axis=0)
    full_map_key_list.append(curr_node_lane_1_pos)
    curr_node_lane_1_index = len(full_map_key_list) - 1
    curr_node_lane_1 = LaneNode(None)
    curr_node_lane_1.pos_x, curr_node_lane_1.pos_y = curr_node_lane_1_pos[0], curr_node_lane_1_pos[1]
    full_map[curr_node_lane_1_index] = curr_node_lane_1
    # Connect left and right edges
    full_map[curr_node_lane_0_index].left = curr_node_lane_1_index
    full_map[curr_node_lane_1_index].right = curr_node_lane_0_index

    # Top lanes
    # Lane 2
    start_node_lane_2 = np.array([start_node, lane_2_y])
    end_node_lane_2 = np.array([end_node, lane_2_y])
    curr_node_lane_2_pos = np.average(np.array([start_node_lane_2, end_node_lane_2]), axis=0)
    full_map_key_list.append(curr_node_lane_2_pos)
    curr_node_lane_2_index = len(full_map_key_list) - 1
    curr_node_lane_2 = LaneNode(None)
    curr_node_lane_2.pos_x, curr_node_lane_2.pos_y = curr_node_lane_2_pos[0], curr_node_lane_2_pos[1]
    full_map[curr_node_lane_2_index] = curr_node_lane_2
    # Lane 3
    start_node_lane_3 = np.array([start_node, lane_3_y])
    end_node_lane_3 = np.array([end_node, lane_3_y])
    curr_node_lane_3_pos = np.average(np.array([start_node_lane_3, end_node_lane_3]), axis=0)
    full_map_key_list.append(curr_node_lane_3_pos)
    curr_node_lane_3_index = len(full_map_key_list) - 1
    curr_node_lane_3 = LaneNode(None)
    curr_node_lane_3.pos_x, curr_node_lane_3.pos_y = curr_node_lane_3_pos[0], curr_node_lane_3_pos[1]
    full_map[curr_node_lane_3_index] = curr_node_lane_3
    # Connect left and right edges
    full_map[curr_node_lane_3_index].left = curr_node_lane_2_index
    full_map[curr_node_lane_2_index].right = curr_node_lane_3_index

    # Save current nodes for future pred/succ connections 
    prev_node_lane_0_index = curr_node_lane_0_index
    prev_node_lane_1_index = curr_node_lane_1_index
    prev_node_lane_2_index = curr_node_lane_2_index
    prev_node_lane_3_index = curr_node_lane_3_index

    # Update start x position for next node
    start_node = end_node

    # Iterate over rest of the road in the x direction
    while start_node < W:

        end_node = start_node + map_resolution

        # Bottom lanes
        # Lane 0
        start_node_lane_0 = np.array([start_node, lane_0_y])
        end_node_lane_0 = np.array([end_node, lane_0_y])
        curr_node_lane_0_pos = np.average(np.array([start_node_lane_0, end_node_lane_0]), axis=0)
        full_map_key_list.append(curr_node_lane_0_pos)
        curr_node_lane_0_index = len(full_map_key_list) - 1
        curr_node_lane_0 = LaneNode(None)
        curr_node_lane_0.pos_x, curr_node_lane_0.pos_y = curr_node_lane_0_pos[0], curr_node_lane_0_pos[1]
        full_map[curr_node_lane_0_index] = curr_node_lane_0
        # Lane 1
        start_node_lane_1 = np.array([start_node, lane_1_y])
        end_node_lane_1 = np.array([end_node, lane_1_y])
        curr_node_lane_1_pos = np.average(np.array([start_node_lane_1, end_node_lane_1]), axis=0)
        full_map_key_list.append(curr_node_lane_1_pos)
        curr_node_lane_1_index = len(full_map_key_list) - 1
        curr_node_lane_1 = LaneNode(None)
        curr_node_lane_1.pos_x, curr_node_lane_1.pos_y = curr_node_lane_1_pos[0], curr_node_lane_1_pos[1]
        full_map[curr_node_lane_1_index] = curr_node_lane_1
        # Connect left and right edges and pred edges to previous nodes
        full_map[curr_node_lane_0_index].left = curr_node_lane_1_index
        full_map[curr_node_lane_0_index].pred = prev_node_lane_0_index
        full_map[curr_node_lane_1_index].right = curr_node_lane_0_index
        full_map[curr_node_lane_1_index].pred = prev_node_lane_1_index
        # Connect succ edges from previous nodes
        full_map[prev_node_lane_0_index].succ = [curr_node_lane_0_index]
        full_map[prev_node_lane_1_index].succ = [curr_node_lane_1_index]

        # Top lanes
        # Lane 2
        start_node_lane_2 = np.array([start_node, lane_2_y])
        end_node_lane_2 = np.array([end_node, lane_2_y])
        curr_node_lane_2_pos = np.average(np.array([start_node_lane_2, end_node_lane_2]), axis=0)
        full_map_key_list.append(curr_node_lane_2_pos)
        curr_node_lane_2_index = len(full_map_key_list) - 1
        curr_node_lane_2 = LaneNode(None)
        curr_node_lane_2.pos_x, curr_node_lane_2.pos_y = curr_node_lane_2_pos[0], curr_node_lane_2_pos[1]
        full_map[curr_node_lane_2_index] = curr_node_lane_2
        # Lane 3
        start_node_lane_3 = np.array([start_node, lane_3_y])
        end_node_lane_3 = np.array([end_node, lane_3_y])
        curr_node_lane_3_pos = np.average(np.array([start_node_lane_3, end_node_lane_3]), axis=0)
        full_map_key_list.append(curr_node_lane_3_pos)
        curr_node_lane_3_index = len(full_map_key_list) - 1
        curr_node_lane_3 = LaneNode(None)
        curr_node_lane_3.pos_x, curr_node_lane_3.pos_y = curr_node_lane_3_pos[0], curr_node_lane_3_pos[1]
        full_map[curr_node_lane_3_index] = curr_node_lane_3
        # Connect left and right edges and succ edges to previous nodes
        full_map[curr_node_lane_3_index].left = curr_node_lane_2_index
        full_map[curr_node_lane_3_index].succ = [prev_node_lane_3_index]
        full_map[curr_node_lane_2_index].right = curr_node_lane_3_index
        full_map[curr_node_lane_2_index].succ = [prev_node_lane_2_index]
        # Connect pred edges from previous nodes
        full_map[prev_node_lane_2_index].pred = curr_node_lane_2_index
        full_map[prev_node_lane_3_index].pred = curr_node_lane_3_index

        # Save current nodes for future pred/succ connections 
        prev_node_lane_0_index = curr_node_lane_0_index
        prev_node_lane_1_index = curr_node_lane_1_index
        prev_node_lane_2_index = curr_node_lane_2_index
        prev_node_lane_3_index = curr_node_lane_3_index

        # Update start x position for next node
        start_node = end_node

    # Create storage for final dataset consisting of windows
    DATA_LENGTH = 24
    # 24 = (sequence id, time id within sequence,
    #       vehicle observed boolean, absolute vehicle pos x, y, relative vehicle pos change x, y,
    #       successor node exists boolean, absolute successor node pos x, y, relative successor node pos x, y,
    #       left node exists boolean, absolute left node pos x, y, relative left node pos x, y,
    #       right node exists boolean, absolute right node pos x, y, relative right node pos x, y,
    #       anomaly label, minor manuever label)

    complete_lane_vehicle_data = np.zeros((num_seq, max_agents_in_frame, obs_len, DATA_LENGTH))

    # Iterate over every vehicle at each time step for all windows to fill up data table with lane and pos info
    print('Converting dataset...')
    for sequence_id in tqdm(range(num_seq)):
        for vehicle_id in range(max_agents_in_frame):
            for t_step in range(obs_len):

                # Sequence id
                complete_lane_vehicle_data[sequence_id][vehicle_id][t_step][0] = batch_seq_ids[sequence_id][vehicle_id]

                # Timestep
                complete_lane_vehicle_data[sequence_id][vehicle_id][t_step][1] = batch_frame_ids[sequence_id][vehicle_id][t_step]

                # Every vehicle is observed at every timestep in the dataset
                complete_lane_vehicle_data[sequence_id][vehicle_id][t_step][2] = 1. 
                    # NOTE: This mask should set unseen vehicles to 0 for timesteps where vehicles 
                    #       are unobserved, but the 2-vehicle MAAD dataset has every vehicle observed at every timestep

                # Vehicle absolute x,y position
                complete_lane_vehicle_data[sequence_id][vehicle_id][t_step][3:5] = batch_obs_traj[sequence_id][vehicle_id][t_step]

                # Vehicle relative x,y position change
                complete_lane_vehicle_data[sequence_id][vehicle_id][t_step][5:7] = batch_obs_traj_rel[sequence_id][vehicle_id][t_step]

                # Find lane node index of vehicle
                veh_lane_node_index = find_nearest(full_map_key_list, batch_obs_traj[sequence_id][vehicle_id][t_step])

                # Check if node has succ edge
                if full_map[veh_lane_node_index].succ != None:
                    complete_lane_vehicle_data[sequence_id][vehicle_id][t_step][7] = 1.
                    succ_node_index = full_map[veh_lane_node_index].succ[0]
                    complete_lane_vehicle_data[sequence_id][vehicle_id][t_step][8:10] = np.array([full_map[succ_node_index].pos_x, full_map[succ_node_index].pos_y])
                    succ_node_rel_x = full_map[succ_node_index].pos_x - batch_obs_traj[sequence_id][vehicle_id][t_step][0]
                    succ_node_rel_y = full_map[succ_node_index].pos_y - batch_obs_traj[sequence_id][vehicle_id][t_step][1]
                    complete_lane_vehicle_data[sequence_id][vehicle_id][t_step][10:12] = np.array([succ_node_rel_x, succ_node_rel_y])

                # Check if node has left edge
                if full_map[veh_lane_node_index].left != None:
                    complete_lane_vehicle_data[sequence_id][vehicle_id][t_step][12] = 1.
                    left_node_index = full_map[veh_lane_node_index].left
                    complete_lane_vehicle_data[sequence_id][vehicle_id][t_step][13:15] = np.array([full_map[left_node_index].pos_x, full_map[left_node_index].pos_y])
                    left_node_rel_x = full_map[left_node_index].pos_x - batch_obs_traj[sequence_id][vehicle_id][t_step][0]
                    left_node_rel_y = full_map[left_node_index].pos_y - batch_obs_traj[sequence_id][vehicle_id][t_step][1]
                    complete_lane_vehicle_data[sequence_id][vehicle_id][t_step][15:17] = np.array([left_node_rel_x, left_node_rel_y])

                # Check if node has right edge
                if full_map[veh_lane_node_index].right != None:
                    complete_lane_vehicle_data[sequence_id][vehicle_id][t_step][17] = 1.
                    right_node_index = full_map[veh_lane_node_index].right
                    complete_lane_vehicle_data[sequence_id][vehicle_id][t_step][18:20] = np.array([full_map[right_node_index].pos_x, full_map[right_node_index].pos_y])
                    right_node_rel_x = full_map[right_node_index].pos_x - batch_obs_traj[sequence_id][vehicle_id][t_step][0]
                    right_node_rel_y = full_map[right_node_index].pos_y - batch_obs_traj[sequence_id][vehicle_id][t_step][1]
                    complete_lane_vehicle_data[sequence_id][vehicle_id][t_step][20:22] = np.array([right_node_rel_x, right_node_rel_y])

                # Label as anomaly or not, and minor maneuver label
                complete_lane_vehicle_data[sequence_id][vehicle_id][t_step][22:24] = batch_labels[sequence_id][vehicle_id][t_step]
    
    # Normalize dataset
    if args.norm:
        # Absolute position indices in obs that need to be normalized
        x_filter_indices = [3, 8, 13, 18]
        y_filter_indices = [4, 9, 14, 19]
        # Normalize absolute positions to [0, 1]
        complete_lane_vehicle_data[:,:,:,x_filter_indices] /= W
        complete_lane_vehicle_data[:,:,:,y_filter_indices] /= H

    # Save complete_lane_vehicle_data to data file 
    save_dir = os.path.join('data', args.run_name, 'maad', args.split, '0')
    os.makedirs(save_dir, exist_ok=True)
    data_file = os.path.join(save_dir, '0.npy')
    with open(data_file, 'wb') as f:
        np.save(f, complete_lane_vehicle_data)
    print('Converted data saved to: ', data_file)
