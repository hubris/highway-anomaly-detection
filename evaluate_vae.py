import os
import sys
import pickle
import csv

import numpy as np
import configargparse
from tqdm import tqdm
import torch
import torch.nn.functional as F

from utils.data_loader import loadDataset
from models.vae import Attenuated_Recurrent_VAE

ENVIRONMENT = ['maad']
SPLIT = ['val', 'test']


if __name__ == '__main__':

    p = configargparse.ArgumentParser(
        prog='SABeR-VAE Evaluator',
        description='Evaluates SABeR-VAE models to reconstruct trajectories from a given test dataset.',
        formatter_class=configargparse.ArgumentDefaultsHelpFormatter
    )

    ###########
    # Dataset #
    ###########

    # Name of CSV File
    p.add('--eval_name', type=str, metavar='EVAL_NAME', required=True,
          help='Name of CSV file to save with evaluation results.')

    # Environment Name
    p.add('--env', type=str, metavar='ENV', required=True, choices=ENVIRONMENT,
          help='Name of dataset to load.')

    # Dataset Split
    p.add('--split', type=str, metavar='SPLIT', required=True, choices=SPLIT,
          help='Name of dataset split to load from <test/val>.')

    # Run Name to Load Data From
    p.add('--data_run_name', type=str, metavar='DATA_RUN_NAME', required=True,
          help='Name of run subfolder to load from data directory.')

    # Checkpoint Number of Model to Test
    p.add('--checkpoint', type=int, metavar='CHECKPOINT', default=-1, required=False,
          help='Checkpoint number of model to test. Set as -1 to test the final checkpoint.')

    # Number of Data Loading Workers
    p.add('--num_workers', type=int, metavar='WORK', default=0,
                help='Number of workers used in data loader.')

    # Test on GPU
    p.add('--gpu', action='store_true', default=False,
                help='Set device for PyTorch as GPU if available.')

    #########
    # Model #
    #########

    # Run Name to Load Model From
    p.add('--run_name', type=str, metavar='RUN_NAME', required=True, nargs='+',
          help='Name of run subfolder to load from training directory.')

    args = p.parse_args()


    header = ['run_name', 'run_num', 'checkpoint', 'batch_size', 'learning_rate', 'pre_kl_weight', 'post_kl_weight',        \
              'latent_dim', 'vva_dropout', 'lva_dropout', 'enc_gru_dropout', 'vva_dim', 'lva_dim', 'enc_gru_dim',           \
              'avg_normal_recon_loss', 'min_normal_recon_loss', 'max_normal_recon_loss', 'variance_normal_recon_loss',      \
              'avg_anomaly_recon_loss', 'min_anomaly_recon_loss', 'max_anomaly_recon_loss', 'variance_anomaly_recon_loss',  \
              'avg_normal_pred_loss', 'min_normal_pred_loss', 'max_normal_pred_loss', 'variance_normal_pred_loss',          \
              'avg_anomaly_pred_loss', 'min_anomaly_pred_loss', 'max_anomaly_pred_loss', 'variance_anomaly_pred_loss'       ]

    os.makedirs('eval_results', exist_ok=True)
    csv_path = os.path.join('eval_results', '{}.csv'.format(args.eval_name))

    with open(csv_path, 'w', encoding='UTF8', newline='') as f:
        writer = csv.writer(f)

        # write the header
        writer.writerow(header)

        # Directory with dataset
        data_dir = os.path.join("data", args.data_run_name, args.env)

        # Get dataset of trajectories for environment with maximum batch size to load complete data to RAM
        data_generator = loadDataset(   split=args.split, 
                                        batch_size=sys.maxsize, 
                                        num_workers=args.num_workers, 
                                        drop_last=False, 
                                        load_dir=data_dir,
                                        allow_shuffle=False)

        # Set device
        device = torch.device("cuda" if args.gpu else "cpu")
        print("Using device:", device)

        # Iterate over model run names
        for model_run_name in args.run_name:

            # Get path to set of run iterations
            run_dir = os.path.join("pretrained", model_run_name, args.env)

            # Iterate over run iterations
            run_num = 0
            while True:
                # Get folder for where model will be loaded from
                model_dir = os.path.join(run_dir, str(run_num))
                if not os.path.exists(model_dir):
                    break
                print('model directory to load from: ', model_dir)

                # Get arguments for how model was trained
                trained_args_path = os.path.join(model_dir, "model_args.pickle")
                with open(trained_args_path, "rb") as f:
                    trained_args = pickle.load(f)

                # Load checkpoint
                checkpoint_path = os.path.join(model_dir, "checkpoints", '{}.pt'.format('final' if args.checkpoint == -1 else args.checkpoint))
                checkpoint = torch.load(checkpoint_path, map_location=device)

                # Create VAE model
                vae_model = Attenuated_Recurrent_VAE(device, trained_args)
                vae_model.load_state_dict(checkpoint['model'])
                vae_model = vae_model.to(device)
                vae_model.eval()

                with torch.no_grad():

                    # Get dataset
                    for i_batch, data in enumerate(tqdm(data_generator)):

                        # Move data to device
                        data = data[:,:trained_args.max_veh,:trained_args.seq_len].float().to(device)   # (dataset size, max_vehicles, seq_len, 15)

                        # Observation mask of which cars were observable at which time step for each episode in the batch
                        observed_mask = data[:,:trained_args.max_veh,:trained_args.seq_len,2].bool()    # (dataset size, max_vehicles, seq_len)

                        # Ground truth absolute and relative change x,y positions of each car at each time
                        pos = data[:,:trained_args.max_veh,:trained_args.seq_len,3:5]       # (dataset size, max_vehicles, seq_len, 2)
                        rel_pos = data[:,:trained_args.max_veh,:trained_args.seq_len,5:7]   # (batch size, max_vehicles, seq_len, 2)

                        # Ground truth x,y positions of the discretized lane nodes of each car at each time
                        lane_succ = data[:,:trained_args.max_veh,:trained_args.seq_len,10:12].unsqueeze(3)  # (dataset size, max_vehicles, seq_len, 1, 2)
                        lane_left = data[:,:trained_args.max_veh,:trained_args.seq_len,15:17].unsqueeze(3)  # (dataset size, max_vehicles, seq_len, 1, 2)
                        lane_right = data[:,:trained_args.max_veh,:trained_args.seq_len,20:22].unsqueeze(3) # (dataset size, max_vehicles, seq_len, 1, 2)
                        lane_pos = torch.cat((lane_succ, lane_left, lane_right), 3)                         # (dataset size, max_vehicles, seq_len, 3, 2)

                        # Mask of which lane edges exist for each center lane node at each time
                        succ_lane_exist_mask = data[:,:trained_args.max_veh,:trained_args.seq_len,7].bool().unsqueeze(3)    # (dataset size, max_vehicles, seq_len, 1)
                        left_lane_exist_mask = data[:,:trained_args.max_veh,:trained_args.seq_len,12].bool().unsqueeze(3)   # (dataset size, max_vehicles, seq_len, 1)
                        right_lane_exist_mask = data[:,:trained_args.max_veh,:trained_args.seq_len,17].bool().unsqueeze(3)  # (dataset size, max_vehicles, seq_len, 1)
                        lane_exist_mask = torch.cat((succ_lane_exist_mask, left_lane_exist_mask, right_lane_exist_mask), 3) # (dataset size, max_vehicles, seq_len, 3)

                        # Create key/val source from vehicle positions with shape (dataset size, max_vehicles, seq_len, max_vehicles, pos_dim) by repeating veh_pos_input
                        # Permute veh_pos_input so that sequence length dimension is outside of repeating area
                        vva_permute_kvs = pos.permute((0, 2, 1, 3))                                 # (dataset size, seq_len, max_vehicles, pos_dim)
                        # Unsqueeze to add dimension to repeat
                        vva_unsqueeze_kvs = vva_permute_kvs.unsqueeze(2)                            # (dataset size, seq_len, 1, max_vehicles, pos_dim)
                        # Repeat by the number of vehicles 
                        vva_repeat_kvs = vva_unsqueeze_kvs.repeat(1, 1, trained_args.max_veh, 1, 1) # (dataset size, seq_len, max_vehicles, max_vehicles, pos_dim)
                        # Unpermute so that sequence length dimension is back to where it was
                        vva_kvs = vva_repeat_kvs.permute(0, 2, 1, 3, 4)                             # (dataset size, max_vehicles, seq_len, max_vehicles, pos_dim)

                        # Based on observation mask
                        # Permute observed_mask so that sequence length dimension is outside of repeating area
                        vva_permute_obs_mask = observed_mask.permute((0, 2, 1))                             # (dataset size, seq_len, max_vehicles)
                        # Unsqueeze to add dimension to repeat
                        vva_unsqueeze_obs_mask = vva_permute_obs_mask.unsqueeze(2)                          # (dataset size, seq_len, 1, max_vehicles)
                        # Repeat by the number of vehicles 
                        vva_repeat_obs_mask = vva_unsqueeze_obs_mask.repeat(1, 1, trained_args.max_veh, 1)  # (dataset size, seq_len, max_vehicles, max_vehicles)
                        # Unpermute so that sequence length dimension is back to where it was
                        vva_unpermute_obs_mask = vva_repeat_obs_mask.permute(0, 2, 1, 3)                    # (dataset size, max_vehicles, seq_len, max_vehicles)
                        # Unsqueeze vva_unpermute_mask to follow convention of Attention module's forward function
                        vva_obs_mask = vva_unpermute_obs_mask.unsqueeze(4)                                  # (dataset size, max_vehicles, seq_len, max_vehicles, 1)

                        # Based on vehicles' distance to other vehicles
                        # Unsqueeze to add dimension to repeat
                        vva_unsqueeze_dist_mask = pos.unsqueeze(3)                                              # (dataset size, max_vehicles, seq_len, 1, pos_dim)
                        # Repeat by number of vehicles
                        vva_repeat_dist_mask = vva_unsqueeze_dist_mask.repeat(1, 1, 1, trained_args.max_veh, 1) # (dataset size, max_vehicles, seq_len, max_vehicles, pos_dim)
                        # Find difference between positions of each vehicle
                        vva_diff_dist_mask = vva_kvs - vva_repeat_dist_mask                                     # (dataset size, max_vehicles, seq_len, max_vehicles, pos_dim)
                        # Calculate distance norms
                        vva_norm_dist_mask = torch.linalg.norm(vva_diff_dist_mask, dim=4, keepdim=True)         # (dataset size, max_vehicles, seq_len, max_vehicles, 1)
                        # Filter out distances greater than threshold
                        vva_dist_mask = (vva_norm_dist_mask <= trained_args.veh_dist_mask).bool()               # (dataset size, max_vehicles, seq_len, max_vehicles, 1)

                        # Combine observation and distance masks
                        vva_mask = torch.logical_and(vva_obs_mask, vva_dist_mask)   # (dataset size, max_vehicles, seq_len, max_vehicles, 1)

                        # Normalize VVA inter-vehicle distance
                        if trained_args.vva_norm:
                            vva_diff_dist_mask /= trained_args.veh_dist_mask

                        # Forward pass through VAE model
                        recon_traj_torch, pred_traj_torch, recon_mean_traj_torch, pred_mean_traj_torch,             \
                        recon_var_traj_torch, pred_var_traj_torch, recon_latent_traj_torch, pred_latent_traj_torch, \
                        vva_traj_torch, lva_traj_torch, koopman_mean_traj_torch, koopman_var_traj_torch =           \
                            vae_model(rel_pos, vva_diff_dist_mask, vva_mask, lane_pos, lane_exist_mask)

                        # Calculate loss between ground truth and reconstruction/prediction trajectory points
                        batched_gt_curr_pos = rel_pos[:,:,:-1].reshape((-1,2))      # (dataset size * max_vehicles * (seq_len - 1), pos dim)
                        batched_gt_next_pos = rel_pos[:,:,1:].reshape((-1,2))       # (dataset size * max_vehicles * (seq_len - 1), pos dim)
                        batched_recon_pos = recon_traj_torch[:,:-1].reshape((-1,2)) # (dataset size * max_vehicles * (seq_len - 1), pos dim)
                        batched_pred_pos = pred_traj_torch[:,:-1].reshape((-1,2))   # (dataset size * max_vehicles * (seq_len - 1), pos dim)

                        recon_loss = torch.mean(F.mse_loss( batched_recon_pos, batched_gt_curr_pos, reduction='none'), dim=-1)  # (dataset size * max_vehicles * (seq_len - 1))
                        pred_loss = torch.mean(F.mse_loss( batched_pred_pos, batched_gt_next_pos, reduction='none'), dim=-1)    # (dataset size * max_vehicles * (seq_len - 1))

                        # Anomaly labels
                        labels = data[:,:trained_args.max_veh,:trained_args.seq_len,22] # (dataset size, max_vehicles, seq_len)
                        batched_gt_curr_labels = labels[:,:,:-1].reshape((-1))          # (dataset size * max_vehicles * (seq_len - 1))
                        batched_gt_next_labels = labels[:,:,1:].reshape((-1))           # (dataset size * max_vehicles * (seq_len - 1))

                        # Apply label masks to loss tensors
                        normal_recon = recon_loss[batched_gt_curr_labels == 0]
                        anomaly_recon = recon_loss[batched_gt_curr_labels == 1]
                        normal_pred = pred_loss[batched_gt_next_labels == 0]
                        anomaly_pred = pred_loss[batched_gt_next_labels == 1]

                        # Calculate avg/min/max/variance of losses
                        avg_normal_recon_loss = torch.mean(normal_recon).item()
                        min_normal_recon_loss = torch.min(normal_recon).item()
                        max_normal_recon_loss = torch.max(normal_recon).item()
                        variance_normal_recon_loss = torch.var(normal_recon).item()

                        avg_anomaly_recon_loss = torch.mean(anomaly_recon).item()
                        min_anomaly_recon_loss = torch.min(anomaly_recon).item()
                        max_anomaly_recon_loss = torch.max(anomaly_recon).item()
                        variance_anomaly_recon_loss = torch.var(anomaly_recon).item()

                        avg_normal_pred_loss = torch.mean(normal_pred).item()
                        min_normal_pred_loss = torch.min(normal_pred).item()
                        max_normal_pred_loss = torch.max(normal_pred).item()
                        variance_normal_pred_loss = torch.var(normal_pred).item()

                        avg_anomaly_pred_loss = torch.mean(anomaly_pred).item()
                        min_anomaly_pred_loss = torch.min(anomaly_pred).item()
                        max_anomaly_pred_loss = torch.max(anomaly_pred).item()
                        variance_anomaly_pred_loss = torch.var(anomaly_pred).item()

                        # Add data to CSV sheet
                        row_data = [model_run_name, run_num, trained_args.num_epochs if args.checkpoint == -1 else args.checkpoint,         \
                                    trained_args.batch_size, trained_args.lr, trained_args.pre_kl_beta, trained_args.post_kl_beta,          \
                                    trained_args.vae_latent, trained_args.vva_drop, trained_args.lva_drop, trained_args.enc_gru_drop,       \
                                    trained_args.vva_out, trained_args.lva_out, trained_args.enc_gru_hid,                                   \
                                    avg_normal_recon_loss, min_normal_recon_loss, max_normal_recon_loss, variance_normal_recon_loss,        \
                                    avg_anomaly_recon_loss, min_anomaly_recon_loss, max_anomaly_recon_loss, variance_anomaly_recon_loss,    \
                                    avg_normal_pred_loss, min_normal_pred_loss, max_normal_pred_loss, variance_normal_pred_loss,            \
                                    avg_anomaly_pred_loss, min_anomaly_pred_loss, max_anomaly_pred_loss, variance_anomaly_pred_loss         ]

                        writer.writerow(row_data)

                # Test next model iteration
                run_num += 1
