import torch
import torch.nn as nn
import torch.nn.functional as F
from models.vae import Vehicle_Self_Attention, Lane_Vehicle_Attention, Decoder, MLP


class Encoder(nn.Module):
    """
    Encoder of AE with GRU architecture.
    """

    def __init__(self, device, args):
        """
        Initialize encoder.

        :param device:  cpu or gpu
        :param args:    training args configuration file
        """

        super(Encoder, self).__init__()

        self.args = args
        self.device = device

        # Embedding network for VVA output
        self.embedding = nn.Sequential( nn.Linear(in_features=self.args.vva_out,
                                                  out_features=self.args.enc_gru_hid),
                                        nn.ReLU(), 
                                        nn.Linear(in_features=self.args.enc_gru_hid,
                                                  out_features=self.args.enc_gru_hid),
                                        nn.ReLU())

        # GRU encoder
        self.rnn = nn.GRU(  input_size=self.args.enc_gru_hid,  
                            hidden_size=self.args.enc_gru_hid, 
                            num_layers=self.args.enc_gru_layers, 
                            dropout=self.args.enc_gru_drop, 
                            bidirectional=self.args.enc_gru_bidir)

        # MLP to generate latent point
        self.mlp = nn.Sequential(   nn.Linear(in_features=self.args.enc_gru_hid,
                                              out_features=self.args.vae_latent),
                                    nn.ReLU(), 
                                    nn.Linear(in_features=self.args.vae_latent,
                                              out_features=self.args.vae_latent))

        # Initialize weights
        for name, param in self.rnn.named_parameters():
            if 'bias' in name:
                 nn.init.constant_(param, 0.0)
            elif 'weight_ih' in name:
                 nn.init.kaiming_normal_(param)
            elif 'weight_hh' in name:
                 nn.init.orthogonal_(param)

    def forward(self, att_input, hidden=None):
        """
        Encode trajectory point.

        :param att_input:   dim - (batch * num vehicles, args.vva_out)
            current time vva attenuated trajectory point        
        :param hidden:      dim - (batch * num vehicles, args.enc_gru_hid)
            hidden state of GRU to propagate from past (None if start of sequence)

        :output out:        dim - (batch * num vehicles, args.vae_latent)
            latent point
        :output gru_hidden: dim - (num_layers * num_directions, batch * num vehicles, args.enc_gru_hid)
            hidden state of GRU network
        """

        # Embed input attenuated trajectory point
        embed_output = self.embedding(att_input).unsqueeze(0)       # (1, batch size * num vehicles, args.enc_gru_hid)

        # Pass embedding through GRU
        if hidden == None:
            gru_output, gru_hidden = self.rnn(embed_output)
        else:
            gru_output, gru_hidden = self.rnn(embed_output, hidden) # (1, batch size * num vehicles, args.enc_gru_hid)
                                                                    # (1, batch size * num vehicles, args.enc_gru_hid)

        # Pass hidden states through mean and variance MLPs
        out = self.mlp(gru_output)                                  # (1, batch size * num vehicles, args.vae_latent)

        return out.squeeze(0), gru_hidden


class Attenuated_Recurrent_AE(nn.Module):
    """
    Complete AE architecture.
    """

    def __init__(self, device, args):
        """
        Initialize AE.

        :param device:  cpu or gpu
        :param args:    training args configuration file
        """

        super(Attenuated_Recurrent_AE, self).__init__()
        
        self.args = args
        self.device = device

        # VVA layer
        if args.vva:
            self.vva_layer = Vehicle_Self_Attention(device, args)
        else:
            self.vva_layer = MLP(device, 2, args.vva_out)
      
        # GRU encoder
        self.gru_enc = Encoder(device, args)
    
        # LVA layer
        if args.lva:
            self.lva_layer = Lane_Vehicle_Attention(device, args)
            prop_in = args.vae_latent + args.lva_out
        else:
            prop_in = args.vae_latent
      
        # Latent propagation network
        self.propagation = nn.Sequential(   nn.Linear(prop_in, 256), nn.ReLU(),     \
                                            nn.Linear(256, 128), nn.ReLU(),         \
                                            nn.Linear(128, self.args.vae_latent))

        # Decoder
        self.dec = Decoder(device, args)

    def forward(self, pos, rel_pos, vva_mask, lane_pos, lane_exist_mask):
        """
        Reconstruct and predict one-step trajectories with AE.

        :param pos:                                                 dim - (batch, num vehicles, seq len, pos dim)
            ground truth x,y positions of each car at each time
        :param rel_pos:                                             dim - (batch, num vehicles, seq len, num vehicles, pos dim)
            distances between vehicles on road at each time
        :param vva_mask:                                            dim - (batch, num vehicles, seq len, num vehicles, 1)
            boolean mask of if one vehicle observes another vehicle for VVA
            entry False if vehicle is too far from the other, 
                or vehicle being observed is not on road
        :param lane_pos:                                            dim - (batch, num vehicles, seq len, 3, pos dim)
            ground truth x,y positions of the discretized 
                lane nodes of each car at each time
            3 lane nodes correspond to successor, left, and right of cars
        :param lane_exist_mask:                                     dim - (batch, num vehicles, seq len, 3)
            mask of which lane edges exist for each center lane node at each time
            entry False where node does not exist for that car

        :output recon_traj_torch, pred_traj_torch:                  dim - (batch_size * num vehicles, seq len, pos dim)
            position trajectories output by model
        :output recon_latent_traj_torch, pred_latent_traj_torch:    dim - (batch_size * num vehicles, seq len, args.vae_latent)
            sampled latent points of reconstruction and prediction distributions
        :output vva_traj_torch:                                     dim - (batch_size * num vehicles, seq len, num vehicles)
            VVA attention weights
        :output lva_traj_torch:                                     dim - (batch_size * num vehicles, seq len, 3)
            LVA attention weights
        """

        # Create lists to return sequences
        recon_traj, pred_traj = [], []                  # position trajectories
        recon_latent_traj, pred_latent_traj = [], []    # sampled latent points of reconstruction and prediction distributions
        vva_traj, lva_traj = [], []                     # vva and lva attention weights
        
        # Initialize GRU hidden state
        enc_hidden = None

        # Iterate over sequence
        for curr_time in range(self.args.seq_len):

            # Access masks for current time step
            curr_pos = pos[:,:self.args.max_veh,curr_time]                      # (batch size, max_vehicles, 2)
            curr_rel_pos = rel_pos[:,:self.args.max_veh,curr_time]              # (batch size, max_vehicles, max_vehicles, 2)
            curr_mask = ~vva_mask[:,:,curr_time,:,0]                            # (batch size, max_vehicles, max_vehicles)
            curr_lane = lane_pos[:,:self.args.max_veh,curr_time]                # (batch size, max_vehicles, 3, 2)
            curr_lane_mask = ~lane_exist_mask[:,:self.args.max_veh,curr_time]   # (batch size, max_vehicles, 3)

            # Vehicle-Vehicle Attention
            if self.args.vva:
                vva_attn_out, vva_attn_weights = self.vva_layer(curr_pos, curr_rel_pos, curr_mask)  # (batch size, max_vehicles, args.vva_out)
                                                                                                    # (batch size, max_vehicles, max_vehicles)
            else:
                vva_attn_out = self.vva_layer(curr_pos)
                vva_attn_weights = None

            # Encode current time step features
            enc_att_in = vva_attn_out.reshape((-1,self.args.vva_out))   # (batch size * max_vehicles, args.vva_out)

            curr_lat_vec, enc_hidden = self.gru_enc(enc_att_in, enc_hidden) # (batch size * max_vehicles, args.vae_latent)
                                                                            # (num_layers * num_directions, batch * max_vehicles, args.enc_gru_hid)

            # Lane-Vehicle Attention
            if self.args.lva:
                lva_attn_out, lva_attn_weights = self.lva_layer(curr_pos, curr_lane, curr_lane_mask)    # (batch size, max_vehicles, args.lva_out)
                                                                                                        # (batch size, max_vehicles, 3)

            # Propagate current time latent point forward
            curr_lat_vec_reshape = curr_lat_vec.reshape((-1,self.args.max_veh,self.args.vae_latent))    # (batch size, max_vehicles, args.vae_latent)
            
            if self.args.lva:
                prop_in = torch.cat([curr_lat_vec_reshape, lva_attn_out], -1)   # (batch size, max_vehicles, args.vae_latent + args.lva_out)
            else:
                prop_in = curr_lat_vec_reshape                                  # (batch size, max_vehicles, args.vae_latent)
            
            next_lat_vec_reshape = self.propagation(prop_in)                        # (batch size, max_vehicles, args.vae_latent)
            next_lat_vec = next_lat_vec_reshape.reshape((-1,self.args.vae_latent))  # (batch size * max_vehicles, args.vae_latent)

            # Reconstruct current points with decoder
            curr_recon = self.dec(curr_lat_vec) # (batch_size * max_vehicles, 2)

            # Predict next points with decoder
            next_pred = self.dec(next_lat_vec)  # (batch_size * max_vehicles, 2)

            # Append trajectory points to sequence lists
            recon_traj.append(curr_recon.unsqueeze(1))  # unsqueezing to add sequence dimension to tensors
            pred_traj.append(next_pred.unsqueeze(1))
            recon_latent_traj.append(curr_lat_vec.unsqueeze(1))
            pred_latent_traj.append(next_lat_vec.unsqueeze(1))
            if self.args.vva:
                vva_traj.append(vva_attn_weights.reshape((-1,self.args.max_veh)).unsqueeze(1))
            if self.args.lva:
                lva_traj.append(lva_attn_weights.reshape((-1,3)).unsqueeze(1))

        # Create torch tensors for returning
        recon_traj_torch = torch.cat(recon_traj, dim=1)                 # (batch_size * max_vehicles, seq len, pos dim)
        pred_traj_torch = torch.cat(pred_traj, dim=1)                   # (batch_size * max_vehicles, seq len, pos dim)
        recon_latent_traj_torch = torch.cat(recon_latent_traj, dim=1)   # (batch_size * max_vehicles, seq len, args.vae_latent)
        pred_latent_traj_torch = torch.cat(pred_latent_traj, dim=1)     # (batch_size * max_vehicles, seq len, args.vae_latent)
        if self.args.vva:
            vva_traj_torch = torch.cat(vva_traj, dim=1)                 # (batch_size * max_vehicles, seq len, max_vehicles)
        else:
            vva_traj_torch = None
        if self.args.lva:
            lva_traj_torch = torch.cat(lva_traj, dim=1)                 # (batch_size * max_vehicles, seq len, 3)
        else:
            lva_traj_torch = None

        return  recon_traj_torch, pred_traj_torch, recon_latent_traj_torch, \
                pred_latent_traj_torch, vva_traj_torch, lva_traj_torch

    def loss(self, obs_mask, gt_traj, pred_curr_traj, pred_future_traj):
        """
        Calculate loss between predicted and ground truth trajectories, and
            apply latent space gaussian regularization.

        :param obs_mask:                    dim - (batch * num vehicles, seq_len)
            observation mask of which cars were observable at which time step 
                for each episode in the batch
        :param gt_traj:                     dim - (batch * num vehicles, seq_len, pos dim)
            ground truth x,y positions of each car at each time
        :param pred_curr_traj:              dim - (batch * num vehicles, seq_len, pos dim)
            model reconstructed x,y current positions of each car
        :param pred_future_traj:            dim - (batch * num vehicles, seq_len, pos dim)
            model predicted x,y one-step future positions of each car

        :output total loss:                 current reconstruction loss + future prediction loss + weighted KL divergence
        :output reconstruction_loss_curr:   MSE reconstruction loss
        :output reconstruction_loss_next:   MSE prediction loss
        """

        # Flatten vectors for masking observed vehicles and calculating MSE reconstruction loss and KL divergence
        obs_mask_flat_curr = obs_mask[:,:-1].reshape((-1, 1))                   # current observation mask          (batch size * max_vehicles * (seq_len - 1), 1)
        obs_mask_flat_next = obs_mask[:,1:].reshape((-1, 1))                    # one-step future observation mask  (batch size * max_vehicles * (seq_len - 1), 1)
        gt_traj_curr = gt_traj[:,:-1].reshape((-1, 2))                          # current ground truth trajectory   (batch size * max_vehicles * (seq_len - 1), pos dim)
        gt_traj_next = gt_traj[:,1:].reshape((-1, 2))                           # one-step future gt trajectory     (batch size * max_vehicles * (seq_len - 1), pos dim)
        pred_traj_curr = pred_curr_traj[:,:-1].reshape((-1, 2))                 # current predicted trajectory      (batch size * max_vehicles * (seq_len - 1), pos dim)
        pred_traj_next = pred_future_traj[:,:-1].reshape((-1, 2))               # future predicted trajectory       (batch size * max_vehicles * (seq_len - 1), pos dim)

        # Mask ground truth and predicted vectors for reconstruction loss
        masked_gt_traj_curr = torch.masked_select(gt_traj_curr, obs_mask_flat_curr).reshape((-1, 2))        # (num_observed, pos dim)
        masked_gt_traj_next = torch.masked_select(gt_traj_next, obs_mask_flat_next).reshape((-1, 2))        # (num_observed, pos dim)
        masked_pred_traj_curr = torch.masked_select(pred_traj_curr, obs_mask_flat_curr).reshape((-1, 2))    # (num_observed, pos dim)
        masked_pred_traj_next = torch.masked_select(pred_traj_next, obs_mask_flat_next).reshape((-1, 2))    # (num_observed, pos dim)

        # Calculate reconstruction and future prediction loss
        reconstruction_loss_curr = F.mse_loss(masked_gt_traj_curr, masked_pred_traj_curr) * 10.
        reconstruction_loss_next = F.mse_loss(masked_gt_traj_next, masked_pred_traj_next) * 10.

        # Calculate complete loss
        total_loss = (reconstruction_loss_curr + reconstruction_loss_next) / 2.

        # Return losses
        return total_loss, reconstruction_loss_curr.item(), reconstruction_loss_next.item()
