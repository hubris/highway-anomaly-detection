import torch
import torch.nn as nn
import torch.nn.functional as F
from models.vae import Vehicle_Self_Attention, Lane_Vehicle_Attention, Encoder


class Decoder(nn.Module):
    """
    Decoder of LSTM VAE with GRU architecture.
    """

    def __init__(self, device, args):
        """
        Initialize decoder.

        :param device:  cpu or gpu
        :param args:    training args configuration file
        """

        super(Decoder, self).__init__()

        self.args = args
        self.device = device

        # Embedding network for concatenation of sampled latent point and LVA output
        self.embedding = nn.Sequential( nn.Linear(in_features=self.args.lva_out+self.args.vae_latent,
                                                  out_features=self.args.enc_gru_hid),
                                        nn.ReLU(), 
                                        nn.Linear(in_features=self.args.enc_gru_hid,
                                                  out_features=self.args.enc_gru_hid),
                                        nn.ReLU())

        # GRU decoder
        self.rnn = nn.GRU(  input_size=self.args.enc_gru_hid,  
                            hidden_size=self.args.enc_gru_hid, 
                            num_layers=self.args.enc_gru_layers, 
                            dropout=self.args.enc_gru_drop, 
                            bidirectional=self.args.enc_gru_bidir)

        # MLP to generate next predicted state
        self.out_mlp = nn.Sequential(   nn.Linear(in_features=self.args.enc_gru_hid,out_features=256), nn.ReLU(),   \
                                        nn.Linear(in_features=256,out_features=128), nn.ReLU(),                     \
                                        nn.Linear(in_features=128,out_features=2),                                  \
                                        )

        # Initialize weights
        for name, param in self.rnn.named_parameters():
            if 'bias' in name:
                 nn.init.constant_(param, 0.0)
            elif 'weight_ih' in name:
                 nn.init.kaiming_normal_(param)
            elif 'weight_hh' in name:
                 nn.init.orthogonal_(param)

    def forward(self, lat_input, att_input, hidden=None):
        """
        Decode trajectory point.

        :param lat_input:   dim - (batch * num vehicles, args.vae_latent)
            current time vva latent point        
        :param att_input:   dim - (batch * num vehicles, args.lva_out)
            current time lva attenuated trajectory point        
        :param hidden:      dim - (batch * num vehicles, args.enc_gru_hid)
            hidden state of GRU to propagate from past (None if start of sequence)

        :output decode_out: dim - (batch_size * num vehicles, 2)
            decoded output
        :output gru_hidden: dim - (num_layers * num_directions, batch * num vehicles, args.enc_gru_hid)
            hidden state of GRU network
        """

        # Embed concatenation of attenuated trajectory point and latent vector
        concat_in = torch.cat([lat_input, att_input], -1)       # (batch size * num vehicles, args.vae_latent + args.lva_out)
        embed_output = self.embedding(concat_in).unsqueeze(0)   # (1, batch size * num vehicles, args.enc_gru_hid)

        # Pass embedding through GRU
        if hidden == None:
            gru_output, gru_hidden = self.rnn(embed_output)
        else:
            gru_output, gru_hidden = self.rnn(embed_output, hidden) # (1, batch size * num vehicles, args.enc_gru_hid)
                                                                    # (1, batch size * num vehicles, args.enc_gru_hid)

        # Pass hidden states through decoder MLP
        decode_out = self.out_mlp(gru_output)   # (1, batch size * num vehicles, 2)

        return decode_out.squeeze(0), gru_hidden


class Attenuated_LSTM_VAE(nn.Module):
    """
    Complete ablation LSTM VAE architecture.
    """

    def __init__(self, device, args):
        """
        Initialize LSTM VAE.

        :param device:  cpu or gpu
        :param args:    training args configuration file
        """

        super(Attenuated_LSTM_VAE, self).__init__()
        
        self.args = args
        self.device = device

        # VVA layer
        self.vva_layer = Vehicle_Self_Attention(device, args)
      
        # GRU encoder
        self.gru_enc = Encoder(device, args)
    
        # LVA layer
        self.lva_layer = Lane_Vehicle_Attention(device, args)

        # Decoder
        self.dec = Decoder(device, args)

    def forward(self, pos, rel_pos, vva_mask, lane_pos, lane_exist_mask):
        """
        Predict one-step trajectories with VAE.

        :param pos:                                                 dim - (batch, num vehicles, seq len, pos dim)
            ground truth x,y positions of each car at each time
        :param rel_pos:                                             dim - (batch, num vehicles, seq len, num vehicles, pos dim)
            distances between vehicles on road at each time
        :param vva_mask:                                            dim - (batch, num vehicles, seq len, num vehicles, 1)
            boolean mask of if one vehicle observes another vehicle for VVA
            entry False if vehicle is too far from the other, 
                or vehicle being observed is not on road
        :param lane_pos:                                            dim - (batch, num vehicles, seq len, 3, pos dim)
            ground truth x,y positions of the discretized 
                lane nodes of each car at each time
            3 lane nodes correspond to successor, left, and right of cars
        :param lane_exist_mask:                                     dim - (batch, num vehicles, seq len, 3)
            mask of which lane edges exist for each center lane node at each time
            entry False where node does not exist for that car

        :output pred_traj_torch:                    dim - (batch_size * num vehicles, seq len, pos dim)
            position trajectories output by model
        :output pred_mean_traj_torch:               dim - (batch_size * num vehicles, seq len, args.vae_latent)
            latent space mean of distributions
        :output pred_var_traj_torch:                dim - (batch_size * num vehicles, seq len, args.vae_latent)
            latent space variance of distributions
        :output pred_latent_traj_torch:             dim - (batch_size * num vehicles, seq len, args.vae_latent)
            sampled latent points of reconstruction distributions
        :output vva_traj_torch:                     dim - (batch_size * num vehicles, seq len, num vehicles)
            VVA attention weights
        :output lva_traj_torch:                     dim - (batch_size * num vehicles, seq len, 3)
            LVA attention weights
        """

        # Create lists to return sequences
        pred_traj = []                          # position trajectories
        pred_mean_traj, pred_var_traj = [], []  # prediction distributions
        pred_latent_traj = []                   # sampled latent points of reconstruction and prediction distributions
        vva_traj, lva_traj = [], []             # vva and lva attention weights

        # Initialize GRU hidden states
        enc_hidden, dec_hidden = None, None

        # Iterate over sequence
        for curr_time in range(self.args.seq_len):

            # Access masks for current time step
            curr_pos = pos[:,:self.args.max_veh,curr_time]                      # (batch size, max_vehicles, 2)
            curr_rel_pos = rel_pos[:,:self.args.max_veh,curr_time]              # (batch size, max_vehicles, max_vehicles, 2)
            curr_mask = ~vva_mask[:,:,curr_time,:,0]                            # (batch size, max_vehicles, max_vehicles)
            curr_lane = lane_pos[:,:self.args.max_veh,curr_time]                # (batch size, max_vehicles, 3, 2)
            curr_lane_mask = ~lane_exist_mask[:,:self.args.max_veh,curr_time]   # (batch size, max_vehicles, 3)

            # Vehicle-Vehicle Attention
            vva_attn_out, vva_attn_weights = self.vva_layer(curr_pos, curr_rel_pos, curr_mask)  # (batch size, max_vehicles, args.vva_out)
                                                                                                # (batch size, max_vehicles, max_vehicles)

            # Encode current time step features
            enc_att_in = vva_attn_out.reshape((-1,self.args.vva_out))   # (batch size * max_vehicles, args.vva_out)

            curr_enc_mean, curr_enc_var, enc_hidden = self.gru_enc(enc_att_in, enc_hidden)  # (batch size * max_vehicles, args.vae_latent)
                                                                                            # (batch size * max_vehicles, args.vae_latent)
                                                                                            # (num_layers * num_directions, batch * max_vehicles, args.enc_gru_hid)

            curr_lat_vec = self.reparameterize(curr_enc_mean, curr_enc_var) # (batch size * max_vehicles, args.vae_latent)

            # Lane-Vehicle Attention
            lva_attn_out, lva_attn_weights = self.lva_layer(curr_pos, curr_lane, curr_lane_mask)    # (batch size, max_vehicles, args.lva_out)
                                                                                                    # (batch size, max_vehicles, 3)

            # Predict next points with decoder
            batch_lva_attn_out = lva_attn_out.reshape((-1,self.args.lva_out))               # (batch size * max_vehicles, args.lva_out)
            next_pred, dec_hidden = self.dec(curr_lat_vec, batch_lva_attn_out, dec_hidden)  # (batch_size * max_vehicles, 2)
                                                                                            # (num_layers * num_directions, batch * max_vehicles, args.enc_gru_hid)

            # Append trajectory points to sequence lists
            pred_traj.append(next_pred.unsqueeze(1))
            pred_mean_traj.append(curr_enc_mean.unsqueeze(1))
            pred_var_traj.append(curr_enc_var.unsqueeze(1))
            pred_latent_traj.append(curr_lat_vec.unsqueeze(1))
            vva_traj.append(vva_attn_weights.reshape((-1,self.args.max_veh)).unsqueeze(1))
            lva_traj.append(lva_attn_weights.reshape((-1,3)).unsqueeze(1))

        # Create torch tensors for returning
        pred_traj_torch = torch.cat(pred_traj, dim=1)               # (batch_size * max_vehicles, seq len, pos dim)
        pred_mean_traj_torch = torch.cat(pred_mean_traj, dim=1)     # (batch_size * max_vehicles, seq len, args.vae_latent)
        pred_var_traj_torch = torch.cat(pred_var_traj, dim=1)       # (batch_size * max_vehicles, seq len, args.vae_latent)
        pred_latent_traj_torch = torch.cat(pred_latent_traj, dim=1) # (batch_size * max_vehicles, seq len, args.vae_latent)
        vva_traj_torch = torch.cat(vva_traj, dim=1)                 # (batch_size * max_vehicles, seq len, max_vehicles)
        lva_traj_torch = torch.cat(lva_traj, dim=1)                 # (batch_size * max_vehicles, seq len, 3)

        return pred_traj_torch, pred_mean_traj_torch, pred_var_traj_torch, pred_latent_traj_torch, vva_traj_torch, lva_traj_torch

    def reparameterize(self, mu, log_var):
        """
        VAE reparameterization trick.

        :param mu:      dim - (batch size, enc_lat_out)
            means
        :param log_var: dim - (batch size, enc_lat_out)
            log variances
        
        :output z:      dim - (batch size, enc_lat_out)
            sampled vectors
        """

        std = torch.exp(0.5 * log_var)
        eps = torch.randn_like(std)
        return mu + eps * std

    def loss(self, obs_mask, gt_traj, pred_future_traj, curr_z_mean, curr_z_var):
        """
        Calculate loss between predicted and ground truth trajectories, and
            apply latent space gaussian regularization.

        :param obs_mask:                    dim - (batch * num vehicles, seq_len)
            observation mask of which cars were observable at which time step 
                for each episode in the batch
        :param gt_traj:                     dim - (batch * num vehicles, seq_len, pos dim)
            ground truth x,y positions of each car at each time
        :param pred_future_traj:            dim - (batch * num vehicles, seq_len, pos dim)
            model predicted x,y one-step future positions of each car
        :param curr_z_mean, curr_z_var:     dim - (batch * num vehicles, seq_len, args.vae_latent)
            model encoded latent mean/variance of current distributions

        :output total_loss:                 future prediction loss + weighted KL divergence
        :output reconstruction_loss_next:   MSE prediction loss
        :output kl_div:                     unweighted KL divergence of current latent distribution
        """

        # Flatten vectors for masking observed vehicles and calculating MSE loss and KL divergence
        obs_mask_flat_curr = obs_mask[:,:-1].reshape((-1, 1))                   # current observation mask          (batch size * max_vehicles * (seq_len - 1), 1)
        obs_mask_flat_next = obs_mask[:,1:].reshape((-1, 1))                    # one-step future observation mask  (batch size * max_vehicles * (seq_len - 1), 1)
        gt_traj_next = gt_traj[:,1:].reshape((-1, 2))                           # one-step future gt trajectory     (batch size * max_vehicles * (seq_len - 1), pos dim)
        pred_traj_next = pred_future_traj[:,:-1].reshape((-1, 2))               # future predicted trajectory       (batch size * max_vehicles * (seq_len - 1), pos dim)
        z_mean_curr = curr_z_mean[:,:-1].reshape((-1, self.args.vae_latent))    # current latent mean               (batch size * max_vehicles * (seq_len - 1), args.vae_latent)
        z_var_curr = curr_z_var[:,:-1].reshape((-1, self.args.vae_latent))      # current latent variance           (batch size * max_vehicles * (seq_len - 1), args.vae_latent)

        # Mask ground truth and predicted vectors for prediction loss
        masked_gt_traj_next = torch.masked_select(gt_traj_next, obs_mask_flat_next).reshape((-1, 2))        # (num_observed, pos dim)
        masked_pred_traj_next = torch.masked_select(pred_traj_next, obs_mask_flat_next).reshape((-1, 2))    # (num_observed, pos dim)

        # Calculate future prediction loss
        reconstruction_loss_next = F.mse_loss(masked_gt_traj_next, masked_pred_traj_next) * 10.

        # Mask current latent mean and variance for KL divergence
        masked_z_mean_curr = torch.masked_select(z_mean_curr, obs_mask_flat_curr).reshape((-1, self.args.vae_latent))   # (num_observed, args.vae_latent)
        masked_z_var_curr = torch.masked_select(z_var_curr, obs_mask_flat_curr).reshape((-1, self.args.vae_latent))     # (num_observed, args.vae_latent)

        # Calculate KL divergence of latent distributions from standard gaussian
        kl_div = torch.mean(-0.5*torch.sum(1 + masked_z_var_curr - masked_z_mean_curr.pow(2) - masked_z_var_curr.exp(), dim=-1))

        # Calculate complete loss
        total_loss = reconstruction_loss_next / 2. + kl_div * self.args.pre_kl_beta

        # Return losses
        return total_loss, reconstruction_loss_next.item(), kl_div.item()
