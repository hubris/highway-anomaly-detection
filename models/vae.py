import torch
import torch.nn as nn
import torch.nn.functional as F


class MLP(nn.Module):
    """
    A standard MLP.
    """

    def __init__(self, device, in_dim, out_dim):
        """
        Initialize MLP.

        :param device:  cpu or gpu
        :param args:    training args configuration file
        """

        super(MLP, self).__init__()

        self.device = device
        self.in_dim = in_dim
        self.out_dim = out_dim

        # Embedding network
        self.embedding = nn.Sequential( nn.Linear(in_features=self.in_dim,
                                                  out_features=self.out_dim),
                                        nn.ReLU(), 
                                        nn.Linear(in_features=self.out_dim,
                                                  out_features=self.out_dim))

    def forward(self, input):
        """
        Embed input.

        :param input:   dim - (batch, num vehicles, in_dim)
            input to encode

        :output out:    dim - (batch, num vehicles, out_dim)
            encoding of input
        """

        output = self.embedding(input)
        return output


class Vehicle_Self_Attention(nn.Module):
    """
    Vehicle-Vehicle Attention Module.
    """

    def __init__(self, device, args):
        """
        Initialize VVA.

        :param device:  cpu or gpu
        :param args:    training args configuration file
        """

        super(Vehicle_Self_Attention, self).__init__()

        self.args = args
        self.device = device

        # Embedding network to produce queries from vehicle positions
        self.embedding = nn.Sequential( nn.Linear(in_features=2,
                                                  out_features=self.args.vva_out),
                                        nn.ReLU(), 
                                        nn.Linear(in_features=self.args.vva_out,
                                                  out_features=self.args.vva_out))

        # Embedding network to produce keys and values from distances between vehicles
        self.rel_embedding = nn.Sequential( nn.Linear(in_features=2,
                                                      out_features=self.args.vva_out),
                                            nn.ReLU(), 
                                            nn.Linear(in_features=self.args.vva_out,
                                                      out_features=self.args.vva_out))

        # Attention layer
        self.vva = nn.MultiheadAttention(   embed_dim=self.args.vva_out, 
                                            num_heads=self.args.vva_heads,
                                            dropout=self.args.vva_drop)

    def forward(self, pos, rel_pos, mask):
        """
        Perform VVA.

        :param pos:             dim - (batch, num vehicles, pos dim)
            real positions of vehicles on road at current time
        :param rel_pos:         dim - (batch, num vehicles, num vehicles, pos dim)
            distances between vehicles on road at current time
        :param mask:            dim - (batch, num vehicles, num vehicles)
            mask of which vehicles to attenuate on (True where should not be attention)

        :output attn_out:       dim - (batch, num vehicles, args.vva_out)
            attenuated output of VVA
        :output attn_weights:   dim - (batch size, num vehicles, num vehicles)
            attention weights between vehicles
        """

        # Embed input positions
        embed_pos_output = self.embedding(pos).reshape((-1, self.args.vva_out)).unsqueeze(0)                                # (1, batch size * num vehicles, args.vva_out)
        embed_rel_output = self.rel_embedding(rel_pos).reshape((-1, self.args.max_veh, self.args.vva_out)).permute((1,0,2)) # (num vehicles, batch size * num vehicles, args.vva_out)

        # Repeat mask for how many heads in VVA
        mask_head = mask.reshape((-1,self.args.max_veh)).unsqueeze(1).unsqueeze(0).repeat((self.args.vva_heads,1,1,1)).permute((1,0,2,3)).reshape((-1,1,self.args.max_veh))
                                                            # (batch size * num vehicles * num heads, 1, num vehicles)
        # VVA
        attn_out, attn_weights = self.vva(embed_pos_output, embed_rel_output, embed_rel_output, attn_mask=mask_head, need_weights=True)
                                                            # (1, batch size * num vehicles, args.vva_out)
                                                            # (batch size * num vehicles, 1, num vehicles)
        
        attn_out = attn_out.permute((1,0,2)).squeeze(1).reshape((-1,self.args.max_veh,self.args.vva_out))
        attn_weights = attn_weights.squeeze(1).reshape((-1,self.args.max_veh,self.args.max_veh))

        return attn_out, attn_weights


class Encoder(nn.Module):
    """
    Encoder of VAE with GRU architecture.
    """

    def __init__(self, device, args):
        """
        Initialize encoder.

        :param device:  cpu or gpu
        :param args:    training args configuration file
        """

        super(Encoder, self).__init__()

        self.args = args
        self.device = device

        # Embedding network for VVA output
        self.embedding = nn.Sequential( nn.Linear(in_features=self.args.vva_out,
                                                  out_features=self.args.enc_gru_hid),
                                        nn.ReLU(), 
                                        nn.Linear(in_features=self.args.enc_gru_hid,
                                                  out_features=self.args.enc_gru_hid),
                                        nn.ReLU())

        # GRU encoder
        self.rnn = nn.GRU(  input_size=self.args.enc_gru_hid,  
                            hidden_size=self.args.enc_gru_hid, 
                            num_layers=self.args.enc_gru_layers, 
                            dropout=self.args.enc_gru_drop, 
                            bidirectional=self.args.enc_gru_bidir)

        # MLP to generate mean of gaussian latent space
        self.mean_mlp = nn.Sequential(  nn.Linear(in_features=self.args.enc_gru_hid,
                                                  out_features=self.args.vae_latent),
                                        nn.ReLU(), 
                                        nn.Linear(in_features=self.args.vae_latent,
                                                  out_features=self.args.vae_latent))

        # MLP to generate variance of gaussian latent space
        self.variance_mlp = nn.Sequential(  nn.Linear(in_features=self.args.enc_gru_hid,
                                                      out_features=self.args.vae_latent),
                                            nn.ReLU(), 
                                            nn.Linear(in_features=self.args.vae_latent,
                                                      out_features=self.args.vae_latent))

        # Initialize weights
        for name, param in self.rnn.named_parameters():
            if 'bias' in name:
                 nn.init.constant_(param, 0.0)
            elif 'weight_ih' in name:
                 nn.init.kaiming_normal_(param)
            elif 'weight_hh' in name:
                 nn.init.orthogonal_(param)

    def forward(self, att_input, hidden=None):
        """
        Encode trajectory point.

        :param att_input:   dim - (batch * num vehicles, args.vva_out)
            current time vva attenuated trajectory point        
        :param hidden:      dim - (batch * num vehicles, args.enc_gru_hid)
            hidden state of GRU to propagate from past (None if start of sequence)

        :output mean:       dim - (batch * num vehicles, args.vae_latent)
            mean of gaussian latent space
        :output variance:   dim - (batch * num vehicles, args.vae_latent)
            log variance of gaussian latent space
        :output gru_hidden: dim - (num_layers * num_directions, batch * num vehicles, args.enc_gru_hid)
            hidden state of GRU network
        """

        # Embed input attenuated trajectory point
        embed_output = self.embedding(att_input).unsqueeze(0)     # (1, batch size * num vehicles, args.enc_gru_hid)

        # Pass embedding through GRU
        if hidden == None:
            gru_output, gru_hidden = self.rnn(embed_output)
        else:
            gru_output, gru_hidden = self.rnn(embed_output, hidden) # (1, batch size * num vehicles, args.enc_gru_hid)
                                                                    # (1, batch size * num vehicles, args.enc_gru_hid)

        # Pass hidden states through mean and variance MLPs
        mean = self.mean_mlp(gru_output)            # (1, batch size * num vehicles, args.vae_latent)
        variance = self.variance_mlp(gru_output)    # (1, batch size * num vehicles, args.vae_latent)

        return mean.squeeze(0), variance.squeeze(0), gru_hidden


class Lane_Vehicle_Attention(nn.Module):
    """
    Lane-Vehicle Attention Module.
    """

    def __init__(self, device, args):
        """
        Initialize LVA.

        :param device:  cpu or gpu
        :param args:    training args configuration file
        """

        super(Lane_Vehicle_Attention, self).__init__()

        self.args = args
        self.device = device

        # Embedding network to produce queries from vehicle positions
        self.veh_embedding = nn.Sequential( nn.Linear(in_features=2,
                                                      out_features=self.args.lva_out),
                                            nn.ReLU(), 
                                            nn.Linear(in_features=self.args.lva_out,
                                                      out_features=self.args.lva_out))

        # Embedding network to produce keys/values from lane positions
        self.lane_embedding = nn.Sequential(nn.Linear(in_features=2,
                                                      out_features=self.args.lva_out),
                                            nn.ReLU(), 
                                            nn.Linear(in_features=self.args.lva_out,
                                                      out_features=self.args.lva_out))

        # Attention layer
        self.lva = nn.MultiheadAttention(   embed_dim=self.args.lva_out, 
                                            num_heads=self.args.lva_heads,
                                            dropout=self.args.lva_drop)

    def forward(self, pos, lane, mask):
        """
        Perform LVA.

        :param pos:             dim - (batch, num vehicles, pos dim)
            real positions of vehicles on road at current time
        :param lane:            dim - (batch, num vehicles, 3, pos dim)
            node positions of successor, left, and right at current time for each car
        :param mask:            dim - (batch, num vehicles, 3)
            mask of which lane positions to attenuate on (True where should not be attention)

        :output attn_out:       dim - (batch, num vehicles, args.lva_out)
            attenuated output of LVA
        :output attn_weights:   dim - (batch size, num vehicles, 3)
            attention weights between vehicles and their lane nodes
        """

        # Embed input positions
        embed_pos_output = self.veh_embedding(pos).reshape((-1, self.args.lva_out)).unsqueeze(0)            # (1, batch size * num vehicles, args.lva_out)
        embed_lane_output = self.lane_embedding(lane).reshape((-1, 3, self.args.lva_out)).permute((1,0,2))  # (3, batch size * num vehicles, args.lva_out)

        # Reshape and repeat mask for how many heads in LVA
        mask_head = mask.reshape((-1,3)).unsqueeze(1).unsqueeze(0).repeat((self.args.lva_heads,1,1,1)).permute((1,0,2,3)).reshape((-1,1,3))
                                                            # (batch size * num vehicles * num heads, 1, 3)
        # LVA
        attn_out, attn_weights = self.lva(embed_pos_output, embed_lane_output, embed_lane_output, attn_mask=mask_head, need_weights=True)
                                                            # (1, batch size * num vehicles, args.lva_out)
                                                            # (batch size * num vehicles, 1, 3)
        
        attn_out = attn_out.permute((1,0,2)).squeeze(1).reshape((-1,self.args.max_veh,self.args.lva_out))
        attn_weights = attn_weights.squeeze(1).reshape((-1,self.args.max_veh,3))

        return attn_out, attn_weights


class Auxiliary(nn.Module):
    """
    Auxiliary net for Koopman model
    """

    def __init__(self, device, args):
        """
        Initialize Auxiliary network for Koopman matrix generation.

        :param device:  cpu or gpu
        :param args:    training args configuration file
        """

        super(Auxiliary, self).__init__()
        
        self.args = args
        self.device = device
        prop_in_shape = self.args.vae_latent+self.args.lva_out if self.args.lva else self.args.vae_latent

        # Network to generate koopman matrix to propagate latent mean forward
        self.mean_fc = nn.Sequential(   nn.Linear(prop_in_shape, 256), nn.ReLU(),                           \
                                        nn.Linear(256, 128), nn.ReLU(),                                     \
                                        nn.Linear(128, self.args.vae_latent+2*(self.args.vae_latent-1)))

        # Network to generate koopman matrix to propagate latent variance forward
        self.var_fc = nn.Sequential(nn.Linear(prop_in_shape, 256), nn.ReLU(),                           \
                                    nn.Linear(256, 128), nn.ReLU(),                                     \
                                    nn.Linear(128, self.args.vae_latent+2*(self.args.vae_latent-1)))

    def forward(self, latent_mean, latent_var, att=None):
        """
        Generate Koopman matrices to propagate latent distribution forward.

        :param latent_mean: dim - (batch, num vehicles, args.vae_latent)
            mean of latent gaussians
        :param latent_var:  dim - (batch, num vehicles, args.vae_latent)
            variance of latent gaussians
        :param att:         dim - (batch, num vehicles, args.lva_out)
            LVA output

        :output k_mean_mat: dim - (batch_size, num vehicles, args.vae_latent, args.vae_latent)
            Koopman matrix for latent mean propagation
        :output k_var_mat:  dim - (batch_size, num vehicles, args.vae_latent, args.vae_latent)
            Koopman matrix for latent variance propagation
        """

        # Create vectors to propagate linearly
        if self.args.lva:
            concat_mean = torch.cat([latent_mean, att], -1) # (batch size, num vehicles, args.vae_latent + args.lva_out)
            concat_var = torch.cat([latent_var, att], -1)   # (batch size, num vehicles, args.vae_latent + args.lva_out)
        else:
            concat_mean = latent_mean   # (batch size, num vehicles, args.vae_latent)
            concat_var = latent_var     # (batch size, num vehicles, args.vae_latent)

        # generate vectors to create koopman matrices from
        k_mean = self.mean_fc(concat_mean).reshape((-1,self.args.vae_latent+2*(self.args.vae_latent-1)))    # (batch size * num vehicles, args.vae_latent + 2 * (args.vae_latent - 1))
        k_var = self.var_fc(concat_var).reshape((-1,self.args.vae_latent+2*(self.args.vae_latent-1)))       # (batch size * num vehicles, args.vae_latent + 2 * (args.vae_latent - 1))

        # Create tridiagonal matrix k_mean from flattened version, each tensor is of shape (batch_size * num vehicles, args.vae_latent, args.vae_latent)
        mean_main_diag = torch.diag_embed(k_mean[:,:self.args.vae_latent], 0)
        mean_upper_diag = torch.diag_embed(k_mean[:,self.args.vae_latent:2*self.args.vae_latent-1], 1)
        mean_lower_diag = torch.diag_embed(k_mean[:,2*self.args.vae_latent-1:3*self.args.vae_latent-2], -1)
        k_mean_mat = mean_main_diag + mean_upper_diag + mean_lower_diag

        # Create tridiagonal matrix k_var from flattened version
        var_main_diag = torch.diag_embed(k_var[:,:self.args.vae_latent], 0)
        var_upper_diag = torch.diag_embed(k_var[:,self.args.vae_latent:2*self.args.vae_latent-1], 1)
        var_lower_diag = torch.diag_embed(k_var[:,2*self.args.vae_latent-1:3*self.args.vae_latent-2], -1)
        k_var_mat = var_main_diag + var_upper_diag + var_lower_diag

        k_mean_mat = k_mean_mat.reshape((-1, self.args.max_veh, self.args.vae_latent, self.args.vae_latent))    # (batch_size, num vehicles, args.vae_latent, args.vae_latent)
        k_var_mat = k_var_mat.reshape((-1, self.args.max_veh, self.args.vae_latent, self.args.vae_latent))      # (batch_size, num vehicles, args.vae_latent, args.vae_latent)

        return k_mean_mat, k_var_mat


class Decoder(nn.Module):
    """
    Decoder of VAE.
    """

    def __init__(self, device, args):
        """
        Initialize decoder.

        :param device:  cpu or gpu
        :param args:    training args configuration file
        """

        super(Decoder, self).__init__()
        
        self.args = args
        self.device = device

        # Network to generate koopman matrix to propagate latent mean forward
        self.fc = nn.Sequential(nn.Linear(self.args.vae_latent, 256), nn.ReLU(),    \
                                nn.Linear(256, 128), nn.ReLU(),                     \
                                nn.Linear(128, 2))

    def forward(self, latent):
        """
        Generate position from latent point.

        :param latent:      dim - (batch * num vehicles, args.vae_latent)
            sampled latent points

        :output decode_out: dim - (batch_size * num vehicles, 2)
            decoded output
        """

        # Pass latent point through decoder
        decode_out = self.fc(latent)

        return decode_out


class Attenuated_Recurrent_VAE(nn.Module):
    """
    Complete VAE architecture.
    """

    def __init__(self, device, args):
        """
        Initialize VAE.

        :param device:  cpu or gpu
        :param args:    training args configuration file
        """

        super(Attenuated_Recurrent_VAE, self).__init__()
        
        self.args = args
        self.device = device

        # VVA layer
        if args.vva:
            self.vva_layer = Vehicle_Self_Attention(device, args)
        else:
            self.vva_layer = MLP(device, 2, args.vva_out)

        # GRU encoder
        self.gru_enc = Encoder(device, args)
    
        # LVA layer
        if args.lva:
            self.lva_layer = Lane_Vehicle_Attention(device, args)

        # Auxiliary network
        self.aux = Auxiliary(device, args)

        # Decoder
        self.dec = Decoder(device, args)

    def forward(self, pos, rel_pos, vva_mask, lane_pos, lane_exist_mask):
        """
        Reconstruct and predict one-step trajectories with VAE.

        :param pos:                                                 dim - (batch, num vehicles, seq len, pos dim)
            ground truth x,y positions of each car at each time
        :param rel_pos:                                             dim - (batch, num vehicles, seq len, num vehicles, pos dim)
            distances between vehicles on road at each time
        :param vva_mask:                                            dim - (batch, num vehicles, seq len, num vehicles, 1)
            boolean mask of if one vehicle observes another vehicle for VVA
            entry False if vehicle is too far from the other, 
                or vehicle being observed is not on road
        :param lane_pos:                                            dim - (batch, num vehicles, seq len, 3, pos dim)
            ground truth x,y positions of the discretized 
                lane nodes of each car at each time
            3 lane nodes correspond to successor, left, and right of cars
        :param lane_exist_mask:                                     dim - (batch, num vehicles, seq len, 3)
            mask of which lane edges exist for each center lane node at each time
            entry False where node does not exist for that car

        :output recon_traj_torch, pred_traj_torch:                  dim - (batch_size * num vehicles, seq len, pos dim)
            position trajectories output by model
        :output recon_mean_traj_torch, pred_mean_traj_torch:        dim - (batch_size * num vehicles, seq len, args.vae_latent)
            latent space mean of distributions
        :output recon_var_traj_torch, pred_var_traj_torch:          dim - (batch_size * num vehicles, seq len, args.vae_latent)
            latent space variance of distributions
        :output recon_latent_traj_torch, pred_latent_traj_torch:    dim - (batch_size * num vehicles, seq len, args.vae_latent)
            sampled latent points of reconstruction and prediction distributions
        :output vva_traj_torch:                                     dim - (batch_size * num vehicles, seq len, num vehicles)
            VVA attention weights
        :output lva_traj_torch:                                     dim - (batch_size * num vehicles, seq len, 3)
            LVA attention weights
        :output koopman_mean_traj_torch, koopman_var_traj_torch:    dim - (batch_size * num vehicles, seq len, args.vae_latent, args.vae_latent)
            Koopman matrices
        """

        # Create lists to return sequences
        recon_traj, pred_traj = [], []                  # position trajectories
        recon_mean_traj, recon_var_traj = [], []        # reconstruction distributions
        pred_mean_traj, pred_var_traj = [], []          # prediction distributions
        recon_latent_traj, pred_latent_traj = [], []    # sampled latent points of reconstruction and prediction distributions
        vva_traj, lva_traj = [], []                     # vva and lva attention weights
        koopman_mean_traj, koopman_var_traj = [], []    # koopman matrices

        # Initialize GRU hidden state
        enc_hidden = None

        # Iterate over sequence
        for curr_time in range(self.args.seq_len):

            # Access masks for current time step
            curr_pos = pos[:,:self.args.max_veh,curr_time]                      # (batch size, max_vehicles, 2)
            curr_rel_pos = rel_pos[:,:self.args.max_veh,curr_time]              # (batch size, max_vehicles, max_vehicles, 2)
            curr_mask = ~vva_mask[:,:,curr_time,:,0]                            # (batch size, max_vehicles, max_vehicles)
            curr_lane = lane_pos[:,:self.args.max_veh,curr_time]                # (batch size, max_vehicles, 3, 2)
            curr_lane_mask = ~lane_exist_mask[:,:self.args.max_veh,curr_time]   # (batch size, max_vehicles, 3)

            # Vehicle-Vehicle Attention
            if self.args.vva:
                vva_attn_out, vva_attn_weights = self.vva_layer(curr_pos, curr_rel_pos, curr_mask)  # (batch size, max_vehicles, args.vva_out)
                                                                                                    # (batch size, max_vehicles, max_vehicles)
            else:
                vva_attn_out = self.vva_layer(curr_pos)
                vva_attn_weights = None

            # Encode current time step features
            enc_att_in = vva_attn_out.reshape((-1,self.args.vva_out))   # (batch size * max_vehicles, args.vva_out)

            curr_enc_mean, curr_enc_var, enc_hidden = self.gru_enc(enc_att_in, enc_hidden)  # (batch size * max_vehicles, args.vae_latent)
                                                                                            # (batch size * max_vehicles, args.vae_latent)
                                                                                            # (num_layers * num_directions, batch * max_vehicles, args.enc_gru_hid)

            curr_lat_vec = self.reparameterize(curr_enc_mean, curr_enc_var) # (batch size * max_vehicles, args.vae_latent)

            # Lane-Vehicle Attention
            if self.args.lva:
                lva_attn_out, lva_attn_weights = self.lva_layer(curr_pos, curr_lane, curr_lane_mask)    # (batch size, max_vehicles, args.lva_out)
                                                                                                        # (batch size, max_vehicles, 3)
            else:
                lva_attn_out, lva_attn_weights = None, None

            # Koopman matrix generation
            batch_curr_enc_mean = curr_enc_mean.reshape((-1,self.args.max_veh,self.args.vae_latent))            # (batch size, max_vehicles, args.vae_latent)
            batch_curr_enc_var = curr_enc_var.reshape((-1,self.args.max_veh,self.args.vae_latent))              # (batch size, max_vehicles, args.vae_latent)
            curr_k_mean_mat, curr_k_var_mat = self.aux(batch_curr_enc_mean, batch_curr_enc_var, lva_attn_out)   # (batch size, max_vehicles, args.vae_latent, args.vae_latent)
                                                                                                                # (batch size, max_vehicles, args.vae_latent, args.vae_latent)

            # Propagate latent distributions forward with Koopman matrices (z_t+1 = z_t + K*z_t)
            reshape_curr_k_mean_mat = curr_k_mean_mat.reshape((-1,self.args.vae_latent,self.args.vae_latent))   # (batch size * max_vehicles, args.vae_latent, args.vae_latent)
            reshape_curr_k_var_mat = curr_k_var_mat.reshape((-1,self.args.vae_latent,self.args.vae_latent))     # (batch size * max_vehicles, args.vae_latent, args.vae_latent)
            next_mean = (curr_enc_mean.unsqueeze(-1) + torch.bmm(reshape_curr_k_mean_mat, curr_enc_mean.unsqueeze(-1))).squeeze(-1) # (batch size * max_vehicles, args.vae_latent)
            next_var = (curr_enc_var.unsqueeze(-1) + torch.bmm(reshape_curr_k_var_mat, curr_enc_var.unsqueeze(-1))).squeeze(-1)     # (batch size * max_vehicles, args.vae_latent)

            # Sample new point from propagated distribution
            next_lat_vec = self.reparameterize(next_mean, next_var)  # (batch size * max_vehicles, args.vae_latent)

            # Reconstruct current points with decoder
            curr_recon = self.dec(curr_lat_vec) # (batch_size * max_vehicles, 2)

            # Predict next points with decoder
            next_pred = self.dec(next_lat_vec)  # (batch_size * max_vehicles, 2)

            # Append trajectory points to sequence lists
            recon_traj.append(curr_recon.unsqueeze(1))  # unsqueezing to add sequence dimension to tensors
            pred_traj.append(next_pred.unsqueeze(1))
            recon_mean_traj.append(curr_enc_mean.unsqueeze(1))
            pred_mean_traj.append(next_mean.unsqueeze(1))
            recon_var_traj.append(curr_enc_var.unsqueeze(1))
            pred_var_traj.append(next_var.unsqueeze(1))
            recon_latent_traj.append(curr_lat_vec.unsqueeze(1))
            pred_latent_traj.append(next_lat_vec.unsqueeze(1))
            koopman_mean_traj.append(reshape_curr_k_mean_mat.unsqueeze(1))
            koopman_var_traj.append(reshape_curr_k_var_mat.unsqueeze(1))
            if self.args.vva:
                vva_traj.append(vva_attn_weights.reshape((-1,self.args.max_veh)).unsqueeze(1))
            if self.args.lva:
                lva_traj.append(lva_attn_weights.reshape((-1,3)).unsqueeze(1))

        # Create torch tensors for returning
        recon_traj_torch = torch.cat(recon_traj, dim=1)                 # (batch_size * max_vehicles, seq len, pos dim)
        pred_traj_torch = torch.cat(pred_traj, dim=1)                   # (batch_size * max_vehicles, seq len, pos dim)
        recon_mean_traj_torch = torch.cat(recon_mean_traj, dim=1)       # (batch_size * max_vehicles, seq len, args.vae_latent)
        pred_mean_traj_torch = torch.cat(pred_mean_traj, dim=1)         # (batch_size * max_vehicles, seq len, args.vae_latent)
        recon_var_traj_torch = torch.cat(recon_var_traj, dim=1)         # (batch_size * max_vehicles, seq len, args.vae_latent)
        pred_var_traj_torch = torch.cat(pred_var_traj, dim=1)           # (batch_size * max_vehicles, seq len, args.vae_latent)
        recon_latent_traj_torch = torch.cat(recon_latent_traj, dim=1)   # (batch_size * max_vehicles, seq len, args.vae_latent)
        pred_latent_traj_torch = torch.cat(pred_latent_traj, dim=1)     # (batch_size * max_vehicles, seq len, args.vae_latent)
        koopman_mean_traj_torch = torch.cat(koopman_mean_traj, dim=1)   # (batch_size * max_vehicles, seq len, args.vae_latent, args.vae_latent)
        koopman_var_traj_torch = torch.cat(koopman_var_traj, dim=1)     # (batch_size * max_vehicles, seq len, args.vae_latent, args.vae_latent)
        if self.args.vva:
            vva_traj_torch = torch.cat(vva_traj, dim=1)                 # (batch_size * max_vehicles, seq len, max_vehicles)
        else:
            vva_traj_torch = None
        if self.args.lva:
            lva_traj_torch = torch.cat(lva_traj, dim=1)                 # (batch_size * max_vehicles, seq len, 3)
        else:
            lva_traj_torch = None

        return  recon_traj_torch, pred_traj_torch, recon_mean_traj_torch, pred_mean_traj_torch,             \
                recon_var_traj_torch, pred_var_traj_torch, recon_latent_traj_torch, pred_latent_traj_torch, \
                vva_traj_torch, lva_traj_torch, koopman_mean_traj_torch, koopman_var_traj_torch

    def reparameterize(self, mu, log_var):
        """
        VAE reparameterization trick.

        :param mu:      dim - (batch size, enc_lat_out)
            means
        :param log_var: dim - (batch size, enc_lat_out)
            log variances
        
        :output z:      dim - (batch size, enc_lat_out)
            sampled vectors
        """

        std = torch.exp(0.5 * log_var)
        eps = torch.randn_like(std)
        return mu + eps * std

    def loss(self, obs_mask, gt_traj, pred_curr_traj, pred_future_traj, curr_z_mean, curr_z_var, future_z_mean, future_z_var):
        """
        Calculate loss between predicted and ground truth trajectories, and
            apply latent space gaussian regularization.

        :param obs_mask:                    dim - (batch * num vehicles, seq_len)
            observation mask of which cars were observable at which time step 
                for each episode in the batch
        :param gt_traj:                     dim - (batch * num vehicles, seq_len, pos dim)
            ground truth x,y positions of each car at each time
        :param pred_curr_traj:              dim - (batch * num vehicles, seq_len, pos dim)
            model reconstructed x,y current positions of each car
        :param pred_future_traj:            dim - (batch * num vehicles, seq_len, pos dim)
            model predicted x,y one-step future positions of each car
        :param curr_z_mean, curr_z_var:     dim - (batch * num vehicles, seq_len, args.vae_latent)
            model encoded latent mean/variance of current distributions
        :param future_z_mean, future_z_var: dim - (batch * num vehicles, seq_len, args.vae_latent)
            model encoded latent mean/variance of one-step future distributions

        :output total loss:                 current reconstruction loss + future prediction loss + weighted KL divergence
        :output reconstruction_loss_curr:   MSE reconstruction loss
        :output reconstruction_loss_next:   MSE prediction loss
        :output kl_div_pre:                 unweighted KL divergence of current latent distribution
        :output kl_div_post:                unweighted KL divergence of future latent distribution
        """

        # Flatten vectors for masking observed vehicles and calculating MSE reconstruction loss and KL divergence
        obs_mask_flat_curr = obs_mask[:,:-1].reshape((-1, 1))                   # current observation mask          (batch size * max_vehicles * (seq_len - 1), 1)
        obs_mask_flat_next = obs_mask[:,1:].reshape((-1, 1))                    # one-step future observation mask  (batch size * max_vehicles * (seq_len - 1), 1)
        gt_traj_curr = gt_traj[:,:-1].reshape((-1, 2))                          # current ground truth trajectory   (batch size * max_vehicles * (seq_len - 1), pos dim)
        gt_traj_next = gt_traj[:,1:].reshape((-1, 2))                           # one-step future gt trajectory     (batch size * max_vehicles * (seq_len - 1), pos dim)
        pred_traj_curr = pred_curr_traj[:,:-1].reshape((-1, 2))                 # current predicted trajectory      (batch size * max_vehicles * (seq_len - 1), pos dim)
        pred_traj_next = pred_future_traj[:,:-1].reshape((-1, 2))               # future predicted trajectory       (batch size * max_vehicles * (seq_len - 1), pos dim)
        z_mean_curr = curr_z_mean[:,:-1].reshape((-1, self.args.vae_latent))    # current latent mean               (batch size * max_vehicles * (seq_len - 1), args.vae_latent)
        z_var_curr = curr_z_var[:,:-1].reshape((-1, self.args.vae_latent))      # current latent variance           (batch size * max_vehicles * (seq_len - 1), args.vae_latent)
        z_mean_next = future_z_mean[:,:-1].reshape((-1, self.args.vae_latent))  # future latent mean                (batch size * max_vehicles * (seq_len - 1), args.vae_latent)
        z_var_next = future_z_var[:,:-1].reshape((-1, self.args.vae_latent))    # future latent variance            (batch size * max_vehicles * (seq_len - 1), args.vae_latent)

        # Mask ground truth and predicted vectors for reconstruction loss
        masked_gt_traj_curr = torch.masked_select(gt_traj_curr, obs_mask_flat_curr).reshape((-1, 2))        # (num_observed, pos dim)
        masked_gt_traj_next = torch.masked_select(gt_traj_next, obs_mask_flat_next).reshape((-1, 2))        # (num_observed, pos dim)
        masked_pred_traj_curr = torch.masked_select(pred_traj_curr, obs_mask_flat_curr).reshape((-1, 2))    # (num_observed, pos dim)
        masked_pred_traj_next = torch.masked_select(pred_traj_next, obs_mask_flat_next).reshape((-1, 2))    # (num_observed, pos dim)

        # Calculate reconstruction and future prediction loss
        reconstruction_loss_curr = F.mse_loss(masked_gt_traj_curr, masked_pred_traj_curr) * 10.
        reconstruction_loss_next = F.mse_loss(masked_gt_traj_next, masked_pred_traj_next) * 10.

        # Mask current and future latent mean and variance for KL divergence
        masked_z_mean_curr = torch.masked_select(z_mean_curr, obs_mask_flat_curr).reshape((-1, self.args.vae_latent))   # (num_observed, args.vae_latent)
        masked_z_var_curr = torch.masked_select(z_var_curr, obs_mask_flat_curr).reshape((-1, self.args.vae_latent))     # (num_observed, args.vae_latent)
        masked_z_mean_next = torch.masked_select(z_mean_next, obs_mask_flat_next).reshape((-1, self.args.vae_latent))   # (num_observed, args.vae_latent)
        masked_z_var_next = torch.masked_select(z_var_next, obs_mask_flat_next).reshape((-1, self.args.vae_latent))     # (num_observed, args.vae_latent)

        # Calculate KL divergence of current latent distributions from standard gaussian
        kl_div_pre = torch.mean(-0.5*torch.sum(1 + masked_z_var_curr - masked_z_mean_curr.pow(2) - masked_z_var_curr.exp(), dim=-1))

        # Calculate KL divergence of future latent distributions from standard gaussian
        kl_div_post = torch.mean(-0.5*torch.sum(1 + masked_z_var_next - masked_z_mean_next.pow(2) - masked_z_var_next.exp(), dim=-1))

        # Calculate complete loss
        total_loss = (reconstruction_loss_curr + reconstruction_loss_next) / 2. + kl_div_pre * self.args.pre_kl_beta + kl_div_post * self.args.post_kl_beta

        # Return losses
        return total_loss, reconstruction_loss_curr.item(), reconstruction_loss_next.item(), kl_div_pre.item(), kl_div_post.item()

    def masked_mean(self, tensor, mask, dim):
        """
        Calculate the mean of a tensor along a dimension while masking out certain values.
        Modified from https://www.codefull.net/2020/03/masked-tensor-operations-in-pytorch/

        :param tensor:  vector to calculate the mean of
        :param mask:    boolean mask with same shape as tensor 
        :param dim:     integer of dimension to calculate mean along
        
        :output mean_out:   averaged vector with a shape of 1 along the dim dimension
        """

        masked = torch.mul(tensor, mask)                    # Apply the mask using an element-wise multiply
        masked_sum = masked.sum(dim=dim, keepdim=True)      # Sum unmasked values
        avg_mask_divisor = mask.sum(dim=dim, keepdim=True)  # Count how many unmasked entries we summed over
        mean_out = masked_sum / avg_mask_divisor            # Calculate average
        return mean_out


def update_linear_schedule(optimizer, epoch, total_num_epochs, initial_lr):
        """Decreases the learning rate linearly"""
        lr = initial_lr - (initial_lr * (epoch / float(total_num_epochs)))
        for param_group in optimizer.param_groups:
            param_group['lr'] = lr
