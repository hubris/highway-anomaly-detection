import os
import sys
import pickle
import csv

import numpy as np
import configargparse
from tqdm import tqdm
import torch
import torch.nn.functional as F
import joblib
import sklearn.metrics
import matplotlib.pyplot as plt
import pandas as pd

from utils.data_loader import loadDataset
from models.ae import Attenuated_Recurrent_AE

ENVIRONMENT = ['maad']
SPLIT = ['val', 'test']


if __name__ == '__main__':

    p = configargparse.ArgumentParser(
        prog='Autoencoder Tester',
        description='Tests an AE model on a dataset split for quantitative results.',
        formatter_class=configargparse.ArgumentDefaultsHelpFormatter
    )

    ###########
    # Dataset #
    ###########

    # Environment Name
    p.add('--env', type=str, metavar='ENV', required=True, choices=ENVIRONMENT,
          help='Name of dataset to load.')

    # Dataset Split
    p.add('--split', type=str, metavar='SPLIT', required=True, choices=SPLIT,
          help='Name of dataset split to load from <test/val>.')

    # Run Name to Load Data From
    p.add('--data_run_name', type=str, metavar='DATA_RUN_NAME', required=True,
          help='Name of run subfolder to load from data directory.')

    # Number of Data Loading Workers
    p.add('--num_workers', type=int, metavar='WORK', default=0,
                help='Number of workers used in data loader.')

    # Test on GPU
    p.add('--gpu', action='store_true', default=False,
                help='Set device for PyTorch as GPU if available.')

    #########
    # Model #
    #########

    # Run Name to Load Model From
    p.add('--run_name', type=str, metavar='RUN_NAME', required=True,
          help='Name of run subfolder to load from training directory.')

    # Run Number to Load Model From
    p.add('--run_num', type=int, metavar='RUN_NUM', required=True,
                help='Run number to load model from run_name subdirectory.')

    # Checkpoint Number of Model to Test
    p.add('--checkpoint', type=int, metavar='CHECKPOINT', default=-1, required=False,
          help='Checkpoint number of model to test. Set as -1 to test the final checkpoint.')

    args = p.parse_args()


    # Get folder for where model will be loaded from
    model_dir = os.path.join("pretrained", args.run_name, args.env, str(args.run_num))
    print('model directory to load from: ', model_dir)

    assert os.path.exists(model_dir)
    
    # Get arguments for how model was trained
    trained_args_path = os.path.join(model_dir, "model_args.pickle")
    with open(trained_args_path, "rb") as f:
        trained_args = pickle.load(f)

    # Directory with dataset
    data_dir = os.path.join("data", args.data_run_name, args.env)

    # Get dataset of trajectories for environment with maximum batch size to load complete data to RAM
    data_generator = loadDataset(   split=args.split, 
                                    batch_size=sys.maxsize, 
                                    num_workers=args.num_workers, 
                                    drop_last=False, 
                                    load_dir=data_dir,
                                    allow_shuffle=False)

    # Set device
    device = torch.device("cuda" if args.gpu else "cpu")
    print("Using device:", device)

    # Load checkpoint
    checkpoint_path = os.path.join(model_dir, "checkpoints", 'final.pt' if args.checkpoint == -1 else str(args.checkpoint)+'.pt')
    checkpoint = torch.load(checkpoint_path, map_location=device)

    # Create VAE model
    ae_model = Attenuated_Recurrent_AE(device, trained_args)
    ae_model.load_state_dict(checkpoint['model'])
    ae_model = ae_model.to(device)
    ae_model.eval()

    # Sub folder to save figures and results to
    figure_folder_path = os.path.join(  model_dir, "figs", args.data_run_name, args.split,                          \
                                        'ckpk_{}'.format('final' if args.checkpoint == -1 else args.checkpoint))
    os.makedirs(figure_folder_path, exist_ok=True)

    np.set_printoptions(edgeitems=1000)
    np.set_printoptions(linewidth=500)

    with torch.no_grad():

        # Get dataset
        for i_batch, data in enumerate(tqdm(data_generator)):

            # Move data to device
            data = data[:,:trained_args.max_veh,:trained_args.seq_len].float().to(device)   # (dataset size, max_vehicles, seq_len, 24)

            # Observation mask of which cars were observable at which time step for each episode in the batch
            observed_mask = data[:,:trained_args.max_veh,:trained_args.seq_len,2].bool()    # (dataset size, max_vehicles, seq_len)

            # Ground truth absolute and relative change x,y positions of each car at each time
            pos = data[:,:trained_args.max_veh,:trained_args.seq_len,3:5]       # (dataset size, max_vehicles, seq_len, 2)
            rel_pos = data[:,:trained_args.max_veh,:trained_args.seq_len,5:7]   # (dataset size, max_vehicles, seq_len, 2)

            # Ground truth x,y positions of the discretized lane nodes of each car at each time
            lane_succ = data[:,:trained_args.max_veh,:trained_args.seq_len,10:12].unsqueeze(3)  # (dataset size, max_vehicles, seq_len, 1, 2)
            lane_left = data[:,:trained_args.max_veh,:trained_args.seq_len,15:17].unsqueeze(3)  # (dataset size, max_vehicles, seq_len, 1, 2)
            lane_right = data[:,:trained_args.max_veh,:trained_args.seq_len,20:22].unsqueeze(3) # (dataset size, max_vehicles, seq_len, 1, 2)
            lane_pos = torch.cat((lane_succ, lane_left, lane_right), 3)                         # (dataset size, max_vehicles, seq_len, 3, 2)

            # Mask of which lane edges exist for each center lane node at each time
            succ_lane_exist_mask = data[:,:trained_args.max_veh,:trained_args.seq_len,7].bool().unsqueeze(3)    # (dataset size, max_vehicles, seq_len, 1)
            left_lane_exist_mask = data[:,:trained_args.max_veh,:trained_args.seq_len,12].bool().unsqueeze(3)   # (dataset size, max_vehicles, seq_len, 1)
            right_lane_exist_mask = data[:,:trained_args.max_veh,:trained_args.seq_len,17].bool().unsqueeze(3)  # (dataset size, max_vehicles, seq_len, 1)
            lane_exist_mask = torch.cat((succ_lane_exist_mask, left_lane_exist_mask, right_lane_exist_mask), 3) # (dataset size, max_vehicles, seq_len, 3)

            # Create key/val source from vehicle positions with shape (dataset size, max_vehicles, seq_len, max_vehicles, pos_dim) by repeating veh_pos_input
            # Permute veh_pos_input so that sequence length dimension is outside of repeating area
            vva_permute_kvs = pos.permute((0, 2, 1, 3))                                 # (dataset size, seq_len, max_vehicles, pos_dim)
            # Unsqueeze to add dimension to repeat
            vva_unsqueeze_kvs = vva_permute_kvs.unsqueeze(2)                            # (dataset size, seq_len, 1, max_vehicles, pos_dim)
            # Repeat by the number of vehicles 
            vva_repeat_kvs = vva_unsqueeze_kvs.repeat(1, 1, trained_args.max_veh, 1, 1) # (dataset size, seq_len, max_vehicles, max_vehicles, pos_dim)
            # Unpermute so that sequence length dimension is back to where it was
            vva_kvs = vva_repeat_kvs.permute(0, 2, 1, 3, 4)                             # (dataset size, max_vehicles, seq_len, max_vehicles, pos_dim)

            # Based on observation mask
            # Permute observed_mask so that sequence length dimension is outside of repeating area
            vva_permute_obs_mask = observed_mask.permute((0, 2, 1))                             # (dataset size, seq_len, max_vehicles)
            # Unsqueeze to add dimension to repeat
            vva_unsqueeze_obs_mask = vva_permute_obs_mask.unsqueeze(2)                          # (dataset size, seq_len, 1, max_vehicles)
            # Repeat by the number of vehicles 
            vva_repeat_obs_mask = vva_unsqueeze_obs_mask.repeat(1, 1, trained_args.max_veh, 1)  # (dataset size, seq_len, max_vehicles, max_vehicles)
            # Unpermute so that sequence length dimension is back to where it was
            vva_unpermute_obs_mask = vva_repeat_obs_mask.permute(0, 2, 1, 3)                    # (dataset size, max_vehicles, seq_len, max_vehicles)
            # Unsqueeze vva_unpermute_mask to follow convention of Attention module's forward function
            vva_obs_mask = vva_unpermute_obs_mask.unsqueeze(4)                                  # (dataset size, max_vehicles, seq_len, max_vehicles, 1)

            # Based on vehicles' distance to other vehicles
            # Unsqueeze to add dimension to repeat
            vva_unsqueeze_dist_mask = pos.unsqueeze(3)                                              # (dataset size, max_vehicles, seq_len, 1, pos_dim)
            # Repeat by number of vehicles
            vva_repeat_dist_mask = vva_unsqueeze_dist_mask.repeat(1, 1, 1, trained_args.max_veh, 1) # (dataset size, max_vehicles, seq_len, max_vehicles, pos_dim)
            # Find difference between positions of each vehicle
            vva_diff_dist_mask = vva_kvs - vva_repeat_dist_mask                                     # (dataset size, max_vehicles, seq_len, max_vehicles, pos_dim)
            # Calculate distance norms
            vva_norm_dist_mask = torch.linalg.norm(vva_diff_dist_mask, dim=4, keepdim=True)         # (dataset size, max_vehicles, seq_len, max_vehicles, 1)
            # Filter out distances greater than threshold
            vva_dist_mask = (vva_norm_dist_mask <= trained_args.veh_dist_mask).bool()               # (dataset size, max_vehicles, seq_len, max_vehicles, 1)

            # Combine observation and distance masks
            vva_mask = torch.logical_and(vva_obs_mask, vva_dist_mask)   # (dataset size, max_vehicles, seq_len, max_vehicles, 1)

            # Normalize VVA inter-vehicle distance
            if trained_args.vva_norm:
                vva_diff_dist_mask /= trained_args.veh_dist_mask

            # Forward pass through VAE model
            recon_traj_torch, pred_traj_torch, recon_latent_traj_torch, \
            pred_latent_traj_torch, vva_traj_torch, lva_traj_torch =    \
                ae_model(rel_pos, vva_diff_dist_mask, vva_mask, lane_pos, lane_exist_mask)

            # Calculate loss between ground truth and reconstruction/prediction trajectory points
            batched_gt_curr_pos = rel_pos[:,:,:-1].reshape((-1,2))      # (dataset size * max_vehicles * (seq_len - 1), pos dim)
            batched_gt_next_pos = rel_pos[:,:,1:].reshape((-1,2))       # (dataset size * max_vehicles * (seq_len - 1), pos dim)
            batched_recon_pos = recon_traj_torch[:,:-1].reshape((-1,2)) # (dataset size * max_vehicles * (seq_len - 1), pos dim)
            batched_pred_pos = pred_traj_torch[:,:-1].reshape((-1,2))   # (dataset size * max_vehicles * (seq_len - 1), pos dim)

            recon_loss = torch.mean(F.mse_loss( batched_recon_pos, batched_gt_curr_pos, reduction='none'), dim=-1)  # (dataset size * max_vehicles * (seq_len - 1))
            pred_loss = torch.mean(F.mse_loss( batched_pred_pos, batched_gt_next_pos, reduction='none'), dim=-1)    # (dataset size * max_vehicles * (seq_len - 1))
            losses = pred_loss.detach().to('cpu').numpy().reshape((-1, 1))                                          # (dataset size * max_vehicles * (seq_len - 1), 1)

            print('Scoring Prediction Losses...')

            # Score prediction losses
            pred_score = losses.reshape((-1, trained_args.max_veh, trained_args.seq_len-1)) # (dataset size, max_vehicles, (seq_len - 1))

            # Get sequence & frame IDs and labels
            sequence_ids = data[:,:trained_args.max_veh,:trained_args.seq_len,0][:,:,1:].detach().to('cpu').numpy()                                                     # (dataset size, max_vehicles, (seq_len - 1))
            frame_ids = data[:,:trained_args.max_veh,:trained_args.seq_len,1][:,:,1:].detach().to('cpu').numpy()                                                        # (dataset size, max_vehicles, (seq_len - 1))
            gt_labels = data[:,:trained_args.max_veh,:trained_args.seq_len,22][:,:,1:].detach().to('cpu').numpy()                                                       # (dataset size, max_vehicles, (seq_len - 1))
            minor_labels = data[:,:trained_args.max_veh,:trained_args.seq_len,23][:,:,1:].detach().to('cpu').numpy()                                                    # (dataset size, max_vehicles, (seq_len - 1))
            pred_latent = pred_latent_traj_torch[:,:-1].reshape((-1,trained_args.max_veh,trained_args.seq_len-1,trained_args.vae_latent)).detach().to('cpu').numpy()    # (dataset size, max_vehicles, (seq_len - 1), args.vae_latent)
            recon_latent = recon_latent_traj_torch[:,1:].reshape((-1,trained_args.max_veh,trained_args.seq_len-1,trained_args.vae_latent)).detach().to('cpu').numpy()   # (dataset size, max_vehicles, (seq_len - 1), args.vae_latent)

            ###########################
            # Minor Label Definitions #
            ###########################

            # Normal
            # [ 0]:   side-by-side
            # [ 1]:   overtake left
            # [ 2]:   multi-overtake
            # [ 3]:   following
            # [ 4]:   opposite drive
            # [ 5]:   else
            NUM_MINOR_NORMAL = 6
            NORMAL_MINOR_LABEL_NAMES = ['side-by-side', 'overtake left', 'multi-overtake', 'following', 'opposite drive', 'other']

            # Anomaly
            # [ 0]:   aggressive overtaking
            # [ 1]:   pushing aside
            # [ 2]:   spreading maneuver to right
            # [ 3]:   spreading maneuver to left
            # [ 4]:   tailgating
            # [ 5]:   thwarting
            # [ 6]:   getting of the road
            # [ 7]:   staggering maneuver
            # [ 8]:   skidding
            # [ 9]:   wrong-way driving
            # [10]:   aggressive reeving
            # [11]:   else
            NUM_MINOR_ABNORMAL = 12
            FIXED_ABNORMAL_MINOR_LABEL_NAMES = ['aggressive overtaking', 'pushing aside', 'spreading maneuver to right', 'spreading maneuver to left', 'tailgating', 
                                                'thwarting',  'getting off the road', 'staggering maneuver', 'skidding', 'wrong-way driving', 'aggressive reeving', 'other']

            # Data dict to hold ground truth labels & prediction scores for complete trajectories
            data_dict = {}

            # Fill in data dict with results
            print('Filling in Results Dictionary...')
            for window in range(sequence_ids.shape[0]):
                for veh_id in range(trained_args.max_veh):
                    for time_id in range(1, trained_args.seq_len - 1):

                        curr_seq_id = sequence_ids[window,veh_id,time_id]       # complete trajectory ID float
                        curr_frame_id = frame_ids[window,veh_id,time_id]        # frame ID within trajectory
                        curr_pred_score = pred_score[window,veh_id,time_id]     # predicted anomaly score
                        curr_gt_label = gt_labels[window,veh_id,time_id]        # ground truth anomaly label (0, 1 or 2)
                        curr_minor_label = minor_labels[window,veh_id,time_id]  # ground truth minor label type 
                        curr_pred_latent = pred_latent[window,veh_id,time_id]   # sampled propagated latent point
                        curr_recon_latent = recon_latent[window,veh_id,time_id] # sampled recon latent point

                        if curr_seq_id not in data_dict:
                            data_dict[curr_seq_id] = {}
                        if curr_frame_id not in data_dict[curr_seq_id]:
                            data_dict[curr_seq_id][curr_frame_id] = {}
                        if veh_id not in data_dict[curr_seq_id][curr_frame_id]:
                            data_dict[curr_seq_id][curr_frame_id][veh_id] = {}
                            data_dict[curr_seq_id][curr_frame_id][veh_id]['predictions'] = []
                            data_dict[curr_seq_id][curr_frame_id][veh_id]['labels'] = []
                            data_dict[curr_seq_id][curr_frame_id][veh_id]['minor_labels'] = []
                            data_dict[curr_seq_id][curr_frame_id][veh_id]['pred_latent_sample'] = []
                            data_dict[curr_seq_id][curr_frame_id][veh_id]['recon_latent_sample'] = []
                        
                        data_dict[curr_seq_id][curr_frame_id][veh_id]['predictions'].append(curr_pred_score)
                        data_dict[curr_seq_id][curr_frame_id][veh_id]['labels'].append(curr_gt_label)
                        data_dict[curr_seq_id][curr_frame_id][veh_id]['minor_labels'].append(curr_minor_label)
                        data_dict[curr_seq_id][curr_frame_id][veh_id]['pred_latent_sample'].append(curr_pred_latent)
                        data_dict[curr_seq_id][curr_frame_id][veh_id]['recon_latent_sample'].append(curr_recon_latent)

            # Convert data dict to lists after aggregation of overlapping windows
            print('Converting Results Dictionary to List...')
            array_pred_scores = []
            array_pred_latent = []
            array_recon_latent = []
            array_gt_labels = []
            array_minor_labels = []
            array_ind_gt_labels = []
            seq_ids_list = []
            frame_ids_list = []

            for curr_seq_id in data_dict:
                seq_ids_list.append(curr_seq_id)
                array_pred_scores.append([])
                array_pred_latent.append([])
                array_recon_latent.append([])
                array_gt_labels.append([])
                array_minor_labels.append([])
                array_ind_gt_labels.append([])
                frame_ids_list.append([])

                for curr_frame_id in data_dict[curr_seq_id]:
                    frame_ids_list[-1].append(curr_frame_id)
                    curr_veh_preds = []
                    curr_veh_preds_latent = []
                    curr_veh_recon_latent = []
                    curr_veh_labels = []
                    curr_veh_minor_labels = []

                    for veh_id in data_dict[curr_seq_id][curr_frame_id]:
                        curr_veh_preds.append(np.average(data_dict[curr_seq_id][curr_frame_id][veh_id]['predictions']))
                        curr_veh_preds_latent.append(data_dict[curr_seq_id][curr_frame_id][veh_id]['pred_latent_sample'][0])
                        curr_veh_recon_latent.append(data_dict[curr_seq_id][curr_frame_id][veh_id]['recon_latent_sample'][0])
                        curr_veh_labels.append(np.max(data_dict[curr_seq_id][curr_frame_id][veh_id]['labels']))
                        curr_veh_minor_labels.append(data_dict[curr_seq_id][curr_frame_id][veh_id]['minor_labels'][0])

                    array_pred_scores[-1].append(np.max(curr_veh_preds))
                    array_pred_latent[-1].append(curr_veh_preds_latent)
                    array_recon_latent[-1].append(curr_veh_recon_latent)
                    array_gt_labels[-1].append(np.max(curr_veh_labels))
                    array_minor_labels[-1].append(curr_veh_minor_labels)
                    array_ind_gt_labels[-1].append(curr_veh_labels)

            # Convert result lists to numpy arrays and combine all trajectories together
            list_np_pred_scores = [np.array(array_pred_scores[i]) for i in range(len(array_pred_scores))]
            np_pred_scores = np.concatenate(list_np_pred_scores)
            list_np_pred_latent = [np.array(array_pred_latent[i]) for i in range(len(array_pred_latent))]
            np_pred_latent = np.concatenate(list_np_pred_latent)
            list_np_recon_latent = [np.array(array_recon_latent[i]) for i in range(len(array_recon_latent))]
            np_recon_latent = np.concatenate(list_np_recon_latent)
            list_np_gt_labels = [np.array(array_gt_labels[i]) for i in range(len(array_gt_labels))]
            np_gt_labels = np.concatenate(list_np_gt_labels)
            list_np_minor_labels = [np.array(array_minor_labels[i]) for i in range(len(array_minor_labels))]
            np_minor_labels = np.concatenate(list_np_minor_labels)
            list_np_ind_gt_labels = [np.array(array_ind_gt_labels[i]) for i in range(len(array_ind_gt_labels))]
            np_ind_gt_labels = np.concatenate(list_np_ind_gt_labels)

            # Ignore timesteps labeled as 2
            label_mask = np.where(np_gt_labels <= 1)
            masked_scores = np_pred_scores[label_mask]
            masked_latent = np_pred_latent[label_mask]
            masked_recon_latent = np_recon_latent[label_mask]
            masked_gt = np_gt_labels[label_mask]
            masked_minor = np_minor_labels[label_mask]
            masked_ind_gt = np_ind_gt_labels[label_mask]

            # Metrics below modified from https://github.com/againerju/maad_highway/blob/master/src/evaluation.py

            # Receiver Operating Characteristic (ROC) Curve
            fpr, tpr, thresholds = sklearn.metrics.roc_curve(masked_gt, masked_scores, pos_label=1, drop_intermediate=False)
            
            # Area Under ROC Curve (AUROC)
            auroc = sklearn.metrics.roc_auc_score(masked_gt, masked_scores)

            # Area Under Precision Recall Curve (AUPR) Abnormal
            aupr_abnormal = sklearn.metrics.average_precision_score(masked_gt, masked_scores)

            # Area Under Precision Recall Curve (AUPR) Normal
            aupr_normal = sklearn.metrics.average_precision_score(1 - masked_gt, -masked_scores)

            # False Positive Rate (FPR) at 95% True Positive Rate (TPR)
            hit = False
            tpr_95_lb = 0
            tpr_95_ub = 0
            fpr_95_lb = 0
            fpr_95_ub = 0
            for i in range(len(tpr)):
                if tpr[i] > 0.95 and not hit:
                    tpr_95_lb = tpr[i - 1]
                    tpr_95_ub = tpr[i]
                    fpr_95_lb = fpr[i - 1]
                    fpr_95_ub = fpr[i]
                    hit = True
            s = pd.Series([fpr_95_lb, np.nan, fpr_95_ub], [tpr_95_lb, 0.95, tpr_95_ub])
            s = s.interpolate(method="index")
            fpr_at_95tpr = s.iloc[1]

            # Plot ROC Curve
            plt.figure('roc_curve', figsize=(8, 8), dpi=300)
            lw = 2
            plt.plot(
                fpr,
                tpr,
                color="darkorange",
                lw=lw,
                label="ROC curve (area = %0.2f)" % auroc,
            )
            plt.plot([0, 1], [0, 1], color="navy", lw=lw, linestyle="--")
            plt.xlim([0.0, 1.0])
            plt.ylim([0.0, 1.05])
            plt.xlabel("False Positive Rate")
            plt.ylabel("True Positive Rate")
            plt.title("Receiver Operating Characteristic Curve")
            plt.legend(loc="lower right")
            figure_path = os.path.join(figure_folder_path, 'roc_curve_pred.png')
            plt.savefig(figure_path)
            plt.close('all')

            # Save results list
            results_dict = {}
            results_dict['array_pred_scores'] = array_pred_scores
            results_dict['array_gt_labels'] = array_gt_labels
            results_dict['array_minor_labels'] = array_minor_labels
            results_dict['seq_ids_list'] = seq_ids_list
            results_dict['frame_ids_list'] = frame_ids_list
            results_dict['roc_fpr'] = fpr
            results_dict['roc_tpr'] = tpr
            results_dict['roc_thresholds'] = thresholds
            results_file = os.path.join(figure_folder_path, 'pred_results.pickle')
            with open(results_file, 'wb') as handle:
                pickle.dump(results_dict, handle, protocol=pickle.HIGHEST_PROTOCOL)

            # Save metrics
            header = ['auroc', 'aupr_abnormal', 'aupr_normal', 'fpr_at_95_tpr']
            row_data = [auroc, aupr_abnormal, aupr_normal, fpr_at_95tpr]
            csv_file = os.path.join(figure_folder_path, 'pred_results.csv')
            with open(csv_file, 'w', encoding='UTF8', newline='') as f:
                writer = csv.writer(f)
                writer.writerow(header)
                writer.writerow(row_data)

            # Save AUROC of individual anomaly minor labels
            header = ['anomaly_type', 'auroc']
            csv_file = os.path.join(figure_folder_path, 'pred_anomaly_auroc.csv')
            with open(csv_file, 'w', encoding='UTF8', newline='') as f:
                writer = csv.writer(f)
                writer.writerow(header)
                normal_gt_mask = masked_gt == 0
                for abnormal_label_id in range(NUM_MINOR_ABNORMAL - 1):
                    curr_minor_label_mask = np.any(masked_minor == abnormal_label_id, axis=-1)
                    curr_anomaly_minor_mask = np.logical_and(masked_gt == 1, curr_minor_label_mask)
                    curr_complete_mask = np.logical_or(normal_gt_mask, curr_anomaly_minor_mask)
                    curr_scores = masked_scores[curr_complete_mask]
                    curr_gt = masked_gt[curr_complete_mask]
                    curr_auroc = sklearn.metrics.roc_auc_score(curr_gt, curr_scores)
                    row_data = [FIXED_ABNORMAL_MINOR_LABEL_NAMES[abnormal_label_id], curr_auroc]
                    writer.writerow(row_data)
