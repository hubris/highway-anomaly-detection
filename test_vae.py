import os
import sys
import pickle
import csv

import numpy as np
import configargparse
from tqdm import tqdm
import torch
import torch.nn.functional as F
import joblib
import sklearn.metrics
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.colors import Normalize 
import matplotlib.lines as mlines
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes 
from mpl_toolkits.axes_grid1.inset_locator import mark_inset
from scipy.interpolate import interpn
import pandas as pd

from utils.data_loader import loadDataset
from models.vae import Attenuated_Recurrent_VAE


##############################
# Utility Plotting Functions #
##############################

def lighten_color(color, amount=0.5):
    """
    Lightens the given color by multiplying (1-luminosity) by the given amount.
    Input can be matplotlib color string, hex string, or RGB tuple.
    https://stackoverflow.com/questions/37765197/darken-or-lighten-a-color-in-matplotlib

    Examples:
    >> lighten_color('g', 0.3)
    >> lighten_color('#F034A3', 0.6)
    >> lighten_color((.3,.55,.1), 0.5)
    """
    import matplotlib.colors as mc
    import colorsys
    try:
        c = mc.cnames[color]
    except:
        c = color
    c = colorsys.rgb_to_hls(*mc.to_rgb(c))
    return colorsys.hls_to_rgb(c[0], 1 - amount * (1 - c[1]), c[2])

def cmap_map(function, cmap):
    """ Applies function (which should operate on vectors of shape 3: [r, g, b]), on colormap cmap.
    This routine will break any discontinuous points in a colormap.
    https://scipy-cookbook.readthedocs.io/items/Matplotlib_ColormapTransformations.html
    """
    cdict = cmap._segmentdata
    step_dict = {}
    # First get the list of points where the segments start or end
    for key in ('red', 'green', 'blue'):
        step_dict[key] = list(map(lambda x: x[0], cdict[key]))
    step_list = sum(step_dict.values(), [])
    step_list = np.array(list(set(step_list)))
    # Then compute the LUT, and apply the function to the LUT
    reduced_cmap = lambda step : np.array(cmap(step)[0:3])
    old_LUT = np.array(list(map(reduced_cmap, step_list)))
    new_LUT = np.array(list(map(function, old_LUT)))
    # Now try to make a minimal segment definition of the new LUT
    cdict = {}
    for i, key in enumerate(['red','green','blue']):
        this_cdict = {}
        for j, step in enumerate(step_list):
            if step in step_dict[key]:
                this_cdict[step] = new_LUT[j, i]
            elif new_LUT[j,i] != old_LUT[j, i]:
                this_cdict[step] = new_LUT[j, i]
        colorvector = list(map(lambda x: x + (x[1], ), this_cdict.items()))
        colorvector.sort()
        cdict[key] = colorvector

    return matplotlib.colors.LinearSegmentedColormap('colormap',cdict,1024)

def density_scatter( x , y, ax = None, sort = True, bins = 20, **kwargs )   :
    """
    Scatter plot colored by 2d histogram
    https://stackoverflow.com/questions/20105364/how-can-i-make-a-scatter-plot-colored-by-density-in-matplotlib
    """
    if ax is None :
        fig , ax = plt.subplots(figsize=(8, 8), dpi=300)
    data , x_e, y_e = np.histogram2d( x, y, bins = bins, density = True )
    z = interpn( ( 0.5*(x_e[1:] + x_e[:-1]) , 0.5*(y_e[1:]+y_e[:-1]) ) , data , np.vstack([x,y]).T , method = "splinef2d", bounds_error = False)

    # To be sure to plot all data
    z[np.where(np.isnan(z))] = 0.0

    # Sort the points by density, so that the densest points are plotted last
    if sort :
        idx = z.argsort()
        x, y, z = x[idx], y[idx], z[idx]

    light_aut = cmap_map(lambda x: x/2 + 0.5, cm.autumn)
    ax.scatter(x, y, c=z, cmap=light_aut, alpha=0.5, zorder=0, **kwargs)

    try:
        return fig, ax
    except:
        return ax

#############
# Constants #
#############

ENVIRONMENT = ['maad']
SPLIT = ['val', 'test']
# MAAD Map Setup
H =  66.66666666666667              # height of total map
W =  416.6666666666667              # width of total map
LANE_WIDTH =  8.333333333333334     # width of each lane
STREET_CENTER =  33.0               # horizontal center of map
UPPER =  49.66666666666667          # top most edge of the map
LOWER =  16.333333333333332         # bottom most edge of the map
# Plotting
CAR_COLORS = ['slateblue', 'teal']
EDGE_CAR_COLORS = ['black', 'black']
RECON_COLORS = ['lightcoral', 'cyan']
PRED_COLORS = ['fuchsia', 'dodgerblue']
LATENT_PRED_COLORS = ['fuchsia', 'aqua']
LATENT_MARKERS = ['^', 's', 'p', 'D', 'H']
LATENT_TITLE_SIZE = 25
LATENT_LEGEND_SIZE = 20
LATENT_ANNOTATE_SIZE = 22


if __name__ == '__main__':

    p = configargparse.ArgumentParser(
        prog='SABeR-VAE Tester',
        description='Tests a SABeR-VAE model on a dataset split for quantitative results.',
        formatter_class=configargparse.ArgumentDefaultsHelpFormatter
    )

    ###########
    # Dataset #
    ###########

    # Environment Name
    p.add('--env', type=str, metavar='ENV', required=True, choices=ENVIRONMENT,
          help='Name of dataset to load.')

    # Dataset Split
    p.add('--split', type=str, metavar='SPLIT', required=True, choices=SPLIT,
          help='Name of dataset split to load from <test/val>.')

    # Run Name to Load Data From
    p.add('--data_run_name', type=str, metavar='DATA_RUN_NAME', required=True,
          help='Name of run subfolder to load from data directory.')

    # Number of Data Loading Workers
    p.add('--num_workers', type=int, metavar='WORK', default=0,
                help='Number of workers used in data loader.')

    # Test on GPU
    p.add('--gpu', action='store_true', default=False,
                help='Set device for PyTorch as GPU if available.')

    #########
    # Model #
    #########

    # Run Name to Load Model From
    p.add('--run_name', type=str, metavar='RUN_NAME', required=True,
          help='Name of run subfolder to load from training directory.')

    # Run Number to Load Model From
    p.add('--run_num', type=int, metavar='RUN_NUM', required=True,
                help='Run number to load model from run_name subdirectory.')

    # Checkpoint Number of Model to Test
    p.add('--checkpoint', type=int, metavar='CHECKPOINT', default=-1, required=False,
          help='Checkpoint number of model to test. Set as -1 to test the final checkpoint.')

    #################
    # Visualization #
    #################

    p.add('--latent_labels', type=int, metavar='LAT_LABELS', required=False, nargs='+',
        help='List of ground truth labels of latent points to visualize trajectories of.')

    p.add('--latent_x', type=float, metavar='LAT_X', required=False, nargs='+',
        help='List of x coordinates of latent points to visualize trajectories of.')

    p.add('--latent_y', type=float, metavar='LAT_Y', required=False, nargs='+',
        help='List of y coordinates of latent points to visualize trajectories of.')

    args = p.parse_args()


    # Get folder for where model will be loaded from
    model_dir = os.path.join("pretrained", args.run_name, args.env, str(args.run_num))
    print('model directory to load from: ', model_dir)

    assert os.path.exists(model_dir)

    if args.latent_labels:
        assert len(args.latent_labels) == len(args.latent_x) == len(args.latent_y)

    # Get arguments for how model was trained
    trained_args_path = os.path.join(model_dir, "model_args.pickle")
    with open(trained_args_path, "rb") as f:
        trained_args = pickle.load(f)

    # Directory with dataset
    data_dir = os.path.join("data", args.data_run_name, args.env)

    # Get dataset of trajectories for environment with maximum batch size to load complete data to RAM
    data_generator = loadDataset(   split=args.split, 
                                    batch_size=sys.maxsize, 
                                    num_workers=args.num_workers, 
                                    drop_last=False, 
                                    load_dir=data_dir,
                                    allow_shuffle=False)

    # Set device
    device = torch.device("cuda" if args.gpu else "cpu")
    print("Using device:", device)

    # Load checkpoint
    checkpoint_path = os.path.join(model_dir, "checkpoints", 'final.pt' if args.checkpoint == -1 else str(args.checkpoint)+'.pt')
    checkpoint = torch.load(checkpoint_path, map_location=device)

    # Create VAE model
    vae_model = Attenuated_Recurrent_VAE(device, trained_args)
    vae_model.load_state_dict(checkpoint['model'])
    vae_model = vae_model.to(device)
    vae_model.eval()

    # Sub folder to save figures and results to
    figure_folder_path = os.path.join(  model_dir, "figs", args.data_run_name, args.split,                          \
                                        'ckpk_{}'.format('final' if args.checkpoint == -1 else args.checkpoint))
    os.makedirs(figure_folder_path, exist_ok=True)

    np.set_printoptions(edgeitems=1000)
    np.set_printoptions(linewidth=500)

    with torch.no_grad():

        # Get dataset
        for i_batch, data in enumerate(tqdm(data_generator)):

            print('number of windows: ', data.shape)

            # Move data to device
            data = data[:,:trained_args.max_veh,:trained_args.seq_len].float().to(device)   # (dataset size, max_vehicles, seq_len, 24)

            # Observation mask of which cars were observable at which time step for each episode in the batch
            observed_mask = data[:,:trained_args.max_veh,:trained_args.seq_len,2].bool()    # (dataset size, max_vehicles, seq_len)

            # Ground truth absolute and relative change x,y positions of each car at each time
            pos = data[:,:trained_args.max_veh,:trained_args.seq_len,3:5]       # (dataset size, max_vehicles, seq_len, 2)
            rel_pos = data[:,:trained_args.max_veh,:trained_args.seq_len,5:7]   # (dataset size, max_vehicles, seq_len, 2)

            # Ground truth x,y positions of the discretized lane nodes of each car at each time
            lane_succ = data[:,:trained_args.max_veh,:trained_args.seq_len,10:12].unsqueeze(3)  # (dataset size, max_vehicles, seq_len, 1, 2)
            lane_left = data[:,:trained_args.max_veh,:trained_args.seq_len,15:17].unsqueeze(3)  # (dataset size, max_vehicles, seq_len, 1, 2)
            lane_right = data[:,:trained_args.max_veh,:trained_args.seq_len,20:22].unsqueeze(3) # (dataset size, max_vehicles, seq_len, 1, 2)
            lane_pos = torch.cat((lane_succ, lane_left, lane_right), 3)                         # (dataset size, max_vehicles, seq_len, 3, 2)

            # Mask of which lane edges exist for each center lane node at each time
            succ_lane_exist_mask = data[:,:trained_args.max_veh,:trained_args.seq_len,7].bool().unsqueeze(3)    # (dataset size, max_vehicles, seq_len, 1)
            left_lane_exist_mask = data[:,:trained_args.max_veh,:trained_args.seq_len,12].bool().unsqueeze(3)   # (dataset size, max_vehicles, seq_len, 1)
            right_lane_exist_mask = data[:,:trained_args.max_veh,:trained_args.seq_len,17].bool().unsqueeze(3)  # (dataset size, max_vehicles, seq_len, 1)
            lane_exist_mask = torch.cat((succ_lane_exist_mask, left_lane_exist_mask, right_lane_exist_mask), 3) # (dataset size, max_vehicles, seq_len, 3)

            # Create key/val source from vehicle positions with shape (dataset size, max_vehicles, seq_len, max_vehicles, pos_dim) by repeating veh_pos_input
            # Permute veh_pos_input so that sequence length dimension is outside of repeating area
            vva_permute_kvs = pos.permute((0, 2, 1, 3))                                 # (dataset size, seq_len, max_vehicles, pos_dim)
            # Unsqueeze to add dimension to repeat
            vva_unsqueeze_kvs = vva_permute_kvs.unsqueeze(2)                            # (dataset size, seq_len, 1, max_vehicles, pos_dim)
            # Repeat by the number of vehicles 
            vva_repeat_kvs = vva_unsqueeze_kvs.repeat(1, 1, trained_args.max_veh, 1, 1) # (dataset size, seq_len, max_vehicles, max_vehicles, pos_dim)
            # Unpermute so that sequence length dimension is back to where it was
            vva_kvs = vva_repeat_kvs.permute(0, 2, 1, 3, 4)                             # (dataset size, max_vehicles, seq_len, max_vehicles, pos_dim)

            # Based on observation mask
            # Permute observed_mask so that sequence length dimension is outside of repeating area
            vva_permute_obs_mask = observed_mask.permute((0, 2, 1))                             # (dataset size, seq_len, max_vehicles)
            # Unsqueeze to add dimension to repeat
            vva_unsqueeze_obs_mask = vva_permute_obs_mask.unsqueeze(2)                          # (dataset size, seq_len, 1, max_vehicles)
            # Repeat by the number of vehicles 
            vva_repeat_obs_mask = vva_unsqueeze_obs_mask.repeat(1, 1, trained_args.max_veh, 1)  # (dataset size, seq_len, max_vehicles, max_vehicles)
            # Unpermute so that sequence length dimension is back to where it was
            vva_unpermute_obs_mask = vva_repeat_obs_mask.permute(0, 2, 1, 3)                    # (dataset size, max_vehicles, seq_len, max_vehicles)
            # Unsqueeze vva_unpermute_mask to follow convention of Attention module's forward function
            vva_obs_mask = vva_unpermute_obs_mask.unsqueeze(4)                                  # (dataset size, max_vehicles, seq_len, max_vehicles, 1)

            # Based on vehicles' distance to other vehicles
            # Unsqueeze to add dimension to repeat
            vva_unsqueeze_dist_mask = pos.unsqueeze(3)                                              # (dataset size, max_vehicles, seq_len, 1, pos_dim)
            # Repeat by number of vehicles
            vva_repeat_dist_mask = vva_unsqueeze_dist_mask.repeat(1, 1, 1, trained_args.max_veh, 1) # (dataset size, max_vehicles, seq_len, max_vehicles, pos_dim)
            # Find difference between positions of each vehicle
            vva_diff_dist_mask = vva_kvs - vva_repeat_dist_mask                                     # (dataset size, max_vehicles, seq_len, max_vehicles, pos_dim)
            # Calculate distance norms
            vva_norm_dist_mask = torch.linalg.norm(vva_diff_dist_mask, dim=4, keepdim=True)         # (dataset size, max_vehicles, seq_len, max_vehicles, 1)
            # Filter out distances greater than threshold
            vva_dist_mask = (vva_norm_dist_mask <= trained_args.veh_dist_mask).bool()               # (dataset size, max_vehicles, seq_len, max_vehicles, 1)

            # Combine observation and distance masks
            vva_mask = torch.logical_and(vva_obs_mask, vva_dist_mask)   # (dataset size, max_vehicles, seq_len, max_vehicles, 1)

            # Normalize VVA inter-vehicle distance
            if trained_args.vva_norm:
                vva_diff_dist_mask /= trained_args.veh_dist_mask

            # Forward pass through VAE model
            recon_traj_torch, pred_traj_torch, recon_mean_traj_torch, pred_mean_traj_torch,             \
            recon_var_traj_torch, pred_var_traj_torch, recon_latent_traj_torch, pred_latent_traj_torch, \
            vva_traj_torch, lva_traj_torch, koopman_mean_traj_torch, koopman_var_traj_torch =           \
                vae_model(rel_pos, vva_diff_dist_mask, vva_mask, lane_pos, lane_exist_mask)

            # Calculate loss between ground truth and reconstruction/prediction trajectory points
            batched_gt_curr_pos = rel_pos[:,:,:-1].reshape((-1,2))      # (dataset size * max_vehicles * (seq_len - 1), pos dim)
            batched_gt_next_pos = rel_pos[:,:,1:].reshape((-1,2))       # (dataset size * max_vehicles * (seq_len - 1), pos dim)
            batched_recon_pos = recon_traj_torch[:,:-1].reshape((-1,2)) # (dataset size * max_vehicles * (seq_len - 1), pos dim)
            batched_pred_pos = pred_traj_torch[:,:-1].reshape((-1,2))   # (dataset size * max_vehicles * (seq_len - 1), pos dim)

            recon_loss = torch.mean(F.mse_loss( batched_recon_pos, batched_gt_curr_pos, reduction='none'), dim=-1)  # (dataset size * max_vehicles * (seq_len - 1))
            pred_loss = torch.mean(F.mse_loss( batched_pred_pos, batched_gt_next_pos, reduction='none'), dim=-1)    # (dataset size * max_vehicles * (seq_len - 1))
            losses = pred_loss.detach().to('cpu').numpy().reshape((-1, 1))                                          # (dataset size * max_vehicles * (seq_len - 1), 1)

            print('Scoring Prediction Losses...')

            # Score prediction losses
            pred_score = losses.reshape((-1, trained_args.max_veh, trained_args.seq_len-1)) # (dataset size, max_vehicles, (seq_len - 1))

            # Get sequence & frame IDs and labels
            sequence_ids = data[:,:trained_args.max_veh,:trained_args.seq_len,0][:,:,1:].detach().to('cpu').numpy()                                                     # (dataset size, max_vehicles, (seq_len - 1))
            frame_ids = data[:,:trained_args.max_veh,:trained_args.seq_len,1][:,:,1:].detach().to('cpu').numpy()                                                        # (dataset size, max_vehicles, (seq_len - 1))
            gt_labels = data[:,:trained_args.max_veh,:trained_args.seq_len,22][:,:,1:].detach().to('cpu').numpy()                                                       # (dataset size, max_vehicles, (seq_len - 1))
            minor_labels = data[:,:trained_args.max_veh,:trained_args.seq_len,23][:,:,1:].detach().to('cpu').numpy()                                                    # (dataset size, max_vehicles, (seq_len - 1))
            pred_latent = pred_latent_traj_torch[:,:-1].reshape((-1,trained_args.max_veh,trained_args.seq_len-1,trained_args.vae_latent)).detach().to('cpu').numpy()    # (dataset size, max_vehicles, (seq_len - 1), args.vae_latent)
            recon_latent = recon_latent_traj_torch[:,1:].reshape((-1,trained_args.max_veh,trained_args.seq_len-1,trained_args.vae_latent)).detach().to('cpu').numpy()   # (dataset size, max_vehicles, (seq_len - 1), args.vae_latent)
            gt_abs_pos = data[:,:trained_args.max_veh,:trained_args.seq_len,3:5][:,:,1:].detach().to('cpu').numpy()                                                     # (dataset size, max_vehicles, (seq_len - 1), pos_dim)

            ###########################
            # Minor Label Definitions #
            ###########################

            # Normal
            # [ 0]:   side-by-side
            # [ 1]:   overtake left
            # [ 2]:   multi-overtake
            # [ 3]:   following
            # [ 4]:   opposite drive
            # [ 5]:   else
            NUM_MINOR_NORMAL = 6
            NORMAL_MINOR_LABEL_NAMES = ['side-by-side', 'overtake left', 'multi-overtake', 'following', 'opposite drive', 'other']

            # Anomaly
            # [ 0]:   aggressive overtaking
            # [ 1]:   pushing aside
            # [ 2]:   spreading maneuver to right
            # [ 3]:   spreading maneuver to left
            # [ 4]:   tailgating
            # [ 5]:   thwarting
            # [ 6]:   getting of the road
            # [ 7]:   staggering maneuver
            # [ 8]:   skidding
            # [ 9]:   wrong-way driving
            # [10]:   aggressive reeving
            # [11]:   else
            NUM_MINOR_ABNORMAL = 12
            FIXED_ABNORMAL_MINOR_LABEL_NAMES = ['aggressive overtaking', 'pushing aside', 'spreading maneuver to right', 'spreading maneuver to left', 'tailgating', 
                                                'thwarting',  'getting off the road', 'staggering maneuver', 'skidding', 'wrong-way driving', 'aggressive reeving', 'other']

            # Data dict to hold ground truth labels & prediction scores for complete trajectories
            data_dict = {}

            # Fill in data dict with results
            print('Filling in Results Dictionary...')
            for window in range(sequence_ids.shape[0]):
                for veh_id in range(trained_args.max_veh):
                    for time_id in range(1, trained_args.seq_len - 1):

                        curr_seq_id = sequence_ids[window,veh_id,time_id]       # complete trajectory ID float
                        curr_frame_id = frame_ids[window,veh_id,time_id]        # frame ID within trajectory
                        curr_pred_score = pred_score[window,veh_id,time_id]     # predicted anomaly score
                        curr_gt_label = gt_labels[window,veh_id,time_id]        # ground truth anomaly label (0, 1 or 2)
                        curr_minor_label = minor_labels[window,veh_id,time_id]  # ground truth minor label type 
                        curr_pred_latent = pred_latent[window,veh_id,time_id]   # sampled propagated latent point
                        curr_recon_latent = recon_latent[window,veh_id,time_id] # sampled recon latent point
                        curr_gt_abs_pos = gt_abs_pos[window,veh_id,time_id]     # ground truth absolute positions of vehicles

                        if curr_seq_id not in data_dict:
                            data_dict[curr_seq_id] = {}
                        if curr_frame_id not in data_dict[curr_seq_id]:
                            data_dict[curr_seq_id][curr_frame_id] = {}
                        if veh_id not in data_dict[curr_seq_id][curr_frame_id]:
                            data_dict[curr_seq_id][curr_frame_id][veh_id] = {}
                            data_dict[curr_seq_id][curr_frame_id][veh_id]['predictions'] = []
                            data_dict[curr_seq_id][curr_frame_id][veh_id]['labels'] = []
                            data_dict[curr_seq_id][curr_frame_id][veh_id]['minor_labels'] = []
                            data_dict[curr_seq_id][curr_frame_id][veh_id]['pred_latent_sample'] = []
                            data_dict[curr_seq_id][curr_frame_id][veh_id]['recon_latent_sample'] = []
                            data_dict[curr_seq_id][curr_frame_id][veh_id]['abs_pos'] = []
                        
                        data_dict[curr_seq_id][curr_frame_id][veh_id]['predictions'].append(curr_pred_score)
                        data_dict[curr_seq_id][curr_frame_id][veh_id]['labels'].append(curr_gt_label)
                        data_dict[curr_seq_id][curr_frame_id][veh_id]['minor_labels'].append(curr_minor_label)
                        data_dict[curr_seq_id][curr_frame_id][veh_id]['pred_latent_sample'].append(curr_pred_latent)
                        data_dict[curr_seq_id][curr_frame_id][veh_id]['recon_latent_sample'].append(curr_recon_latent)
                        data_dict[curr_seq_id][curr_frame_id][veh_id]['abs_pos'].append(curr_gt_abs_pos)

            # Convert data dict to lists after aggregation of overlapping windows
            print('Converting Results Dictionary to List...')
            array_pred_scores = []
            array_pred_latent = []
            array_recon_latent = []
            array_gt_labels = []
            array_minor_labels = []
            array_ind_gt_labels = []
            seq_ids_list = []
            frame_ids_list = []
            array_gt_abs_pos = []

            for curr_seq_id in data_dict:
                seq_ids_list.append(curr_seq_id)
                array_pred_scores.append([])
                array_pred_latent.append([])
                array_recon_latent.append([])
                array_gt_labels.append([])
                array_minor_labels.append([])
                array_ind_gt_labels.append([])
                frame_ids_list.append([])
                array_gt_abs_pos.append([])

                for curr_frame_id in data_dict[curr_seq_id]:
                    frame_ids_list[-1].append(curr_frame_id)
                    curr_veh_preds = []
                    curr_veh_preds_latent = []
                    curr_veh_recon_latent = []
                    curr_veh_labels = []
                    curr_veh_minor_labels = []
                    curr_veh_poses = []

                    for veh_id in data_dict[curr_seq_id][curr_frame_id]:
                        curr_veh_preds.append(np.average(data_dict[curr_seq_id][curr_frame_id][veh_id]['predictions']))
                        curr_veh_preds_latent.append(data_dict[curr_seq_id][curr_frame_id][veh_id]['pred_latent_sample'][0])
                        curr_veh_recon_latent.append(data_dict[curr_seq_id][curr_frame_id][veh_id]['recon_latent_sample'][0])
                        curr_veh_labels.append(np.max(data_dict[curr_seq_id][curr_frame_id][veh_id]['labels']))
                        curr_veh_minor_labels.append(data_dict[curr_seq_id][curr_frame_id][veh_id]['minor_labels'][0])
                        curr_veh_poses.append(data_dict[curr_seq_id][curr_frame_id][veh_id]['abs_pos'][0])

                    array_pred_scores[-1].append(np.max(curr_veh_preds))
                    array_pred_latent[-1].append(curr_veh_preds_latent)
                    array_recon_latent[-1].append(curr_veh_recon_latent)
                    array_gt_labels[-1].append(np.max(curr_veh_labels))
                    array_minor_labels[-1].append(curr_veh_minor_labels)
                    array_ind_gt_labels[-1].append(curr_veh_labels)
                    array_gt_abs_pos[-1].append(curr_veh_poses)

            # Convert result lists to numpy arrays and combine all trajectories together
            list_np_pred_scores = [np.array(array_pred_scores[i]) for i in range(len(array_pred_scores))]
            np_pred_scores = np.concatenate(list_np_pred_scores)
            list_np_pred_latent = [np.array(array_pred_latent[i]) for i in range(len(array_pred_latent))]
            np_pred_latent = np.concatenate(list_np_pred_latent)
            list_np_recon_latent = [np.array(array_recon_latent[i]) for i in range(len(array_recon_latent))]
            np_recon_latent = np.concatenate(list_np_recon_latent)
            list_np_gt_labels = [np.array(array_gt_labels[i]) for i in range(len(array_gt_labels))]
            np_gt_labels = np.concatenate(list_np_gt_labels)
            list_np_minor_labels = [np.array(array_minor_labels[i]) for i in range(len(array_minor_labels))]
            np_minor_labels = np.concatenate(list_np_minor_labels)
            list_np_ind_gt_labels = [np.array(array_ind_gt_labels[i]) for i in range(len(array_ind_gt_labels))]
            np_ind_gt_labels = np.concatenate(list_np_ind_gt_labels)

            # Ignore timesteps labeled as 2
            label_mask = np.where(np_gt_labels <= 1)
            masked_scores = np_pred_scores[label_mask]
            masked_latent = np_pred_latent[label_mask]
            masked_recon_latent = np_recon_latent[label_mask]
            masked_gt = np_gt_labels[label_mask]
            masked_minor = np_minor_labels[label_mask]
            masked_ind_gt = np_ind_gt_labels[label_mask]

            # Plot pre-koopman latent space
            batch_masked_ind_gt = masked_ind_gt.reshape((-1))
            batch_masked_minor = masked_minor.reshape((-1))
            batch_masked_recon_latent = masked_recon_latent.reshape((-1,trained_args.vae_latent))

            plt.figure('recon_latent_space', figsize=(8, 8), dpi=300)
            for gt_label_id in range(2):
                curr_gt_ind_labels = np.where(batch_masked_ind_gt == gt_label_id)
                curr_batch_masked_recon_latent = batch_masked_recon_latent[curr_gt_ind_labels]
                plt.scatter(curr_batch_masked_recon_latent[:,0], curr_batch_masked_recon_latent[:,1], label='abnormal' if gt_label_id else 'normal', alpha=0.5)
            plt.title("Reconstruction Latent Space")
            plt.legend()
            figure_path = os.path.join(figure_folder_path, 'recon_latent_space_pred.png')
            plt.savefig(figure_path)
            plt.close('all')

            plt.figure('recon_normal_latent_space', figsize=(8, 8), dpi=300)
            normal_gt_ind_labels = np.where(batch_masked_ind_gt == 0)
            normal_batch_masked_recon_latent = batch_masked_recon_latent[normal_gt_ind_labels]
            normal_batch_masked_minor = batch_masked_minor[normal_gt_ind_labels]
            for normal_label_id in range(NUM_MINOR_NORMAL):
                curr_minor_mask = np.where(normal_batch_masked_minor == normal_label_id)
                curr_normal_batch_masked_recon_latent = normal_batch_masked_recon_latent[curr_minor_mask]
                plt.scatter(curr_normal_batch_masked_recon_latent[:,0], curr_normal_batch_masked_recon_latent[:,1], label=NORMAL_MINOR_LABEL_NAMES[normal_label_id], alpha=0.5)
            plt.title("Reconstruction Normal Latent Space")
            plt.legend()
            figure_path = os.path.join(figure_folder_path, 'recon_normal_latent_space_pred.png')
            plt.savefig(figure_path)
            plt.close('all')

            plt.figure('recon_abnormal_latent_space', figsize=(8, 8), dpi=300)
            abnormal_gt_ind_labels = np.where(batch_masked_ind_gt == 1)
            abnormal_batch_masked_recon_latent = batch_masked_recon_latent[abnormal_gt_ind_labels]
            abnormal_batch_masked_minor = batch_masked_minor[abnormal_gt_ind_labels]
            for abnormal_label_id in range(NUM_MINOR_ABNORMAL):
                curr_minor_mask = np.where(abnormal_batch_masked_minor == abnormal_label_id)
                curr_abnormal_batch_masked_recon_latent = abnormal_batch_masked_recon_latent[curr_minor_mask]
                plt.scatter(curr_abnormal_batch_masked_recon_latent[:,0], curr_abnormal_batch_masked_recon_latent[:,1], label=FIXED_ABNORMAL_MINOR_LABEL_NAMES[abnormal_label_id], alpha=0.5)
            plt.title("Reconstruction Abnormal Latent Space")
            plt.legend()
            figure_path = os.path.join(figure_folder_path, 'recon_abnormal_latent_space_pred.png')
            plt.savefig(figure_path)
            plt.close('all')

            # Plot koopman propagated latent space
            batch_masked_ind_gt = masked_ind_gt.reshape((-1))
            batch_masked_minor = masked_minor.reshape((-1))
            batch_masked_latent = masked_latent.reshape((-1,trained_args.vae_latent))

            plt.figure('prop_latent_space', figsize=(8, 8), dpi=300)
            for gt_label_id in range(2):
                curr_gt_ind_labels = np.where(batch_masked_ind_gt == gt_label_id)
                curr_batch_masked_latent = batch_masked_latent[curr_gt_ind_labels]
                plt.scatter(curr_batch_masked_latent[:,0], curr_batch_masked_latent[:,1], label='abnormal' if gt_label_id else 'normal', alpha=0.5)
            plt.title("Propagated Latent Space")
            plt.legend()
            figure_path = os.path.join(figure_folder_path, 'prop_latent_space_pred.png')
            plt.savefig(figure_path)

            plt.close('all')

            plt.figure('prop_normal_latent_space', figsize=(8, 8), dpi=300)
            normal_gt_ind_labels = np.where(batch_masked_ind_gt == 0)
            normal_batch_masked_latent = batch_masked_latent[normal_gt_ind_labels]
            normal_batch_masked_minor = batch_masked_minor[normal_gt_ind_labels]
            for normal_label_id in range(NUM_MINOR_NORMAL):
                curr_minor_mask = np.where(normal_batch_masked_minor == normal_label_id)
                curr_normal_batch_masked_latent = normal_batch_masked_latent[curr_minor_mask]
                plt.scatter(curr_normal_batch_masked_latent[:,0], curr_normal_batch_masked_latent[:,1], label=NORMAL_MINOR_LABEL_NAMES[normal_label_id], alpha=0.5)
            plt.title("Propagated Normal Latent Space")
            plt.legend()
            figure_path = os.path.join(figure_folder_path, 'prop_normal_latent_space_pred.png')
            plt.savefig(figure_path)
            plt.close('all')

            plt.figure('prop_abnormal_latent_space', figsize=(8, 8), dpi=300)
            abnormal_gt_ind_labels = np.where(batch_masked_ind_gt == 1)
            abnormal_batch_masked_latent = batch_masked_latent[abnormal_gt_ind_labels]
            abnormal_batch_masked_minor = batch_masked_minor[abnormal_gt_ind_labels]
            for abnormal_label_id in range(NUM_MINOR_ABNORMAL):
                curr_minor_mask = np.where(abnormal_batch_masked_minor == abnormal_label_id)
                curr_abnormal_batch_masked_latent = abnormal_batch_masked_latent[curr_minor_mask]
                plt.scatter(curr_abnormal_batch_masked_latent[:,0], curr_abnormal_batch_masked_latent[:,1], label=FIXED_ABNORMAL_MINOR_LABEL_NAMES[abnormal_label_id], alpha=0.5)
            plt.title("Propagated Abnormal Latent Space")
            plt.legend()
            figure_path = os.path.join(figure_folder_path, 'prop_abnormal_latent_space_pred.png')
            plt.savefig(figure_path)
            plt.close('all')

            # Metrics below modified from https://github.com/againerju/maad_highway/blob/master/src/evaluation.py

            print('masked_gt: ', masked_gt.shape)
            print('masked_scores: ', masked_scores.shape)
            print('normal points: ', masked_gt[masked_gt == 0].shape)
            print('anomaly points: ', masked_gt[masked_gt == 1].shape)

            # Receiver Operating Characteristic (ROC) Curve
            fpr, tpr, thresholds = sklearn.metrics.roc_curve(masked_gt, masked_scores, pos_label=1, drop_intermediate=False)
            
            # Area Under ROC Curve (AUROC)
            auroc = sklearn.metrics.roc_auc_score(masked_gt, masked_scores)

            # Area Under Precision Recall Curve (AUPR) Abnormal
            aupr_abnormal = sklearn.metrics.average_precision_score(masked_gt, masked_scores)

            # Area Under Precision Recall Curve (AUPR) Normal
            aupr_normal = sklearn.metrics.average_precision_score(1 - masked_gt, -masked_scores)

            # False Positive Rate (FPR) at 95% True Positive Rate (TPR)
            hit = False
            tpr_95_lb = 0
            tpr_95_ub = 0
            fpr_95_lb = 0
            fpr_95_ub = 0
            for i in range(len(tpr)):
                if tpr[i] > 0.95 and not hit:
                    tpr_95_lb = tpr[i - 1]
                    tpr_95_ub = tpr[i]
                    fpr_95_lb = fpr[i - 1]
                    fpr_95_ub = fpr[i]
                    hit = True
            s = pd.Series([fpr_95_lb, np.nan, fpr_95_ub], [tpr_95_lb, 0.95, tpr_95_ub])
            s = s.interpolate(method="index")
            fpr_at_95tpr = s.iloc[1]

            # Plot ROC Curve
            plt.figure('roc_curve', figsize=(8, 8), dpi=300)
            lw = 2
            plt.plot(
                fpr,
                tpr,
                color="darkorange",
                lw=lw,
                label="ROC curve (area = %0.2f)" % auroc,
            )
            plt.plot([0, 1], [0, 1], color="navy", lw=lw, linestyle="--")
            plt.xlim([0.0, 1.0])
            plt.ylim([0.0, 1.05])
            plt.xlabel("False Positive Rate")
            plt.ylabel("True Positive Rate")
            plt.title("Receiver Operating Characteristic Curve")
            plt.legend(loc="lower right")
            figure_path = os.path.join(figure_folder_path, 'roc_curve_pred.png')
            plt.savefig(figure_path)
            plt.close('all')

            # Save results list
            results_dict = {}
            results_dict['array_pred_scores'] = array_pred_scores
            results_dict['array_gt_labels'] = array_gt_labels
            results_dict['array_minor_labels'] = array_minor_labels
            results_dict['seq_ids_list'] = seq_ids_list
            results_dict['frame_ids_list'] = frame_ids_list
            results_dict['array_gt_abs_pos'] = array_gt_abs_pos
            results_dict['roc_fpr'] = fpr
            results_dict['roc_tpr'] = tpr
            results_dict['roc_thresholds'] = thresholds
            results_file = os.path.join(figure_folder_path, 'pred_results.pickle')
            with open(results_file, 'wb') as handle:
                pickle.dump(results_dict, handle, protocol=pickle.HIGHEST_PROTOCOL)

            # Save metrics
            header = ['auroc', 'aupr_abnormal', 'aupr_normal', 'fpr_at_95_tpr']
            row_data = [auroc, aupr_abnormal, aupr_normal, fpr_at_95tpr]
            csv_file = os.path.join(figure_folder_path, 'pred_results.csv')
            with open(csv_file, 'w', encoding='UTF8', newline='') as f:
                writer = csv.writer(f)
                writer.writerow(header)
                writer.writerow(row_data)

            # Save AUROC of individual anomaly minor labels
            header = ['anomaly_type', 'auroc']
            csv_file = os.path.join(figure_folder_path, 'pred_anomaly_auroc.csv')
            with open(csv_file, 'w', encoding='UTF8', newline='') as f:
                writer = csv.writer(f)
                writer.writerow(header)
                normal_gt_mask = masked_gt == 0
                for abnormal_label_id in range(NUM_MINOR_ABNORMAL - 1):
                    curr_minor_label_mask = np.any(masked_minor == abnormal_label_id, axis=-1)
                    curr_anomaly_minor_mask = np.logical_and(masked_gt == 1, curr_minor_label_mask)
                    curr_complete_mask = np.logical_or(normal_gt_mask, curr_anomaly_minor_mask)
                    curr_scores = masked_scores[curr_complete_mask]
                    curr_gt = masked_gt[curr_complete_mask]
                    curr_auroc = sklearn.metrics.roc_auc_score(curr_gt, curr_scores)
                    row_data = [FIXED_ABNORMAL_MINOR_LABEL_NAMES[abnormal_label_id], curr_auroc]
                    writer.writerow(row_data)
                    print(FIXED_ABNORMAL_MINOR_LABEL_NAMES[abnormal_label_id], ': ', curr_gt[curr_gt == 1].shape)

            # Visualize latent points
            last_gt_labels = gt_labels[:,:,-1]          # (dataset size, max_vehicles)
            last_pred_latent = pred_latent[:,:,-1]      # (dataset size, max_vehicles, args.vae_latent)
            last_recon_latent = recon_latent[:,:,-1]    # (dataset size, max_vehicles, args.vae_latent)
            
            max_last_gt_labels = np.max(last_gt_labels, axis=-1)    # (dataset size)
            last_label_mask = np.where(max_last_gt_labels <= 1)
            
            masked_last_gt_labels = last_gt_labels[last_label_mask]         # (num observed points, max_vehicles)
            masked_max_last_gt_labels = max_last_gt_labels[last_label_mask] # (num observed points)
            masked_last_pred_latent = last_pred_latent[last_label_mask]     # (num observed points, max_vehicles, args.vae_latent)
            masked_last_recon_latent = last_recon_latent[last_label_mask]   # (num observed points, max_vehicles, args.vae_latent)

            plt.figure('prop_latent_space', figsize=(8, 8), dpi=300)
            plt.xticks([])
            plt.yticks([])
            for gt_label_id in range(2):
                batched_masked_last_gt_labels = masked_last_gt_labels.reshape((-1))                             # (num observed points * max_vehicles)
                curr_gt_labels = np.where(batched_masked_last_gt_labels == gt_label_id)
                batched_masked_last_pred_latent = masked_last_pred_latent.reshape((-1,trained_args.vae_latent)) # (num observed points * max_vehicles, args.vae_latent)
                curr_masked_latent = batched_masked_last_pred_latent[curr_gt_labels]
                plt.scatter(curr_masked_latent[:,0].reshape((-1)), curr_masked_latent[:,1].reshape((-1)), label='abnormal' if gt_label_id else 'normal', alpha=0.1)

            plt.title("Propagated Latent Space", fontsize=LATENT_TITLE_SIZE)
            leg = plt.legend(prop={'size': LATENT_LEGEND_SIZE})
            for lh in leg.legendHandles: 
                lh.set_alpha(1)
            figure_path = os.path.join(figure_folder_path, 'prop_latent_space_pred_final.png')
            plt.savefig(figure_path)

            # Create density map of normal points
            batched_masked_last_gt_labels = masked_last_gt_labels.reshape((-1))                             # (num observed points * max_vehicles)
            curr_gt_labels = np.where(batched_masked_last_gt_labels == 0)
            batched_masked_last_pred_latent = masked_last_pred_latent.reshape((-1,trained_args.vae_latent)) # (num observed points * max_vehicles, args.vae_latent)
            curr_masked_latent_density = batched_masked_last_pred_latent[curr_gt_labels]
            density_fig, density_ax = density_scatter( curr_masked_latent_density[:,0].reshape((-1)), curr_masked_latent_density[:,1].reshape((-1)), bins = [30,30] )

            ########################################################
            # Plot per-latent-point basis if provided in arguments #
            ########################################################

            # Plotting trajectories closest to chosen latent points
            if args.latent_labels:

                # Create subfolder for latest run of trajectory plotting
                fig_folder_num = 0
                while os.path.exists(os.path.join(figure_folder_path, 'figs_{}'.format(fig_folder_num))):
                    fig_folder_num += 1

                curr_figure_folder_path = os.path.join(figure_folder_path, 'figs_{}'.format(fig_folder_num))
                os.makedirs(curr_figure_folder_path, exist_ok=False)

                # Access trajectories
                np_gt_pos = pos.detach().to('cpu').numpy()                                                      # (dataset size, max_vehicles, seq_len, 2)
                reshaped_pred_pos = pred_traj_torch.reshape((-1,trained_args.max_veh,trained_args.seq_len,2))   # (dataset size, max_vehicles, seq_len, 2)
                np_pred_pos = reshaped_pred_pos.detach().to('cpu').numpy()                                      # (dataset size, max_vehicles, seq_len, 2)

                masked_gt_pos = np_gt_pos[last_label_mask]      # (num observed points, max_vehicles, seq_len, 2)
                masked_pred_pos = np_pred_pos[last_label_mask]  # (num observed points, max_vehicles, seq_len, 2)

                # Access attention weights
                reshaped_vva_traj = vva_traj_torch.reshape((-1,trained_args.max_veh,trained_args.seq_len,trained_args.max_veh)) # (dataset size, max_vehicles, seq_len, max_vehicles)
                np_vva_traj = reshaped_vva_traj.permute((0,2,1,3)).detach().to('cpu').numpy()                                   # (dataset size, seq_len, max_vehicles, max_vehicles)
                reshaped_lva_traj = lva_traj_torch.reshape((-1,trained_args.max_veh,trained_args.seq_len,3))                    # (dataset size, max_vehicles, seq_len, 3)
                np_lva_traj = reshaped_lva_traj.permute((0,2,1,3)).detach().to('cpu').numpy()                                   # (dataset size, seq_len, max_vehicles, 3)
                
                masked_vva_traj = np_vva_traj[last_label_mask]  # (num observed points, seq_len, max_vehicles, max_vehicles)
                masked_lva_traj = np_lva_traj[last_label_mask]  # (num observed points, seq_len, max_vehicles, 3)

                # Access koopman matrices
                reshaped_koopman_mean_traj = koopman_mean_traj_torch.reshape((-1,trained_args.max_veh,trained_args.seq_len,trained_args.vae_latent,trained_args.vae_latent))    # (dataset size, max_vehicles, seq_len, args.vae_latent, args.vae_latent)
                np_koopman_mean_traj = reshaped_koopman_mean_traj.detach().to('cpu').numpy()                                    # (dataset size, max_vehicles, seq_len, args.vae_latent, args.vae_latent)
                reshaped_koopman_var_traj = koopman_var_traj_torch.reshape((-1,trained_args.max_veh,trained_args.seq_len,trained_args.vae_latent,trained_args.vae_latent))      # (dataset size, max_vehicles, seq_len, args.vae_latent, args.vae_latent)
                np_koopman_var_traj = reshaped_koopman_var_traj.detach().to('cpu').numpy()                                      # (dataset size, max_vehicles, seq_len, args.vae_latent, args.vae_latent)
                
                masked_koopman_mean_traj = np_koopman_mean_traj[last_label_mask]    # (num observed points, max_vehicles, seq_len, args.vae_latent, args.vae_latent)
                masked_koopman_var_traj = np_koopman_var_traj[last_label_mask]      # (num observed points, max_vehicles, seq_len, args.vae_latent, args.vae_latent)

                # Access losses
                masked_pred_score = pred_score[last_label_mask] # (num observed points, max_vehicles, (seq_len - 1))

                # Access latent vector sequences
                reshaped_pred_latent_traj = pred_latent_traj_torch.reshape((-1,trained_args.max_veh,trained_args.seq_len,trained_args.vae_latent))  # (dataset size, max_vehicles, seq_len, args.vae_latent)
                np_pred_latent_traj = reshaped_pred_latent_traj.permute((0,1,3,2)).detach().to('cpu').numpy()   # (dataset size, max_vehicles, args.vae_latent, seq_len)

                masked_pred_latent_traj = np_pred_latent_traj[last_label_mask]  # (num observed points, max_vehicles, args.vae_latent, seq_len)

                # Access labels by car by each timestep in window
                masked_all_gt_labels = gt_labels[last_label_mask]               # (num observed points, max_vehicles, (seq_len - 1))

                # Iterate over each queried latent point
                for traj_num, (latent_label, latent_x, latent_y) in enumerate(zip(args.latent_labels, args.latent_x, args.latent_y)):
                    
                    # Create sub-folder for current trajectory
                    sub_figure_folder_path = os.path.join(curr_figure_folder_path, str(traj_num))
                    os.makedirs(sub_figure_folder_path, exist_ok=False)

                    # Find closest latent point
                    curr_gt_labels = np.where(masked_max_last_gt_labels == latent_label)
                    curr_masked_latent = masked_last_pred_latent[curr_gt_labels]    # (num points of type latent_label, max_vehicles, args.vae_latent)
                    curr_masked_ind_labels = masked_last_gt_labels[curr_gt_labels]  # (num points of type latent_label, max_vehicles)

                    shifted_curr_masked_latent = np.copy(curr_masked_latent)    # (num points of type latent_label, max_vehicles, args.vae_latent)
                    shifted_curr_masked_latent[:,:,0] -= latent_x
                    shifted_curr_masked_latent[:,:,1] -= latent_y

                    dist_curr_masked_latent = np.linalg.norm(shifted_curr_masked_latent, axis=-1)   # (num points of type latent_label, max_vehicles)
                    closest_latent_index = np.unravel_index(dist_curr_masked_latent.argmin(), dist_curr_masked_latent.shape)

                    curr_chosen_latent = curr_masked_latent[closest_latent_index[0]]            # (max_vehicles, args.vae_latent)
                    curr_chosen_ind_labels = curr_masked_ind_labels[closest_latent_index[0]]    # (max_vehicles)

                    # Highlight chosen latent points in propagated latent plot 
                    plt.figure('prop_latent_space')
                    for agent_id in range(trained_args.max_veh):
                        plt.scatter(curr_chosen_latent[agent_id][0], curr_chosen_latent[agent_id][1], color='white', s=100, marker='s', edgecolors='black')
                        plt.annotate('{}{}'.format(traj_num+1, 'P' if agent_id == 0 else 'B'), (curr_chosen_latent[agent_id][0] - 5, curr_chosen_latent[agent_id][1] + 5), fontsize=LATENT_ANNOTATE_SIZE)

                    # Access trajectories corresponding to chosen latent point
                    curr_masked_gt_pos = masked_gt_pos[curr_gt_labels]                          # (num points of type latent_label, max_vehicles, seq_len, 2)
                    curr_masked_pred_pos = masked_pred_pos[curr_gt_labels]                      # (num points of type latent_label, max_vehicles, seq_len, 2)
                    curr_masked_vva_traj = masked_vva_traj[curr_gt_labels]                      # (num points of type latent_label, seq_len, max_vehicles, max_vehicles)
                    curr_masked_lva_traj = masked_lva_traj[curr_gt_labels]                      # (num points of type latent_label, seq_len, max_vehicles, 3)
                    curr_masked_koopman_mean_traj = masked_koopman_mean_traj[curr_gt_labels]    # (num points of type latent_label, max_vehicles, seq_len, args.vae_latent, args.vae_latent)
                    curr_masked_koopman_var_traj = masked_koopman_var_traj[curr_gt_labels]      # (num points of type latent_label, max_vehicles, seq_len, args.vae_latent, args.vae_latent)
                    curr_masked_pred_score = masked_pred_score[curr_gt_labels]                  # (num points of type latent_label, max_vehicles, (seq_len - 1))
                    curr_masked_pred_latent_traj = masked_pred_latent_traj[curr_gt_labels]      # (num points of type latent_label, max_vehicles, args.vae_latent, seq_len)
                    curr_masked_all_gt_labels = masked_all_gt_labels[curr_gt_labels]            # (num points of type latent_label, max_vehicles, (seq_len - 1))

                    curr_chosen_gt_pos = curr_masked_gt_pos[closest_latent_index[0]]                        # (max_vehicles, seq_len, 2)
                    curr_chosen_pred_pos = curr_masked_pred_pos[closest_latent_index[0]]                    # (max_vehicles, seq_len, 2)
                    curr_chosen_vva_traj = curr_masked_vva_traj[closest_latent_index[0]]                    # (seq_len, max_vehicles, max_vehicles)
                    curr_chosen_lva_traj = curr_masked_lva_traj[closest_latent_index[0]]                    # (seq_len, max_vehicles, 3)
                    curr_chosen_koopman_mean_traj = curr_masked_koopman_mean_traj[closest_latent_index[0]]  # (max_vehicles, seq_len, args.vae_latent, args.vae_latent)
                    curr_chosen_koopman_var_traj = curr_masked_koopman_var_traj[closest_latent_index[0]]    # (max_vehicles, seq_len, args.vae_latent, args.vae_latent)
                    curr_chosen_pred_score = curr_masked_pred_score[closest_latent_index[0]]                # (max_vehicles, (seq_len - 1))
                    curr_chosen_pred_latent_traj = curr_masked_pred_latent_traj[closest_latent_index[0]]    # (max_vehicles, args.vae_latent, seq_len)
                    curr_chosen_all_gt_labels = curr_masked_all_gt_labels[closest_latent_index[0]]          # (max_vehicles, (seq_len - 1))

                    #############
                    # Plot road #
                    #############

                    plt.figure('road_trajectory_{}'.format(traj_num), figsize=(8, 3), dpi=300)

                    xpoints = np.array([0., W])

                    bottom_outer_ypoints = np.array([LOWER, LOWER])
                    plt.plot(xpoints, bottom_outer_ypoints, 'k-')
                    bottom_center_ypoints = np.array([LOWER + LANE_WIDTH, LOWER + LANE_WIDTH])
                    plt.plot(xpoints, bottom_center_ypoints, 'y--')

                    center_ypoints = np.array([STREET_CENTER, STREET_CENTER])
                    plt.plot(xpoints, center_ypoints, 'k-')

                    top_center_ypoints = np.array([STREET_CENTER + LANE_WIDTH, STREET_CENTER + LANE_WIDTH])
                    plt.plot(xpoints, top_center_ypoints, 'y--')
                    top_outer_ypoints = np.array([STREET_CENTER + 2 * LANE_WIDTH, STREET_CENTER + 2 * LANE_WIDTH])
                    plt.plot(xpoints, top_outer_ypoints, 'k-')

                    ############ Plot ground truth trajectories ############
                    perm_gt_pos = np.transpose(curr_chosen_gt_pos, (0, 2, 1))   # (max_vehicles, 2, seq_len)
                    for agent_id in range(trained_args.max_veh):
                        for t in range(trained_args.seq_len - 1):
                            plt.plot([perm_gt_pos[agent_id][0][t], perm_gt_pos[agent_id][0][t+1]], [perm_gt_pos[agent_id][1][t], perm_gt_pos[agent_id][1][t+1]], color=CAR_COLORS[agent_id], linestyle='-', linewidth=2, alpha=(t/(trained_args.seq_len-1)), zorder=2)

                    ############ Plot model prediction trajectories ############
                    perm_pred_pos = np.transpose(curr_chosen_pred_pos, (0, 2, 1))   # (max_vehicles, 2, seq_len) 
                    pred_pos_abs = perm_gt_pos + perm_pred_pos
                    for agent_id in range(trained_args.max_veh):
                        plt.plot(pred_pos_abs[agent_id][0], pred_pos_abs[agent_id][1], color=PRED_COLORS[agent_id], linestyle=':', marker='o', markerfacecolor='none', alpha=0.7, zorder=1)

                    # Save figure
                    figure_path = os.path.join(sub_figure_folder_path, 'trajectory_{}.png'.format(traj_num))
                    plt.savefig(figure_path)

                    ##########################
                    # Plot attention weights #
                    ##########################

                    plt.figure('attention_{}'.format(traj_num))
                    fig, axs = plt.subplots(2, trained_args.seq_len, figsize=(8, 3), dpi=300)

                    # Plot VVA Sequence
                    for time in range(trained_args.seq_len):
                        axs[0, time].axis('off')
                        axs[0, time].imshow(curr_chosen_vva_traj[time])

                    # Plot LVA Sequence
                    for time in range(trained_args.seq_len):
                        axs[1, time].axis('off')
                        axs[1, time].imshow(curr_chosen_lva_traj[time])

                    # Save figure
                    fig.tight_layout()
                    figure_path = os.path.join(sub_figure_folder_path, 'attention_{}.png'.format(traj_num))
                    plt.savefig(figure_path)

                    #########################
                    # Plot koopman matrices #
                    #########################

                    plt.figure('koopman_{}'.format(traj_num))
                    fig, axs = plt.subplots(2 * trained_args.max_veh, trained_args.seq_len, figsize=(8, 3), dpi=300)

                    # Plot Koopman Mean Sequence
                    for time in range(trained_args.seq_len):
                        for agent_id in range(trained_args.max_veh):
                            axs[2*agent_id, time].axis('off')
                            axs[2*agent_id, time].imshow(curr_chosen_koopman_mean_traj[agent_id][time])

                    # Plot Koopman Variance Sequence
                    for time in range(trained_args.seq_len):
                        for agent_id in range(trained_args.max_veh):
                            axs[2*agent_id+1, time].axis('off')
                            axs[2*agent_id+1, time].imshow(curr_chosen_koopman_var_traj[agent_id][time])

                    # Save figure
                    fig.tight_layout()
                    figure_path = os.path.join(sub_figure_folder_path, 'koopman_{}.png'.format(traj_num))
                    plt.savefig(figure_path)

                    ########################################################
                    # Trajectory Loss Curve - 2 Options as commented below #
                    ########################################################

                    ######################################################################################################
                    # USE FOLLOWING PLOTTING CODE IF PREDICTION LOSSES ARE GREATER THAN 1.0 TO SPLIT Y-AXIS IN TWO PARTS #
                    ######################################################################################################

                    # f, (ax, ax2) = plt.subplots(2, 1, sharex=True, figsize=(9, 4), dpi=300)

                    # # Plot prediction loss line graphs
                    # for agent_id in range(trained_args.max_veh):
                    #     ax.plot(np.array([t for t in range(trained_args.seq_len - 1)]), curr_chosen_pred_score[agent_id], color=PRED_COLORS[agent_id], linestyle=':', marker='o', linewidth=3, markersize=8)
                    #     ax2.plot(np.array([t for t in range(trained_args.seq_len - 1)]), curr_chosen_pred_score[agent_id], color=PRED_COLORS[agent_id], linestyle=':', marker='o', linewidth=3, markersize=8)

                    # max_curr_chosen_all_gt_labels = np.max(curr_chosen_all_gt_labels, axis=0)

                    # for t in range(trained_args.seq_len - 2):
                    #     if max_curr_chosen_all_gt_labels[t+1] == 0:
                    #         back_color = 'green'
                    #     elif max_curr_chosen_all_gt_labels[t+1] == 1:
                    #         back_color = 'red'
                    #     else:
                    #         back_color = 'orange'
                    #     light_back_color = lighten_color(back_color)
                    #     ax.axvspan(t, t+1, facecolor=light_back_color, alpha=0.3)
                    #     ax2.axvspan(t, t+1, facecolor=light_back_color, alpha=0.3)

                    # ax.set_ylim(10, 40)  # outliers only
                    # ax2.set_ylim(-0.05, 1)  # most of the data
                    # # plt.xlabel('Timestep')

                    # # hide the spines between ax and ax2
                    # ax.spines['bottom'].set_visible(False)
                    # ax2.spines['top'].set_visible(False)
                    # ax.xaxis.tick_top()
                    # ax.tick_params(labeltop=False)  # don't put tick labels at the top
                    # ax2.xaxis.tick_bottom()

                    # ax.tick_params(
                    #     axis='x',          # changes apply to the x-axis
                    #     which='both',      # both major and minor ticks are affected
                    #     bottom=False,      # ticks along the bottom edge are off
                    #     top=False,         # ticks along the top edge are off
                    #     labelbottom=False) # labels along the bottom edge are off

                    # d = .005  # how big to make the diagonal lines in axes coordinates
                    # # arguments to pass to plot, just so we don't keep repeating them
                    # kwargs = dict(transform=ax.transAxes, color='k', clip_on=False)
                    # ax.plot((-d, +d), (-d, +d), **kwargs)        # top-left diagonal
                    # ax.plot((1 - d, 1 + d), (-d, +d), **kwargs)  # top-right diagonal

                    # kwargs.update(transform=ax2.transAxes)  # switch to the bottom axes
                    # ax2.plot((-d, +d), (1 - d, 1 + d), **kwargs)  # bottom-left diagonal
                    # ax2.plot((1 - d, 1 + d), (1 - d, 1 + d), **kwargs)  # bottom-right diagonal

                    # # plt.xlabel('Timestep')
                    # # overall_ax = f.add_subplot(111)
                    # # overall_ax.set_ylabel('Anomaly Score')
                    # # f.text(0.065, 0.5, 'Anomaly Score', va='center', rotation='vertical')

                    # ax.tick_params(axis='both', which='major', labelsize=16)
                    # ax2.tick_params(axis='both', which='major', labelsize=16)

                    # # Save figure
                    # figure_path = os.path.join(sub_figure_folder_path, 'loss_curve_{}.png'.format(traj_num))
                    # plt.savefig(figure_path)

                    ######################################################################################################

                    ######################################################################
                    # USE FOLLOWING PLOTTING CODE IF PREDICTION LOSSES ARE LESS THAN 1.0 #
                    ######################################################################

                    fig = plt.figure('trajectory_loss_{}'.format(traj_num), figsize=(9, 4), dpi=300)

                    # Plot prediction loss line graphs
                    for agent_id in range(trained_args.max_veh):
                        plt.plot(np.array([t for t in range(trained_args.seq_len - 1)]), curr_chosen_pred_score[agent_id], color=PRED_COLORS[agent_id], linestyle=':', marker='o', linewidth=3, markersize=8)

                    max_curr_chosen_all_gt_labels = np.max(curr_chosen_all_gt_labels, axis=0)
                    ax = fig.axes[0]

                    for t in range(trained_args.seq_len - 2):
                        if max_curr_chosen_all_gt_labels[t+1] == 0:
                            back_color = 'green'
                        elif max_curr_chosen_all_gt_labels[t+1] == 1:
                            back_color = 'red'
                        else:
                            back_color = 'orange'
                        light_back_color = lighten_color(back_color)
                        ax.axvspan(t, t+1, facecolor=light_back_color, alpha=0.3)

                    plt.ylim((-0.05, 1))
                    plt.ylabel('Anomaly Score')
                    plt.xlabel('Timestep')
                    ax.tick_params(axis='both', which='major', labelsize=16)

                    ######################################################################

                    # Save figure
                    figure_path = os.path.join(sub_figure_folder_path, 'loss_curve_{}.png'.format(traj_num))
                    plt.savefig(figure_path)

                    ############################
                    # Latent space propagation #
                    ############################

                    # plt.figure('latent_sequence_{}'.format(traj_num), figsize=(8, 8), dpi=300)
                    plt.figure(density_fig)

                    # Plot latent points
                    for agent_id in range(trained_args.max_veh):
                        for t in range(trained_args.seq_len - 2):
                            if curr_chosen_all_gt_labels[agent_id][t+1] == 0:
                                latent_prop_color = 'green'
                            elif curr_chosen_all_gt_labels[agent_id][t+1] == 1:
                                latent_prop_color = 'red'
                            else:
                                latent_prop_color = 'orange'
                            plt.plot([curr_chosen_pred_latent_traj[agent_id][0][t], curr_chosen_pred_latent_traj[agent_id][0][t+1]], [curr_chosen_pred_latent_traj[agent_id][1][t], curr_chosen_pred_latent_traj[agent_id][1][t+1]], color=latent_prop_color, linestyle='-', alpha=(t/(trained_args.seq_len - 2)), zorder=1)
                    
                    for t in range(1, trained_args.seq_len - 1):
                        for agent_id in range(trained_args.max_veh):
                            plt.scatter(curr_chosen_pred_latent_traj[agent_id][0][t], curr_chosen_pred_latent_traj[agent_id][1][t], color=LATENT_PRED_COLORS[agent_id], marker='o', edgecolors='k', zorder=2, s=3*1.5**(trained_args.seq_len - 1 - t))

                    plt.xlim([-150, 0])
                    plt.ylim([-130, 20])
                    plt.xticks([])
                    plt.yticks([])

                    # Add custom legend
                    custom_lines = [mlines.Line2D([], [], color="white", marker='o', markersize=20, markerfacecolor="white", markeredgecolor='black'),
                                    mlines.Line2D([], [], color="white", marker='o', markersize=10, markerfacecolor="white", markeredgecolor='black'),
                                    mlines.Line2D([], [], color="white", marker='o', markersize=4, markerfacecolor="white", markeredgecolor='black')]
                    density_ax.legend(custom_lines, ['t = 0', 't = 6', 't = 12'], prop={'size': LATENT_LEGEND_SIZE})

                    ##########################################################
                    # UNCOMMENT FOLLOWING CODE IF WANT TO ADD A ZOOM-IN PLOT #
                    ##########################################################

                    # # Make the zoom-in plot:
                    # axins = zoomed_inset_axes(density_ax, 6, loc=4) # zoom = 2
                    # # axins = zoomed_inset_axes(density_ax, 8, loc=4) # zoom = 2
                    # # axins = zoomed_inset_axes(density_ax, 10, loc=4) # zoom = 2

                    # density_scatter( curr_masked_latent_density[:,0].reshape((-1)), curr_masked_latent_density[:,1].reshape((-1)), axins, bins = [30,30] )

                    # for agent_id in range(trained_args.max_veh):
                    #     for t in range(trained_args.seq_len - 2):
                    #         if curr_chosen_all_gt_labels[agent_id][t+1] == 0:
                    #             latent_prop_color = 'green'
                    #         elif curr_chosen_all_gt_labels[agent_id][t+1] == 1:
                    #             latent_prop_color = 'red'
                    #         else:
                    #             latent_prop_color = 'orange'
                    #         # plt.plot([curr_chosen_pred_latent_traj[agent_id][0][t], curr_chosen_pred_latent_traj[agent_id][0][t+1]], [curr_chosen_pred_latent_traj[agent_id][1][t], curr_chosen_pred_latent_traj[agent_id][1][t+1]], color=latent_prop_color, linestyle='-', marker='x', mfc=RECON_COLORS[agent_id], mec='k', alpha=(t/(trained_args.seq_len - 2)))
                    #         axins.plot([curr_chosen_pred_latent_traj[agent_id][0][t], curr_chosen_pred_latent_traj[agent_id][0][t+1]], [curr_chosen_pred_latent_traj[agent_id][1][t], curr_chosen_pred_latent_traj[agent_id][1][t+1]], color=latent_prop_color, linestyle='-', alpha=(t/(trained_args.seq_len - 2)), zorder=1)

                    # for t in range(1, trained_args.seq_len - 1):
                    #     for agent_id in range(trained_args.max_veh):
                    #         # print('t: ', t, ' s: ', 2*1.5**t, ' opp: ', 2*1.5**(trained_args.seq_len - 1 - t))
                    #         axins.scatter(curr_chosen_pred_latent_traj[agent_id][0][t], curr_chosen_pred_latent_traj[agent_id][1][t], color=LATENT_PRED_COLORS[agent_id], marker='o', edgecolors='k', zorder=2, s=12*1.5**(trained_args.seq_len - 1 - t), alpha=(t/(trained_args.seq_len - 2))/1.2)

                    # axins.set_xlim(-13, -3)
                    # axins.set_ylim(3, 13)
                    # # axins.set_xlim(-11, -5)
                    # # axins.set_ylim(5, 11)
                    # plt.xticks(visible=False)
                    # plt.yticks(visible=False)
                    # mark_inset(density_ax, axins, loc1=2, loc2=1, fc="none", ec="0.5")
                    # plt.draw()

                    ##########################################################

                    # Save figure
                    figure_path = os.path.join(sub_figure_folder_path, 'latent_sequence_{}.png'.format(traj_num))
                    plt.savefig(figure_path)

                # Save annotated latent space figure
                plt.figure('prop_latent_space')
                plt.title("Annotated Propagated Latent Space", fontsize=LATENT_TITLE_SIZE)
                figure_path = os.path.join(curr_figure_folder_path, 'annotated_latent.png')
                plt.savefig(figure_path)
