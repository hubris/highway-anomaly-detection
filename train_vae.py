import os
import pickle
from statistics import mean

import configargparse
from tqdm import tqdm
import torch
import torch.optim as optim
import pandas as pd

from utils.data_loader import loadDataset
from models.vae import Attenuated_Recurrent_VAE, update_linear_schedule

ENVIRONMENT = ['maad']


if __name__ == '__main__':

    p = configargparse.ArgumentParser(
        prog='SABeR-VAE Trainer',
        description='Trains a SABeR-VAE model to reconstruct trajectories from a given dataset.',
        formatter_class=configargparse.ArgumentDefaultsHelpFormatter
    )

    ############
    # OS Setup #
    ############

    # Environment Name
    p.add('--env', type=str, metavar='ENV', required=True, choices=ENVIRONMENT,
          help='Name of environment to load data from.')

    # Training Run Name
    p.add('--run_name', type=str, metavar='RUN_NAME', default='', required=False,
          help='Name of run subfolder to create under training directory.')

    # Run Name to Load Data From
    p.add('--data_run_name', type=str, metavar='DATA_RUN_NAME', required=True,
          help='Name of run subfolder to load from data directory.')

    # Train on GPU
    p.add('--gpu', action='store_true', default=False,
                help='Set device for PyTorch as GPU if available.')

    # Debug Flag
    p.add('--debug', action='store_true', default=False,
                help='Set true if debugging.')

    # Number of Data Loading Workers
    p.add('--num_workers', type=int, metavar='THREADS', default=0,
                help='Number of workers used in data loader.')
    
    ####################
    # Hyper Parameters #
    ####################

    # Latent Space Dimension
    p.add('--vae_latent', type=int, metavar='VAE_LAT', default=64,
                help='Dimension of Gaussian latent space.')

    # Number of Epochs to Train VAE
    p.add('--num_epochs', type=int, metavar='EPOCH', default=500,
                help='Total number of epochs to train the VAE for over the whole dataset.')

    # Batch Size
    p.add('--batch_size', type=int, metavar='BATCH', default=64,
                help='Batch size of training inputs.')

    # Max Trajectory Length
    p.add('--seq_len', type=int, metavar='SEQ_LEN', default=15,
                help='Max length of windows in a sequence.')

    # Learning Rate
    p.add('--lr', type=float, metavar='LR', default=5e-4,
                help='Learning rate for SGD.')

    # Optimizer Weight Decay
    p.add('--weight_decay', type=float, metavar='DECAY', default=1e-6,
                help='Weight decay for Adam optimizer.')

    # Pre-Koopman KL Divergence Weighting 
    p.add('--pre_kl_beta', type=float, metavar='PRE_BETA', default=5e-7,
                help='Weight applied to KL divergence in the loss function before koopman operator.')

    # Post-Koopman KL Divergence Weighting 
    p.add('--post_kl_beta', type=float, metavar='POST_BETA', default=5e-7,
                help='Weight applied to KL divergence in the loss function after koopman operator.')

    #############################
    # Vehicle-Vehicle Attention #
    #############################

    # Use a VVA layer
    p.add('--vva', action='store_true', default=False,
                help='Use a VVA layer.')

    # Max Number of Vehicles for Interactions
    p.add('--max_veh', type=int, metavar='MAX_VEH', default=2,
                help='Max number of vehicles for vehicle-vehicle interaction modeling.')

    # Distance to Mask Out Vehicles in VVA
    p.add('--veh_dist_mask', type=float, metavar='DIST_MASK', default=45,
                help='Agents farther than this distance away from any other vehicle will not \
                      be attenuated on, in that vehicle\'s attention layers.')

    # Normalize Inter-vehicle Distance
    p.add('--vva_norm', action='store_true', default=False,
                help='Normalize the distance between vehicles before passing to VVA.')

    # Number of Heads in VVA Layer
    p.add('--vva_heads', type=int, metavar='VVA_HEADS', default=8,
                help='Number of heads in VVA layer.')

    # VVA Dropout
    p.add('--vva_drop', type=float, metavar='VVA_DROP', default=0.0,
                help='Float between 0 and 1 for dropout in VVA layer.')

    # Vehicle-Vehicle Attention Output Size
    p.add('--vva_out', type=int, metavar='VVA_SIZE', default=128,
                help='Size of output encodings of Vehicle-Vehicle attention network.')

    ##########################
    # Lane-Vehicle Attention #
    ##########################

    # Use an LVA layer
    p.add('--lva', action='store_true', default=False,
                help='Use an LVA layer.')

    # Number of Heads in LVA Layer
    p.add('--lva_heads', type=int, metavar='LVA_HEADS', default=8,
                help='Number of heads in LVA layer.')

    # LVA Dropout
    p.add('--lva_drop', type=float, metavar='LVA_DROP', default=0.0,
                help='Float between 0 and 1 for dropout in LVA layer.')

    # Lane-Vehicle Attention Output Size
    p.add('--lva_out', type=int, metavar='LVA_SIZE', default=128,
                help='Size of output encodings of Lane-Vehicle attention network.')

    ###############
    # GRU Encoder #
    ###############

    # Encoder GRU Hidden Size
    p.add('--enc_gru_hid', type=int, metavar='GRU_HID', default=256,
                help='Size of hidden layers in GRU of encoder.')

    # Encoder GRU Number of Layers
    p.add('--enc_gru_layers', type=int, metavar='GRU_LAY', default=1,
                help='Number of layers in encoder GRU\'s network.')

    # Encoder GRU Dropout
    p.add('--enc_gru_drop', type=float, metavar='GRU_DROP', default=0.0,
                help='Percentage of nodes to dropout in encoder GRU\'s network during forward passes.')

    # Encoder GRU Bidirectional
    p.add('--enc_gru_bidir', action='store_true', default=False,
                help='Set encoder GRU to be bidirectional.')


    args = p.parse_args()

    # Create folder for where model will be saved to
    model_dir = os.path.join("pretrained", "debug" if (args.debug or not args.run_name) else args.run_name)
    os.makedirs(model_dir, exist_ok=True)

    # Create subfolder for environment
    env_dir = os.path.join(model_dir, args.env)
    os.makedirs(env_dir, exist_ok=True)

    # Create subfolder for latest run of saves
    run_num = 0
    while os.path.exists(os.path.join(env_dir, str(run_num))):
        run_num += 1

    run_dir = os.path.join(env_dir, str(run_num))
    os.makedirs(run_dir, exist_ok=False)

    checkpoint_dir = os.path.join(run_dir, 'checkpoints')
    os.makedirs(checkpoint_dir, exist_ok=False)

    # Save arguments to run directory
    model_args_file = os.path.join(run_dir, 'model_args.pickle')
    with open(model_args_file, 'wb') as handle:
        pickle.dump(args, handle, protocol=pickle.HIGHEST_PROTOCOL)

    # Directory with dataset
    data_dir = os.path.join("data", args.data_run_name, args.env)

    # Get training dataset of trajectories for environment
    data_generator = loadDataset(   split='train', 
                                    batch_size=args.batch_size, 
                                    num_workers=args.num_workers, 
                                    drop_last=True, 
                                    load_dir=data_dir)

    # Set device
    device = torch.device("cuda" if torch.cuda.is_available() and args.gpu else "cpu")
    print("Using device:", device)

    if args.debug:
        torch.set_printoptions(linewidth=250)
        torch.set_printoptions(profile="full")
        import pdb

    # Create VAE model
    vae_model = Attenuated_Recurrent_VAE(device, args)
    vae_model = vae_model.to(device)
    vae_model.train()

    # Define the optimizer
    optimizer = optim.Adam(vae_model.parameters(),
                           lr=args.lr,
                           weight_decay=args.weight_decay)

    # Iterate over epochs
    for epoch in tqdm(range(args.num_epochs)):

        # Lists to keep track of average loss across training
        loss_ep = []            # total loss
        act_loss_curr_ep = []   # current reconstruction loss
        act_loss_next_ep = []   # future prediction loss
        kl_loss_pre_ep = []     # latent KL divergence pre-koopman
        kl_loss_post_ep = []    # latent KL divergence post-koopman

        # Iterate over batches
        for i_batch, data in enumerate(tqdm(data_generator)):

            # Zero gradients
            vae_model.zero_grad()
            optimizer.zero_grad()

            # Move data to device
            data = data.float().to(device)  # (batch size, max_vehicles, seq_len, 15)

            # Max possible sequence length
            max_traj_len = data.shape[2]
            assert args.seq_len <= max_traj_len and args.seq_len > 0

            # Max possible number of vehicles
            max_num_veh = data.shape[1]
            assert args.max_veh <= max_num_veh and args.max_veh > 0

            # Observation mask of which cars were observable at which time step for each episode in the batch
            observed_mask = data[:,:args.max_veh,:args.seq_len,2].bool()    # (batch size, max_vehicles, seq_len)

            # Ground truth absolute and relative change x,y positions of each car at each time
            pos = data[:,:args.max_veh,:args.seq_len,3:5]       # (batch size, max_vehicles, seq_len, 2)
            rel_pos = data[:,:args.max_veh,:args.seq_len,5:7]   # (batch size, max_vehicles, seq_len, 2)

            # Ground truth x,y positions of the discretized lane nodes of each car at each time relative to vehicles
            lane_succ = data[:,:args.max_veh,:args.seq_len,10:12].unsqueeze(3)  # (batch size, max_vehicles, seq_len, 1, 2)
            lane_left = data[:,:args.max_veh,:args.seq_len,15:17].unsqueeze(3)  # (batch size, max_vehicles, seq_len, 1, 2)
            lane_right = data[:,:args.max_veh,:args.seq_len,20:22].unsqueeze(3) # (batch size, max_vehicles, seq_len, 1, 2)
            lane_pos = torch.cat((lane_succ, lane_left, lane_right), 3)         # (batch size, max_vehicles, seq_len, 3, 2)

            # Mask of which lane edges exist for each center lane node at each time
            succ_lane_exist_mask = data[:,:args.max_veh,:args.seq_len,7].bool().unsqueeze(3)                    # (batch size, max_vehicles, seq_len, 1)
            left_lane_exist_mask = data[:,:args.max_veh,:args.seq_len,12].bool().unsqueeze(3)                   # (batch size, max_vehicles, seq_len, 1)
            right_lane_exist_mask = data[:,:args.max_veh,:args.seq_len,17].bool().unsqueeze(3)                  # (batch size, max_vehicles, seq_len, 1)
            lane_exist_mask = torch.cat((succ_lane_exist_mask, left_lane_exist_mask, right_lane_exist_mask), 3) # (batch size, max_vehicles, seq_len, 3)

            # Create key/val source from vehicle positions with shape (batch size, max_vehicles, seq_len, max_vehicles, pos_dim) by repeating veh_pos_input
            # Permute veh_pos_input so that sequence length dimension is outside of repeating area
            vva_permute_kvs = pos.permute((0, 2, 1, 3))                         # (batch size, seq_len, max_vehicles, pos_dim)
            # Unsqueeze to add dimension to repeat
            vva_unsqueeze_kvs = vva_permute_kvs.unsqueeze(2)                    # (batch size, seq_len, 1, max_vehicles, pos_dim)
            # Repeat by the number of vehicles 
            vva_repeat_kvs = vva_unsqueeze_kvs.repeat(1, 1, args.max_veh, 1, 1) # (batch size, seq_len, max_vehicles, max_vehicles, pos_dim)
            # Unpermute so that sequence length dimension is back to where it was
            vva_kvs = vva_repeat_kvs.permute(0, 2, 1, 3, 4)                     # (batch size, max_vehicles, seq_len, max_vehicles, pos_dim)

            # Based on observation mask
            # Permute observed_mask so that sequence length dimension is outside of repeating area
            vva_permute_obs_mask = observed_mask.permute((0, 2, 1))                     # (batch size, seq_len, max_vehicles)
            # Unsqueeze to add dimension to repeat
            vva_unsqueeze_obs_mask = vva_permute_obs_mask.unsqueeze(2)                  # (batch size, seq_len, 1, max_vehicles)
            # Repeat by the number of vehicles 
            vva_repeat_obs_mask = vva_unsqueeze_obs_mask.repeat(1, 1, args.max_veh, 1)  # (batch size, seq_len, max_vehicles, max_vehicles)
            # Unpermute so that sequence length dimension is back to where it was
            vva_unpermute_obs_mask = vva_repeat_obs_mask.permute(0, 2, 1, 3)            # (batch size, max_vehicles, seq_len, max_vehicles)
            # Unsqueeze vva_unpermute_mask to follow convention of Attention module's forward function
            vva_obs_mask = vva_unpermute_obs_mask.unsqueeze(4)                          # (batch size, max_vehicles, seq_len, max_vehicles, 1)

            # Based on vehicles' distance to other vehicles
            # Unsqueeze to add dimension to repeat
            vva_unsqueeze_dist_mask = pos.unsqueeze(3)                                      # (batch size, max_vehicles, seq_len, 1, pos_dim)
            # Repeat by number of vehicles
            vva_repeat_dist_mask = vva_unsqueeze_dist_mask.repeat(1, 1, 1, args.max_veh, 1) # (batch size, max_vehicles, seq_len, max_vehicles, pos_dim)
            # Find difference between positions of each vehicle
            vva_diff_dist_mask = vva_kvs - vva_repeat_dist_mask                             # (batch size, max_vehicles, seq_len, max_vehicles, pos_dim)
            # Calculate distance norms
            vva_norm_dist_mask = torch.linalg.norm(vva_diff_dist_mask, dim=4, keepdim=True) # (batch size, max_vehicles, seq_len, max_vehicles, 1)
            # Filter out distances greater than threshold
            vva_dist_mask = (vva_norm_dist_mask <= args.veh_dist_mask).bool()               # (batch size, max_vehicles, seq_len, max_vehicles, 1)

            # Combine observation and distance masks
            vva_mask = torch.logical_and(vva_obs_mask, vva_dist_mask)   # (batch size, max_vehicles, seq_len, max_vehicles, 1)

            # Normalize VVA inter-vehicle distance
            if args.vva_norm:
                vva_diff_dist_mask /= args.veh_dist_mask

            # Forward pass through VAE model
            recon_traj_torch, pred_traj_torch, recon_mean_traj_torch, pred_mean_traj_torch,             \
            recon_var_traj_torch, pred_var_traj_torch, recon_latent_traj_torch, pred_latent_traj_torch, \
            vva_traj_torch, lva_traj_torch, koopman_mean_traj_torch, koopman_var_traj_torch =           \
                vae_model(rel_pos, vva_diff_dist_mask, vva_mask, lane_pos, lane_exist_mask)

            # Calculate loss against ground truth, with KL divergence regularization
            total_loss, recon_curr_loss, pred_next_loss, kl_div_pre, kl_div_post =                              \
                vae_model.loss( observed_mask.reshape((-1,args.seq_len)), rel_pos.reshape((-1,args.seq_len,2)), \
                                recon_traj_torch, pred_traj_torch, recon_mean_traj_torch, recon_var_traj_torch, \
                                pred_mean_traj_torch, pred_var_traj_torch)

            # Update model
            total_loss.backward()
            optimizer.step()

            # Keep track of losses
            loss_ep.append(total_loss.item())
            act_loss_curr_ep.append(recon_curr_loss)
            act_loss_next_ep.append(pred_next_loss)
            kl_loss_pre_ep.append(kl_div_pre)
            kl_loss_post_ep.append(kl_div_post)

        # Update learning rate
        update_linear_schedule(optimizer, epoch + 1, args.num_epochs, args.lr)

        # Zero gradients
        vae_model.zero_grad()
        optimizer.zero_grad()

        # Save final model
        if epoch == args.num_epochs - 1:
            fname = os.path.join(checkpoint_dir, 'final.pt')
            save_dict = {}
            save_dict['model'] = vae_model.state_dict()
            save_dict['optim'] = optimizer.state_dict()
            torch.save(save_dict, fname)
            print('Model saved to '+fname)

        # Save the model checkpoint every 5 epochs
        elif epoch % 5 == 0:
            fname = os.path.join(checkpoint_dir, str(epoch)+'.pt')
            save_dict = {}
            save_dict['model'] = vae_model.state_dict()
            save_dict['optim'] = optimizer.state_dict()
            torch.save(save_dict, fname)
            print('Model saved to '+fname)

        # Log losses every epoch
        if epoch % 1 == 0:
            print("Epoch {}, total loss {}, recon loss {}, pred loss {}, pre kl loss {}, post kl loss {}\n"  \
                  .format(epoch, round(mean(loss_ep), 3), round(mean(act_loss_curr_ep), 3), round(mean(act_loss_next_ep), 3), round(mean(kl_loss_pre_ep), 3), round(mean(kl_loss_post_ep), 3)))
            df = pd.DataFrame({ 'epoch': [epoch], 'total_loss': [mean(loss_ep)], 'recon_loss': [mean(act_loss_curr_ep)],    \
                                'pred_loss': [mean(act_loss_next_ep)], 'pre_kl_loss': [mean(kl_loss_pre_ep)], 'post_kl_loss': [mean(kl_loss_post_ep)]})

            if os.path.exists(os.path.join(run_dir, 'progress.csv')) and epoch > 1:
                df.to_csv(os.path.join(run_dir, 'progress.csv'), mode='a', header=False, index=False)
            else:
                df.to_csv(os.path.join(run_dir, 'progress.csv'), mode='w', header=True, index=False)
