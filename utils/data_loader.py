import numpy as np
import torch
import os

from torch.utils.data.dataset import ConcatDataset
from torch.utils.data.dataset import Dataset

class TrajectoryDataset(Dataset):
    """
    Dataset containing trajectories collected from a simulator environment.
    """

    def __init__(self, data_path):
        self.file_path = data_path
        self.traj_data = np.load(data_path)

    def __getitem__(self, index):
        return self.traj_data[index]

    def __len__(self):
        return self.traj_data.shape[0]

def loadDataset(split, batch_size, num_workers, drop_last, load_dir, allow_shuffle=True):
    """
    Dataset containing trajectories collected from a simulator environment.

    :param split: name of data split to load from <train/val/test>
    :param batch_size: size of batches in data loader
    :param num_workers: number of subprocesses to use for data loading
    :param drop_last: Set to True to drop the last incomplete batch
    :param load_dir: directory to load numpy trajectory files from
    
    output: dataloader
    """

    # Check if load directory exists
    assert os.path.exists(load_dir)

    all_datasets = []   # list to append different datasets to

    train_test_folder = os.path.join(load_dir, split)

    # Iterate over run folders within the train or test folder
    run_num = 0
    while os.path.exists(os.path.join(train_test_folder, str(run_num))):
        
        # Iterate over epoch numpy files within the run folders
        epoch = 0
        while os.path.exists(os.path.join(train_test_folder, str(run_num), str(epoch)+'.npy')):
            # Append dataset for epoch to all_datasets
            all_datasets.append(TrajectoryDataset(os.path.join(train_test_folder, str(run_num), str(epoch)+'.npy')))
            epoch += 1

        run_num += 1

    # Create overall datasets holding all epochs over all seeds
    complete_dataset = ConcatDataset(all_datasets)

    # Create data loader for all trajectories
    shuffle = True if (split=='train' and allow_shuffle) else False
    data_loader = torch.utils.data.DataLoader(complete_dataset,
                                            batch_size=batch_size,
                                            shuffle=shuffle,
                                            num_workers=num_workers,
                                            pin_memory=True,
                                            drop_last=drop_last)

    return data_loader
