import numpy as np


class LaneNode(object):
    """
    A class holding information about the predecessor(s), successor(s), left, and right nodes
    in a road network.
    """

    def __init__(self, lane) -> None:
        """
        pred: index of LaneNode predecessor -> int (a node can only have one predeccessor)
        succ: list of indices of LaneNode successors -> [int, int, ...] (a node can have multiple successors)
        left: index of LaneNode to left -> int (a node can only have one left node)
        right: index of LaneNode to right -> int (a node can only have one right node)
        lane: lane that the LaneNode is on -> AbstractLane
        pos_x: x coordinate of the center of this node -> float
        pos_y: y coordinate of the center of this node -> float
        """
        self.pred = None
        self.succ = None
        self.left = None
        self.right = None 
        self.lane = lane
        self.pos_x = 0.
        self.pos_y = 0.

    def __str__(self) -> str:
        
        out = {}
        out["pred"] = self.pred
        out["succ"] = self.succ
        out["left"] = self.left
        out["right"] = self.right
        out["lane"] = self.lane
        out["pos_x"] = self.pos_x
        out["pos_y"] = self.pos_y
        return str(out)


def find_nearest(array, value):
    """
    return the index of the element in the array that is closest to given value by euclidean distance
    param array: list of 2D elements
    param value: 2D value to find element that is closest to in the array
    output: index of element from array
    """
    node_poses = np.asarray(array)
    sub = node_poses-value
    norm = np.linalg.norm(sub, axis=1)
    argmin = np.argmin(norm)
    return argmin
